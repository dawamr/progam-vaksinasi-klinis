<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_get extends CI_Model
{
	public function getFaskesById($id){
		$hsl=$this->db->query("SELECT * FROM table_faskes WHERE id_faskes='$id'");

		if($hsl->num_rows()>0){
			foreach ($hsl->result() as $data) {
				$hasil=array(
					'id_faskes' => $data->id_faskes,
          			'nama_faskes' => $data->nama_faskes,
					'tipe_faskes' => $data->tipe_faskes,
					'alamat_faskes' => $data->alamat_faskes,
					'kota_faskes' => $data->kota_faskes,
					'email_faskes' => $data->email_faskes,
					'phone_faskes' => $data->phone_faskes,
					'longtitude_faskes' => $data->longtitude_faskes,
					'latitude_faskes' => $data->latitude_faskes,
					'logo_mitra' => $data->logo_mitra,
        		);
			}
		}
		return $hasil;
	}

	public function getUserById($id){
		$hsl=$this->db->query("SELECT * FROM table_user WHERE id_user='$id'");

		if($hsl->num_rows()>0){
			foreach ($hsl->result() as $data) {
				$hasil=array(
					'id_user' => $data->id_user,
          			'id_faskes' => $data->id_faskes,
					'nama_user' => $data->nama_user,
					'email_user' => $data->email_user,
          			'status_user' => $data->status_user,
        		);
			}
		}
		return $hasil;
	}

	public function getProgramById($id){
		$hsl=$this->db->query("SELECT * FROM table_program WHERE id_program='$id'");

		if($hsl->num_rows()>0){
			foreach ($hsl->result() as $data) {
				$hasil=array(
          			'id_program' => $data->id_program,
					'id_user' => $data->id_user,
					'tipe_program' => $data->tipe_program,
					'nama_program' => $data->nama_program,
					'deskripsi_program' => $data->deskripsi_program,
					'tanggal_mulai_program' => $data->tanggal_mulai_program,
					'tanggal_berakhir_program' => $data->tanggal_berakhir_program,
					'lokasi_program' => $data->lokasi_program,
					'kecamatan_program' => $data->kecamatan_program,
					'jenis_vaksin_program' => $data->jenis_vaksin_program,
					'target_peserta_program' => $data->target_peserta_program,
					'target_klien_program' => $data->target_klien_program,
					'goals_program' => $data->goals_program,
					'anggaran_program' => $data->anggaran_program,
          			'status_program' => $data->status_program,
        		);
			}
		}
		return $hasil;
	}

	public function getSesiById($id){
		$hsl=$this->db->query("SELECT * FROM table_sesi_program WHERE id_sesi='$id'");

		if($hsl->num_rows()>0){
			foreach ($hsl->result() as $data) {
				$hasil=array(
          			'id_sesi' => $data->id_sesi,
					'id_program' => $data->id_program,
					'nama_sesi' => $data->nama_sesi,
					'tanggal_sesi' => $data->tanggal_sesi,
					'waktu_mulai_sesi' => $data->waktu_mulai_sesi,
					'waktu_berakhir_sesi' => $data->waktu_berakhir_sesi,
					'kuota_sesi' => $data->kuota_sesi,
					'status_sesi' => $data->status_sesi,
        		);
			}
		}
		return $hasil;
	}

	public function getKotaById($id){
		$hsl=$this->db->query("SELECT * FROM table_kota_kabupaten WHERE kode_kota_kabupaten='$id'");

		if($hsl->num_rows()>0){
			foreach ($hsl->result() as $data) {
				$hasil=array(
          			'id_kota_kabupaten' => $data->id_kota_kabupaten,
					'kode_kota_kabupaten' => $data->kode_kota_kabupaten,
					'nama_kota_kabupaten' => $data->nama_kota_kabupaten,
        		);
			}
		}
		return $hasil;
	}

	public function getKategoriById($kode_kategori){
		$hsl=$this->db->query("SELECT * FROM table_kategori_program WHERE kode_kategori='$kode_kategori'");

		if($hsl->num_rows()>0){
			foreach ($hsl->result() as $data) {
				$hasil=array(
					'kode_kategori' => $data->kode_kategori,
					'nama_kategori' => $data->nama_kategori,
        		);
			}
		}
		return $hasil;
	}

	public function getPesertaByNIKandPhone($nik,$phone){
		// $hsl=$this->db->query("SELECT * FROM table_peserta WHERE nik_peserta='$nik'");
		$sql_query = "
            SELECT a.*, b.*, c.*, d.*,e.* FROM table_peserta a
            LEFT JOIN table_koneksi_program b ON a.id_peserta = b.id_peserta
            LEFT JOIN table_sesi_program c ON b.id_sesi = c.id_sesi
            LEFT JOIN table_kategori_program d ON d.kode_kategori = a.pekerjaan_peserta
            LEFT JOIN table_kota_kabupaten e ON a.kota_peserta = e.kode_kota_kabupaten
            WHERE a.nik_peserta = '$nik'
        ";
        $hsl = $this->db->query($sql_query);

		if($hsl->num_rows()>0){
			foreach ($hsl->result() as $data) {
				if($data->phone_peserta == $phone){
					$hasil=array(
						'id_peserta' => $data->id_peserta,
	          			'nik_peserta' => $data->nik_peserta,
	          			'nama_peserta' => $data->nama_peserta,
	          			'email_peserta' => $data->email_peserta,
	          			'phone_peserta' => $data->phone_peserta,
	          			'tanggal_lahir_peserta' => date("d F Y", strtotime($data->tanggal_lahir_peserta)),
						'jenis_kelamin_peserta' => $data->jenis_kelamin_peserta,
						'kota_peserta' => $data->kota_peserta,
						'alamat_peserta' => $data->alamat_peserta,
						'pekerjaan_peserta' => $data->pekerjaan_peserta,
						'jenis_pekerjaan_peserta' => $data->jenis_pekerjaan_peserta,
						'status_peserta' => $data->status_peserta,

						'tanggal_sesi' => date('d F Y', strtotime($data->tanggal_sesi)),
						'waktu_mulai_sesi' => date("H:i", strtotime($data->waktu_mulai_sesi)),
						'waktu_berakhir_sesi' => date("H:i", strtotime($data->waktu_berakhir_sesi)),
						'kode_kategori' => $data->kategori_program,
						'nama_kategori' => $data->nama_kategori,

						'nama_kota_kabupaten' => $data->nama_kota_kabupaten,
						'response' => "berhasil",
        			);
				}else{
					$hasil=array(
						'response' => "gagal",
					);
				};
			}
		}
		return $hasil;
	}
}