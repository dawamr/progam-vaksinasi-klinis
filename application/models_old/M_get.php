<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_get extends CI_Model
{
	public function getFaskesById($id){
		$hsl=$this->db->query("SELECT * FROM table_faskes WHERE id_faskes='$id'");

		if($hsl->num_rows()>0){
			foreach ($hsl->result() as $data) {
				$hasil=array(
					'id_faskes' => $data->id_faskes,
          			'nama_faskes' => $data->nama_faskes,
					'tipe_faskes' => $data->tipe_faskes,
					'alamat_faskes' => $data->alamat_faskes,
					'kota_faskes' => $data->kota_faskes,
					'email_faskes' => $data->email_faskes,
					'phone_faskes' => $data->phone_faskes,
					'longtitude_faskes' => $data->longtitude_faskes,
					'latitude_faskes' => $data->latitude_faskes,
					'logo_mitra' => $data->logo_mitra,
        		);
			}
		}
		return $hasil;
	}

	public function getUserById($id){
		$hsl=$this->db->query("SELECT * FROM table_user WHERE id_user='$id'");

		if($hsl->num_rows()>0){
			foreach ($hsl->result() as $data) {
				$hasil=array(
					'id_user' => $data->id_user,
          			'id_faskes' => $data->id_faskes,
					'nama_user' => $data->nama_user,
					'email_user' => $data->email_user,
          			'status_user' => $data->status_user,
        		);
			}
		}
		return $hasil;
	}

	public function getProgramById($id){
		$hsl=$this->db->query("SELECT * FROM table_program WHERE id_program='$id'");

		if($hsl->num_rows()>0){
			foreach ($hsl->result() as $data) {
				$hasil=array(
          			'id_program' => $data->id_program,
					'id_user' => $data->id_user,
					'tipe_program' => $data->tipe_program,
					'nama_program' => $data->nama_program,
					'deskripsi_program' => $data->deskripsi_program,
					'tanggal_mulai_program' => $data->tanggal_mulai_program,
					'tanggal_berakhir_program' => $data->tanggal_berakhir_program,
					'lokasi_program' => $data->lokasi_program,
					'target_peserta_program' => $data->target_peserta_program,
					'target_klien_program' => $data->target_klien_program,
					'goals_program' => $data->goals_program,
					'anggaran_program' => $data->anggaran_program,
          			'status_program' => $data->status_program,
        		);
			}
		}
		return $hasil;
	}

	public function getSesiById($id){
		$hsl=$this->db->query("SELECT * FROM table_sesi_program WHERE id_sesi='$id'");

		if($hsl->num_rows()>0){
			foreach ($hsl->result() as $data) {
				$hasil=array(
          			'id_sesi' => $data->id_sesi,
					'id_program' => $data->id_program,
					'tanggal_sesi' => $data->tanggal_sesi,
					'waktu_mulai_sesi' => $data->waktu_mulai_sesi,
					'waktu_berakhir_sesi' => $data->waktu_berakhir_sesi,
					'kuota_sesi' => $data->kuota_sesi,
					'status_sesi' => $data->status_sesi,
        		);
			}
		}
		return $hasil;
	}
}