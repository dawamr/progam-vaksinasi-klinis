<!-- MODAL EDIT FASKES-->
<div class="modal fade edit-faskes" id="testUp" tabindex="-1" role="dialog" aria-labelledby="editFaskes" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="editFaskes">EDIT DATA FASILITAS KESEHATAN / MITRA</h4>
            </div>
            <div class="modal-body">
                <form class="" action="<?= base_url('Dashboard/updateFaskes')?>" method="POST" enctype= multipart/form-data>
                    <input type="hidden" name="id_faskes">
                    <input type="hidden" name="foto_faskes">
                    <div class="form-group m-b-20 row">
                        <div class="col-12">
                            <h5 class="m-t-0 mb-3"><b>Logo Fasilitas Kesehatan</b></h5>
                            <img src="" alt="logo" id="img_temp_faskes" class="img-responsive" width="200">
                            <div class="form-group mt-2">
                                <input type="file" class="filestyle" id="edit_foto_faskes" name="edit_foto_faskes" data-buttonbefore="true" data-btnClass="btn-custom">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row m-b-20">
                        <div class="col-12">
                            <label for="">Nama Fasilitas Kesehatan / Mitra<span class="text-danger"> *</span></label>
                            <input class="form-control" type="text" id="edit_nama_faskes" name="edit_nama_faskes" placeholder="Masukkan Nama Mitra" required>
                        </div>
                    </div>

                    <div class="form-group row m-b-20">
                        <div class="col-12">
                            <label for="">Alamat Lengkap<span class="text-danger"> *</span></label>
                            <textarea id="textarea" class="form-control" rows="1" id="edit_alamat_faskes" name="edit_alamat_faskes" required placeholder="Masukkan Alamat Lengkap"></textarea>
                        </div>
                    </div>
                    
                    <?php
                        $this->db->ORDER_BY('nama_kota_kabupaten', "DESC");
                        $data_kota_kabupaten = $this->db->get('table_kota_kabupaten')->result_array();
                    ?>
                    <div class="form-row  m-b-20">
                        <div class="form-group col-md-12 clearfix">
                            <label for="kota" class="col-form-label">Kota / Kabupaten<span class="text-danger"> *</span></label>
                            <select id="kota" class="form-control select2" name="edit_kota_faskes" required>
                                <option disabled selected value="">Pilih kota / kabupaten</option>
                                <?php foreach($data_kota_kabupaten as $dkk):?>
                                    <option value="<?= $dkk['kode_kota_kabupaten']?>"><?= $dkk['nama_kota_kabupaten']?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row m-b-20">
                        <div class="col-12">
                            <label for="">Email Fasilitas Kesehatan / Mitra<span class="text-danger"> *</span></label>
                            <input class="form-control" type="email" id="edit_email_faskes" name="edit_email_faskes" placeholder="Masukkan Email Mitra" required>
                        </div>
                    </div>
                    <div class="form-group row m-b-20">
                        <div class="col-12">
                            <label for="">No Telp Fasilitas Kesehatan / Mitra<span class="text-danger"> *</span></label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">(+62)</span>
                                </div>
                                <input type="number" class="form-control" id="edit_phone_faskes" name="edit_phone_faskes" placeholder="Nasukkan Nomor Telpon" required>
                            </div>
                        </div>
                    </div>
                    <h6>Titik Koordinat Lokasi</h6>
                    <div class="form-row">
                        <div class="form-group col-md-6 clearfix">
                            <label class="control-label" for="long"> Long</label>
                            <div class="">
                                <input id="long" name="edit_longtitude_faskes" type="text" class="required form-control" required>
                            </div>
                        </div>
                        <div class="form-group col-md-6 clearfix">
                            <label class="control-label " for="lat"> Latitude</label>
                            <div class="">
                                <input id="lat" name="edit_latitude_faskes" type="text" class="required form-control" required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group col-md-3 mx-auto text-center m-t-10 mt-5">
                        <div class="col-12">
                            <button class="btn btn-lg btn-block btn-custom waves-effect waves-light" type="submit" >Update Data</button>
                        </div>
                    </div>

                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- MODAL EDIT ADMINISTRATOR-->
<div class="modal fade edit-admin" tabindex="-1" role="dialog" aria-labelledby="editAdmin" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="editAdmin">EDIT DATA ADMINISTRATOR</h4>
            </div>
            <div class="modal-body">
                <form class="" action="<?= base_url('Dashboard/updateUser')?>" method="POST">
                    <input type="hidden" name="id_user">
                    <div class="form-group m-b-20 row">
                        <div class="col-12">
                            <label for="namaadmin">Nama Administrator<span class="text-danger"> *</span></label>
                            <input class="form-control" type="text" id="namaadmin" name="edit_nama_user" placeholder="Masukkan Nama Administrator">
                        </div>
                    </div>

                    <div class="form-group m-b-20 row">
                        <div class="col-12">
                            <label for="emailaddress">Email address <span class="text-danger"> *</span></label>
                            <input class="form-control" type="email" id="emailaddress" name="edit_email_user" placeholder="Masukkan email">
                        </div>
                    </div>
                    <div class="form-group col-md-3 mx-auto text-center m-t-10">
                        <div class="col-12">
                            <button class="btn btn-lg btn-block btn-custom waves-effect waves-light" type="submit" >Update Data</button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- MODAL CHANGE PASSWORD ADMINISTRATOR-->
<div class="modal fade change-password-admin" tabindex="-1" role="dialog" aria-labelledby="changePasswordAdmin" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="changePasswordAdmin">CHANGE PASSWORD ADMINISTRATOR</h4>
            </div>
            <div class="modal-body">
                <form class="" action="<?= base_url("Dashboard/changePassword")?>" method="POST">
                    <input type="hidden" name="id_user">
                    <div class="form-group m-b-20 row">
                        <div class="col-12">
                            <label for="pass1">Password Lama <span class="text-danger">*</span></label>
                            <input id="passlama" type="password" placeholder="Password Lama" name="old_password" required
                                   class="form-control">
                        </div>
                    </div>

                    <div class="form-group m-b-20 row">
                        <div class="col-12">
                            <label for="pass1">Password Baru <span class="text-danger">*</span></label>
                            <input id="pass1" type="password" placeholder="Password Baru" name="new_password" required
                                   class="form-control">
                        </div>
                    </div>

                    <div class="form-group m-b-20 row">
                        <div class="col-12">
                            <label for="passWord2">Konfirmasi Password Baru <span class="text-danger">*</span></label>
                            <input data-parsley-equalto="#pass1" type="password" required
                                   placeholder="Ulangi Password Baru" class="form-control" id="passWord2" name="new_password1">
                        </div>
                    </div>
                    <div class="form-group col-md-3 mx-auto text-center m-t-10">
                        <div class="col-12">
                            <button class="btn btn-lg btn-block btn-custom waves-effect waves-light" type="submit" >Update Data</button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- MODAL EDIT PROGRAM-->
<div class="modal fade edit-program" tabindex="-1" role="dialog" aria-labelledby="editProgram" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="editProgram">EDIT PROGRAM</h4>
            </div>
            <form action="<?= base_url('Program/editProgram')?>" method="POST">
                <div class="modal-body">
                    <!-- Summernote css -->
                    <link href="<?=base_url()?>assets/plugins/summernote/summernote-bs4.css" rel="stylesheet" />
                    <link href="<?=base_url()?>assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css" rel="stylesheet" />
            
                    <!-- Start Page content -->
                    <div class="content">
                        <div class="row">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="card-box">
                                                <input type="hidden" name="edit_id_user">
                                                <input type="hidden" name="edit_id_program">
                                                <div class="form-group">
                                                    <label>Tipe Program<span class="text-danger">*</span></label>
                                                    <select class="form-control" name="edit_tipe_program">
                                                        <option>Vaksinasi Covid</option>
                                                    </select>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label>Nama Program<span class="text-danger">*</span></label>
                                                    <input type="text" name="edit_nama_program" class="form-control" placeholder="Masukkan Nama Program">
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label>Deskripsi Program</label>
                                                    <textarea class="summernote" name="edit_deskripsi_program">
                                                        <h3>Masukkan Deskripsi</h3>
                                                    </textarea>
                                                </div>
                                        </div> <!-- end card-box -->
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="card-box">
                                            <div class="form-group m-b-20">
                                                <label>Periode Program Acara</label>
                                                <div class="input-daterange input-group" id="date-range">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="row">
                                                                <div class="col-6">
                                                                    <label>Mulai</label>
                                                                    <input type="text" class="form-control" name="edit_program_start_date" id="edit_program_start_date" placeholder="Mulai"/>
                                                                </div>
                                                                <div class="col-6">
                                                                    <label>Berakhir</label>
                                                                    <input type="text" class="form-control" name="edit_program_end_date" id="edit_program_end_date" placeholder="Selesai"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Lokasi Program</label>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" name="lokasi_program" placeholder="Nama Tempat Vaksinasi" required></input>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" name="lokasi_kecamatan_program" placeholder="Kelurahan/Kecamatan Vaksinasi" required></input>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Target Peserta</label>
                                                <div class="tags-default">
                                                    <input type="text" class="form-control" id="target_peserta_program" name="edit_target_peserta_program" placeholder="Masukkan Target Peserta"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Target Klien</label>
                                                <div class="tags-default">
                                                    <input type="text" data-role="tagsinput" name="edit_target_klien_program" placeholder="Tekan [TAB] Untuk menambahkan Target Peserta"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Goals<span class="text-danger">*</span></label>
                                                <div>
                                                    <textarea required class="form-control" name="edit_goals_program"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Anggaran (Rp.)</label>
                                                <input type="text" placeholder="" data-a-sign="Rp. " class="form-control autonumber" name="edit_anggaran_program">
                                                <!-- <span class="font-14 text-muted">Masukkan Nominal Anggaran</span> -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end col -->
                                </div>
                                <!-- end row -->
                            </div>
                        </div>
                        <!-- end row -->
                    </div> <!-- content -->
                </div>
                <div class="modal-footer">
                    <div class="form-group text-right m-b-0">
                        <button class="btn btn-custom waves-effect waves-light" type="submit">
                            Update Data
                        </button>
                        <button type="button" class="btn btn-light waves-effect m-l-5" data-dismiss="modal">
                            Cancel
                        </button>
                    </div>
                </div>
            </form>

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- MODAL HAPUS PROGRAM-->
<div class="modal fade hapus-program" tabindex="-1" role="dialog" aria-labelledby="hapusProgram" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="hapusProgram">HAPUS PROGRAM</h4>
            </div>
            <form class="" action="<?= base_url("Program/hapusProgram")?>" method="POST">
                <div class="modal-body">
                    <input type="hidden" name="id_program">
                    <span>Apakah anda yakin ingin menghapus program ini ?</span>  
                </div>
                <div class="modal-footer">
                    <div class="form-group text-right m-b-0">
                        <button class="btn btn-custom waves-effect waves-light btn-danger" type="submit" >Hapus</button>
                        <button type="button" class="btn btn-light waves-effect m-l-5" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- MODAL DECLINE PESERTA PROGRAM-->
<div class="modal fade decline-peserta-program" tabindex="-1" role="dialog" aria-labelledby="declinePesertaProgram" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="declinePesertaProgram">TOLAK PESERTA</h4>
            </div>
            <form class="" action="<?= base_url("Program/declinePesertaProgram")?>" method="POST">
                <div class="modal-body">
                    <input type="hidden" name="id_koneksi">
                    <input type="hidden" name="link_url">
                    <span>Apakah anda yakin ingin menolak peserta ini ?</span>
                    <div class="form-group row m-b-20 mt-3">
                        <div class="col-12">
                            <label for="">Alasan penolakkan peserta<span class="text-danger"> *</span></label>
                            <textarea id="textarea" class="form-control" rows="1" name="alasan_menolak_peserta" required placeholder="Masukkan alasan *alasan ini akan ditampilkan kepada peserta"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-group text-right m-b-0">
                        <button class="btn btn-custom waves-effect waves-light btn-danger" type="submit" >Tolak</button>
                        <button type="button" class="btn btn-light waves-effect m-l-5" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- MODAL LINK FORMULIR BERAKHIR-->
<div class="modal fade link-formulir-berakhir" tabindex="-1" role="dialog" aria-labelledby="linkFormulirBerakhir" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="linkFormulirBerakhir">LINK FORMULIR BERAKHIR</h4>
            </div>
            <div class="modal-body text-center">
                <span>Periode program telah berakhir !</span>  
            </div>
            <div class="modal-footer">
                <div class="form-group text-right m-b-0">
                    <button type="button" class="btn btn-light waves-effect m-l-5" data-dismiss="modal">Cancel</button>
                </div>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- MODAL HAPUS SESI PROGRAM-->
<div class="modal fade hapus-sesi-program" tabindex="-1" role="dialog" aria-labelledby="hapusSesiProgram" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="hapusSesiProgram">HAPUS SESI</h4>
            </div>
            <form class="" action="<?= base_url("Program/hapusSesiProgram")?>" method="POST">
                <input type="hidden" name="id_sesi_hapus">
                <input type="hidden" name="link_url_hapus_sesi">
                <div class="modal-body text-center">
                    <h4>Apakah anda yakin ingin menghapus sesi ini ?</h4>
                    <br>
                    <span>Tindakan ini tidak dapat diurungkan</span>
                </div>
                <div class="modal-footer">
                    <div class="form-group text-right m-b-0">
                        <button class="btn btn-custom waves-effect waves-light btn-danger" type="submit" >Hapus</button>
                        <button type="button" class="btn btn-light waves-effect m-l-5" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- MODAL RESCHEDULE PESERTA PROGRAM-->
<div class="modal fade reschedule-peserta-program" tabindex="-1" role="dialog" aria-labelledby="reschedulePesertaProgram" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="reschedulePesertaProgram">RESCHEDULE PESERTA</h4>
            </div>
            <form class="" action="<?= base_url("Program/reschedulePesertaProgram")?>" method="POST">
                <div class="modal-body">
                    <input type="hidden" name="id_koneksi">
                    <input type="hidden" name="id_peserta">
                    <input type="hidden" name="link_url">
                    <input type="hidden" name="reschedule_sesi_program_input">
                    <div class="form-group clearfix">
                        <label for="reschedule_sesi_program" class="col-form-label">Sesi Vaksinasi</label>
                        <select id="reschedule_sesi_program" class="form-control" name="reschedule_sesi_program" disabled>
                            <option value="">Pilih Sesi</option>
                            <?php
                                foreach ($data_sesi as $ds) {
                            ?>
                                <option value="<?= $ds['id_sesi']?>">
                                    <?= date("d F Y",strtotime($ds['tanggal_sesi']))?> - 
                                    Jam <?= date("H:i",strtotime($ds['waktu_mulai_sesi']))?> - 
                                    <?= date("H:i",strtotime($ds['waktu_berakhir_sesi']))?>
                                    (Kuota : <?= $ds['kuota_sesi']?>, Tersisa <?= $ds['kuota_sesi'] - $ds['kuota_terdaftar']?>)
                                </option>;
                            <?php
                                }
                            ?>
                        </select>
                    </div>
                    <div class="form-group clearfix">
                        <label for="new_reschedule_sesi_program" class="col-form-label">Sesi Vaksinasi Baru</label>
                        <select id="new_reschedule_sesi_program" class="form-control" name="new_reschedule_sesi_program" required onchange="cekReschedule()">
                            <option value="">Pilih Sesi</option>
                            <?php
                                foreach ($data_sesi as $ds) {
                                    if($ds['status_sesi'] == "active"){
                            ?>
                                        <option value="<?= $ds['id_sesi']?>">
                                            <?= date("d F Y",strtotime($ds['tanggal_sesi']))?> - 
                                            Jam <?= date("H:i",strtotime($ds['waktu_mulai_sesi']))?> - 
                                            <?= date("H:i",strtotime($ds['waktu_berakhir_sesi']))?>
                                            (Kuota : <?= $ds['kuota_sesi']?>, Tersisa <?= $ds['kuota_sesi'] - $ds['kuota_terdaftar']?>)
                                        </option>;
                            <?php
                                    }
                                }
                            ?>
                        </select>
                    </div>

                    <div class="form-group row m-b-20 mt-3">
                        <div class="col-12">
                            <label for="">Alasan reschedule peserta<span class="text-danger"> *</span></label>
                            <textarea id="textarea" class="form-control" rows="1" name="alasan_reschedule_peserta" required placeholder="Masukkan alasan *alasan ini akan ditampilkan kepada peserta"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-group text-right m-b-0">
                        <button class="btn btn-custom waves-effect waves-light btn-info" type="submit" >Reschedule</button>
                        <button type="button" class="btn btn-light waves-effect m-l-5" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- MODAL RESCHEDULE BERAKHIR-->
<div class="modal fade reschedule-berakhir" tabindex="-1" role="dialog" aria-labelledby="rescheduleBerakhir" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="rescheduleBerakhir">RESCHEDULE BERAKHIR</h4>
            </div>
            <div class="modal-body text-center">
                <span>Batasan waktu untuk reschedule telah berakhir !</span>  
            </div>
            <div class="modal-footer">
                <div class="form-group text-right m-b-0">
                    <button type="button" class="btn btn-light waves-effect m-l-5" data-dismiss="modal">Cancel</button>
                </div>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- MODAL RESCHEDULE BERAKHIR-->
<div class="modal fade pie-chart-kuota" tabindex="-1" role="dialog" aria-labelledby="pieChartNama" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="pieChartNama">Informasi Kuota</h4>
            </div>
            <div class="modal-body d-flex justify-content-center">
                <div class="col-md-6">
                    <div class="chart-container">
                        <div class="doughnut-chart-container">
                          <canvas id="doughnut-chartcanvas-2"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-group text-right m-b-0">
                    <button type="button" class="btn btn-light waves-effect m-l-5" data-dismiss="modal">Cancel</button>
                </div>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->