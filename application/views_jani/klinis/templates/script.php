
        <!-- jQuery  -->
        <script src="<?=base_url()?>assets/js/jquery.min.js"></script>
        <script src="<?=base_url()?>assets/js/popper.min.js"></script>
        <script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/js/metisMenu.min.js"></script>
        <script src="<?=base_url()?>assets/js/waves.js"></script>
        <script src="<?=base_url()?>assets/js/jquery.slimscroll.js"></script>

        <!-- plugin js -->
        <script src="<?=base_url()?>assets/plugins/moment/moment.js"></script>
        <script src="<?=base_url()?>assets/plugins/bootstrap-timepicker/bootstrap-timepicker.js"></script>
        <script src="<?=base_url()?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/clockpicker/js/bootstrap-clockpicker.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

        <!-- KNOB JS -->
        <!--[if IE]>
        <script type="text/javascript" src="<?=base_url()?>assets/plugins/jquery-knob/excanvas.js"></script>
        <![endif]
        <script src="<?=base_url()?>assets/plugins/jquery-knob/jquery.knob.js"></script>-->

        <!-- Dashboard Init -->
        <script src="<?=base_url()?>assets/pages/jquery.dashboard.init.js"></script>

        <!-- App js -->
        <script src="<?=base_url()?>assets/js/jquery.core.js"></script>
        <script src="<?=base_url()?>assets/js/jquery.app.js"></script>

        <!-- Init js -->
        <script src="<?=base_url()?>assets/pages/jquery.form-pickers.init.js"></script>

        <script src="<?=base_url()?>assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js"></script>

        <script src="<?=base_url()?>assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/plugins/autoNumeric/autoNumeric.js" type="text/javascript"></script>

        <!--Summernote js-->
        <script src="<?=base_url()?>assets/plugins/summernote/summernote-bs4.min.js"></script>

        <!-- Modal-Effect -->
        <script src="<?=base_url()?>assets/plugins/custombox/js/custombox.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/custombox/js/legacy.min.js"></script>

        <!-- Datatables -->
        <script src="<?=base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.responsive.min.js"></script>

        <?php include "js_dashboard.php"?>
        <?php include "js_program.php"?>

        <script src="<?=base_url()?>assets/js/chart.min.js"></script>
        <script>
        $(function(){

          //get the doughnut chart canvas
          var ctx2 = $("#doughnut-chartcanvas-2");

          //doughnut chart data
          var data2 = {
            labels: ["match1", "match2", "match3", "match4", "match5", "match6"],
            datasets: [
              {
                label: "TeamB Score",
                data: [20, 35, 40, 60, 50, 5],
                backgroundColor: [
                  '#02c0ce','#2d7bf4','#e3eaef','#f1556c',"#f9bc0b", "#49220b"
                ],
                borderColor: [
                  '#02c0ce','#2d7bf4','#e3eaef','#f1556c',"#f9bc0b", "#49220b"
                ],
                borderWidth: [1, 1, 1, 1, 1, 1]
              }
            ]
          };

          //options
          var options = {
            responsive: true,
            title: {
              display: true,
              position: "top",
              text: "Doughnut Chart",
              fontSize: 18,
              fontColor: "#111"
            },
            legend: {
              display: true,
              position: "bottom",
              labels: {
                fontColor: "#333",
                fontSize: 16
              }
            }
          };

          //create Chart class object
          var chart2 = new Chart(ctx2, {
            type: "doughnut",
            data: data2,
            options: options
          });
        });
        </script>
        <script>
            jQuery(document).ready(function(){
                $('.summernote').summernote({
                    height: 350,                 // set editor height
                    minHeight: null,             // set minimum height of editor
                    maxHeight: null,             // set maximum height of editor
                    focus: false                 // set focus to editable area after initializing summernote
                });

                $('.inline-editor').summernote({
                    airMode: true
                });
            });
        </script>

        <script type="text/javascript">
            jQuery(function($) {
                $('.autonumber').autoNumeric('init');
            });
            jQuery.browser = {};
            (function () {
                jQuery.browser.msie = false;
                jQuery.browser.version = 0;
                if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
                    jQuery.browser.msie = true;
                    jQuery.browser.version = RegExp.$1;
                }
            })();
        </script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#datatable').dataTable();
                $('#example').DataTable({
                    "dom":' <"search"f><"top p">rt<"bottom"p><"clear">',
                    "order": [[ 11, "ASC" ]],
                    "lengthMenu": [[10, 25, 50, -1], [5, 25, 50, "All"]]
                });
            });
            
            $(function(){
                $(".flash-messages").delay(2500).fadeOut();
                setTimeout(function(){ 
                    <?php $this->session->set_flashdata('message',"");?>
                }, 3000);
            });
        </script>


        <!-- Script validasi inputan angka -->
        <script type="text/javascript">
            setInputFilter(document.getElementById("target_peserta_program"), function(value) {
                return /^\d*\.?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
            });

            function setInputFilter(textbox, inputFilter) {
                ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
                    textbox.addEventListener(event, function() {
                        if (inputFilter(this.value)) {
                            this.oldValue = this.value;
                            this.oldSelectionStart = this.selectionStart;
                            this.oldSelectionEnd = this.selectionEnd;
                        } else if (this.hasOwnProperty("oldValue")) {
                            this.value = this.oldValue;
                            this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                        } else {
                            this.value = "";
                        }
                    });
                });
            }
        </script>

    </body>
</html>