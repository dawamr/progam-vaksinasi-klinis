<!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">

                <div class="slimscroll-menu" id="remove-scroll">

                    <!-- User box -->
                    <div class="user-box text-center"><!-- 
                        <div class="user-img text-center">
                            <img src="<?=base_url()?>assets/images/users/avatar-1.jpg" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid">
                        </div> -->
                        <img src="<?= base_url("assets/logo_mitra/").$this->session->userdata['logo_faskes']?>" alt="logo" height="80" width="80" class="img-responsive"  style="object-fit:scale-down">
                        <h5><a href="#"><?= $data_user['nama_user']?></a> </h5>
                        <p class="text-muted"><?= $data_user['email_user']?></p>
                    </div>

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">

                        <ul class="metismenu" id="side-menu">

                            <li class="menu-title">Menu</li>

                            <li>
                                <a href="<?=base_url()?>dashboard" class="">
                                    <i class="fi-air-play"></i> <span> Dashboard </span><span class="menu-arrow"></span>
                                </a>
                            </li>
                            <li>
                                <a href="<?=base_url()?>program"class="">
                                    <i class="fi-layers"></i> <span> Program </span><span class="menu-arrow"></span>
                                </a>
                            </li>

                        </ul>

                        <!-- LOGO -->
                        <div class="text-center mt-5">
                            <p class="mt-5 text-muted">Powered by</p>
                            <p class="mt-5 d-block d-md-none">Klinis</p>
                            <a href="https://klinis.id" target="_BLANK" class="logo">
                                <span>
                                    <img src="<?=base_url()?>assets/images/logo.png" alt="" height="30">
                                </span>
                                <i>
                                    <img src="<?=base_url()?>assets/images/logo_sm.png" alt="" height="28">
                                </i>
                            </a>
                        </div>

                    </div>
                    <!-- Sidebar -->

                    <div class="clearfix"></div>

                </div>
                <!-- Sidebar -left -->

            </div>
            <!-- Left Sidebar End -->


        <div class="content-page">

                <!-- Top Bar Start -->
                <div class="topbar">

                    <nav class="navbar-custom">

                        <ul class="list-unstyled topbar-right-menu float-right mb-0">

                            <li class="dropdown notification-list">
                                <a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button"
                                   aria-haspopup="false" aria-expanded="false">
                                    <img src="<?=base_url("assets/logo_mitra/").$this->session->userdata('logo_faskes')?>" alt="user" class="rounded-circle" style="object-fit:scale-down"> <span class="ml-1"><?= $data_user['nama_user']?> <i class="mdi mdi-chevron-down"></i> </span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown ">
                                    <!-- item-->
                                    <div class="dropdown-item noti-title">
                                        <h6 class="text-overflow m-0">Welcome !</h6>
                                    </div>

                                    <!-- item-->
                                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                                        <i class="fi-cog"></i> <span>Settings</span>
                                    </a>

                                    <!-- item-->
                                    <a href="<?= base_url('Dashboard/Logout')?>" class="dropdown-item notify-item">
                                        <i class="fi-power"></i> <span>Logout</span>
                                    </a>

                                </div>
                            </li>
                        </ul>

                        <ul class="list-inline menu-left mb-0">
                            <li class="float-left">
                                <button class="button-menu-mobile open-left disable-btn">
                                    <i class="dripicons-menu"></i>
                                </button>
                            </li>
                            <li>
                                <div class="page-title-box">
                                    <h4 class="page-title"><?= $page_title?></h4>
                                </div>
                            </li>

                        </ul>

                    </nav>

                </div>
                <!-- Top Bar End -->