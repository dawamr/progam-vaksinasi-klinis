<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><?=$title?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="Dashboard Klinis for Partners." name="description" />
        <meta content="PT Kreasi Layanan Medis (Klinis)" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="<?=base_url();?>assets/images/favicon.ico">

        <!-- Plugins css-->
        <link href="<?=base_url();?>assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css" rel="stylesheet" />
        <link href="<?=base_url();?>assets/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />
        <link href="<?=base_url();?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?=base_url();?>assets/plugins/switchery/switchery.min.css">

        <!-- App css -->
        <link href="<?=base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="<?=base_url();?>assets/js/modernizr.min.js"></script>

    </head>


    <body class="account-pages">

        <!-- Begin page -->
        <div class="accountbg d-flex justify-content-center align-items-center" style="/*background: url('<?=base_url();?>assets/images/bg-1.jpg');*/background-size: cover;"></div>

        <div class="wrapper-page col-md-8 mx-auto mt-5">

            <div class="card">
                <div class="card-block">

                    <div class="account-box">

                        <div class="card-box p-5">
                            <h2 class="text-uppercase text-center pb-5">
                                <a href="<?=base_url();?>" class="text-success">
                                    <span><img src="<?=base_url();?>assets/images/logo.png" alt="" height="50"></span>
                                </a>
                            </h2>

                            <form action="<?=base_url("auth/registerMitra");?>" method="POST" enctype= multipart/form-data>
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="m-t-0 mt-md-2 mb-3"><b>DATA FASILITAS KESEHATAN / MITRA</b></h5>
                                        <div class="form-group row m-b-20">
                                            <div class="col-12">
                                                <label for="">Nama Fasilitas Kesehatan / Mitra<span class="text-danger"> *</span></label>
                                                <input class="form-control" type="text" name="nama_mitra" placeholder="Masukkan Nama Mitra" required>
                                            </div>
                                        </div>

                                        <div class="form-group row m-b-20">
                                            <div class="col-12">
                                                <label>Tipe Fasilitas Kesehatan / Mitra<span class="text-danger"> *</span></label>
                                                <select class="form-control" name="tipe_mitra" required>
                                                    <option value="" disabled selected>Pilih Tipe Faskes / Mitra</option>
                                                    <option value="Klinik Umum / Mandiri">Klinik Umum / Mandiri</option>
                                                    <option value="Rumah Sakit">Rumah Sakit</option>
                                                    <option value="Organisasi">Organisasi</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row m-b-20">
                                            <div class="col-12">
                                                <label for="">Alamat Lengkap<span class="text-danger"> *</span></label>
                                                <textarea id="textarea" class="form-control" name="alamat_mitra" rows="1" required placeholder="Masukkan Alamat Lengkap"></textarea>
                                            </div>
                                        </div>
                                        <?php
                                            $this->db->ORDER_BY('nama_kota_kabupaten', "ASC");
                                            $data_kota_kabupaten = $this->db->get('table_kota_kabupaten')->result_array();
                                        ?>
                                        <div class="form-row  m-b-20">
                                            <div class="form-group col-md-12 clearfix">
                                                <label for="kota" class="col-form-label">Kota / Kabupaten<span class="text-danger"> *</span></label>
                                                <select id="kota" class="form-control select2" name="kota_kabupaten_mitra" required>
                                                    <option disabled selected value="">Pilih kota / kabupaten</option>
                                                    <?php foreach($data_kota_kabupaten as $dkk):?>
                                                        <option value="<?= $dkk['kode_kota_kabupaten']?>"><?= $dkk['nama_kota_kabupaten']?></option>
                                                    <?php endforeach;?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row m-b-20">
                                            <div class="col-12">
                                                <label for="">Email Fasilitas Kesehatan / Mitra<span class="text-danger"> *</span></label>
                                                <input class="form-control" type="email" name="email_mitra" placeholder="Masukkan Email Mitra" required>
                                            </div>
                                        </div>
                                        <div class="form-group row m-b-20">
                                            <div class="col-12">
                                                <label for="">No Telp Fasilitas Kesehatan / Mitra<span class="text-danger"> *</span></label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon1">(+62)</span>
                                                    </div>
                                                    <input type="number" class="form-control" name="phone_mitra" placeholder="Nasukkan Nomor Telpon" required>
                                                </div>
                                            </div>
                                        </div>
                                        <h6>Titik Koordinat Lokasi</h6>
                                        <div class="form-row">
                                            <div class="form-group col-md-12 clearfix">
                                                Silahkan baca
                                                <a href="void:javascript()" class="text-custom" id="tutorial_titik_koordinasi"> Tutorial </a>
                                                untuk pengambilan titik koordinasi
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6 clearfix">
                                                <label class="control-label" for="long"> Longtitude</label>
                                                <div class="">
                                                    <input id="long" name="longtitude_mitra" type="text" class="required form-control" required>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6 clearfix">
                                                <label class="control-label " for="lat"> Latitude</label>
                                                <div class="">
                                                    <input id="lat" name="latitude_mitra" type="text" class="required form-control" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <h5 class="m-t-0 mt-5 mt-md-2 mb-3"><b>DATA USER LOGIN</b></h5>
                                        <div class="form-group m-b-20 row">
                                            <div class="col-12">
                                                <label for="namaadmin">Nama Administrator<span class="text-danger"> *</span></label>
                                                <input class="form-control" type="text" id="namaadmin" name="nama_user" placeholder="Masukkan Nama Administrator" required>
                                            </div>
                                        </div>

                                        <div class="form-group m-b-20 row">
                                            <div class="col-12">
                                                <label for="emailaddress">Email address<span class="text-danger"> *</span></label>
                                                <input class="form-control" type="email" id="emailaddress" name="email_user" placeholder="Masukkan email" required>
                                            </div>
                                        </div>

                                        <div class="form-group m-b-20 row">
                                            <div class="col-12">
                                                <label for="pass1">Password<span class="text-danger">*</span></label>
                                                <input id="pass1" type="password" name="password_user" placeholder="Password" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="form-group m-b-20 row">
                                            <div class="col-12">
                                                <label for="passWord2">Konfirmasi Password <span class="text-danger">*</span></label>
                                                <input data-parsley-equalto="#pass1" type="password" name="password_confirm_user" required
                                                       placeholder="Password" class="form-control" id="passWord2">
                                            </div>
                                        </div>
                                        <div class="form-group m-b-20 row">
                                            <div class="col-12">
                                                <h5 class="m-t-0 mt-5 mb-3"><b>Logo Fasilitas Kesehatan</b></h5>
                                                <img src="https://dash.klinis.id/assets/images/users/avatar-1.jpg" alt="logo" class="img-responsive" height="100" id="logo_mitra_temp">
                                                <div class="form-group mt-2">
                                                    <input type="file" class="filestyle" name="logo_mitra" id="logo_mitra" required data-buttonbefore="true" data-btnClass="btn-custom">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row col-12 text-center mt-4 mx-auto m-b-20">
                                    <div class="col-12">

                                        <div class="checkbox checkbox-custom">
                                            <input id="setujusk" type="checkbox" required>
                                            <label for="setujusk">
                                                Saya Setuju dengan Syarat dan Ketentuan dalam bermitra dengan Klinis
                                            </label>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group col-md-3 mx-auto text-center m-t-10">
                                    <div class="col-12">
                                        <button class="btn btn-lg btn-block btn-custom waves-effect waves-light" type="submit" >Daftar</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>

                </div>
            </div>

            <div class="m-t-40 text-center">
                <p class="account-copyright">Powered by <b>Klinis</b> @ <b>PT. Kreasi Layanan Medis </b></p>
            </div>

        </div>


        <!-- MODAL Tutorial Titik Koordinasi-->
        <div class="modal fade tutorial-titik-koordinasi" tabindex="-1" role="dialog" aria-labelledby="tutorialTitikKoordinasi" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title" id="tutorialTitikKoordinasi">TUTORIAL MENDAPATKAN TITIK KOORDINASI</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-12">
                                <span>1. Buka <a href="https://maps.google.com" target="_blank">https://maps.google.com</a></span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <span>2. Masukkan alamat / lokasi pada inputan "Telusuri Google Maps", selanjutnya klik pada tombol "Telusuri" atau tekan "Enter"</span>
                                
                                
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6">
                                <img src="<?= base_url("assets/tutorialkoordinat/tutorial1.png")?>" height="120" width="350">
                            </div>
                            <div class="col-6">
                                <img src="<?= base_url("assets/tutorialkoordinat/tutorial2.png")?>" height="120" width="350">
                                
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-12">
                                <span>3. Apabila alamat tidak muncul, Masukkan alamat lokasi lain yang berdekatan dengan lokasi faskes, kemudian klik pada posisi koordinat yang merupakan alamat faskes. Setelah di klik akan muncul titik GPS dan kotak kecil dibagian bawah yang terdapat angka Longtitude & Latitude dan salin angka pada bagian bawah yang dipisahkan oleh , (koma)</span>
                            </div>
                            <div class="col-12 text-center">
                                <img src="<?= base_url("assets/tutorialkoordinat/tutorial3.png")?>" height="200" width="450">
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-12">
                                <span>4. Apabila alamat muncul, akan ada tampilan titik GPS merah pada maps dan silahkan klik kanan pada titik GPS tersebut. Setelah klik kanan, akan muncul angka titik koordinasi Longtitude & Latitude</span>
                            </div>
                            <div class="col-12 text-center">
                                <img src="<?= base_url("assets/tutorialkoordinat/tutorial4.png")?>" height="150" width="450">
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-12">
                                <span>5. angka sebelum , (koma) merupakan titik Longtitude (-6.19060), sedangkan angka setelah , (koma) merupakan titik Latitude (106.73791)</span>
                            </div>
                        </div>
                        
                    </div>
                    <div class="modal-footer">
                        <div class="form-group text-right m-b-0">
                            <button type="button" class="btn btn-light waves-effect m-l-5" data-dismiss="modal">TUTUP</button>
                        </div>
                    </div>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <!-- jQuery  -->
        <script src="<?=base_url();?>assets/js/jquery.min.js"></script>
        <script src="<?=base_url();?>assets/js/popper.min.js"></script>
        <script src="<?=base_url();?>assets/js/bootstrap.min.js"></script>
        <script src="<?=base_url();?>assets/js/metisMenu.min.js"></script>
        <script src="<?=base_url();?>assets/js/waves.js"></script>
        <script src="<?=base_url();?>assets/js/jquery.slimscroll.js"></script>

        <script src="<?=base_url();?>assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js" type="text/javascript"></script>

        <!-- App js -->
        <script src="<?=base_url();?>assets/js/jquery.core.js"></script>
        <script src="<?=base_url();?>assets/js/jquery.app.js"></script>
        <script type="text/javascript">
            $("#tutorial_titik_koordinasi").click(function(){
                $(".tutorial-titik-koordinasi").modal("show");
            });
            logo_mitra.onchange = evt => {
              const [file] = logo_mitra.files
              if (file) {
                logo_mitra_temp.src = URL.createObjectURL(file)
              }
            }

        </script>
    </body>
</html>