<?php 
    $replace_url_nama = str_replace(" ", "-", $data_program['nama_program']);
    $replace_url_tanggal = str_replace("-", "", $data_program['tanggal_mulai_program']);

    $tanggal_mulai_program = strtotime($data_program['tanggal_mulai_program']);
    $tanggal_berakhir_program = strtotime($data_program['tanggal_berakhir_program']); 

    $tanggal_sekarang = strtotime(date('Y-m-d'));

    $jarak = $tanggal_berakhir_program - $tanggal_mulai_program;
    $hari = $jarak / 60 / 60 / 24;


    $jarak_program_mulai = $tanggal_mulai_program - $tanggal_sekarang;
    $jarak_program_berakhir = $tanggal_berakhir_program - $tanggal_sekarang;
    $hari_akan_mulai = $jarak_program_mulai / 60 / 60 / 24;
    $hari_akan_berakhir = $jarak_program_berakhir / 60 / 60 / 24;
?>
<div class="content">
    <div class="container-fluid">
        <div class="row mt-3">
            <div class="col-12 rounded">
                <div class="card-box pt-0 px-0 pb-5">
                    <span class="mb-4 font-weight-bold px-4 px-md-5 py-3 bg-custom text-light rounded">Program Yang Sedang Berlangsung</span>
                    <div class="px-md-5 mt-5">
                        <div class="card-box shadow-sm pt-0 px-0 pb-5">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card-box">
                                        <div class="row">
                                            <div class="col- mx-3">
                                                <h3 class=""><a href="<?=base_url()?>program/vaksinasi" class="text-custom"><i class="fa fa-chevron-left mr-3"></i></a><?= $data_program['nama_program']?></h3>
                                            </div>
                                            <div class="col- ml-auto mx-3 mt-2">
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn btn-outline-custom dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false" ><i class="fa fa-share-alt"></i> Share</button>
                                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 35px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                        <a class="dropdown-item py-3" href="#"><i class="fa fa-copy"></i> Copy Link Pendaftaran</a>
                                                        <a class="dropdown-item py-3" href="#"><i class="fa fa-whatsapp"></i> Whatsapp</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="">
                                <nav class="navbar navbar-expand-lg navbar-dark bg-custom">
                                  <a class="navbar-brand d-block d-md-none" href="#">Menu</a>
                                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon"></span>
                                  </button>

                                  <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0 m-auto py-2 d-flex justify-content-center">
                                      <li class="nav-item mx-4 py-2">
                                        <a class="nav-link" href="<?= base_url('Program/List/').$replace_url_nama."/".$data_program['id_program'].$replace_url_tanggal?>"><i class="fa fa-dashboard"></i> Dashboard</a>
                                      </li>
                                      <li class="nav-item mx-4 py-2 active">
                                        <a class="nav-link" href="<?= base_url('Program/Jadwal/').$replace_url_nama."/".$data_program['id_program'].$replace_url_tanggal?>"><i class="fa fa-calendar"></i> Atur Jadwal Program</a>
                                      </li>
                                      <li class="nav-item mx-4 py-2">
                                        <a class="nav-link" href="<?= base_url('Program/Peserta/').$replace_url_nama."/".$data_program['id_program'].$replace_url_tanggal?>"><i class="fa fa-users"></i> Peserta</a>
                                      </li>
                                      <li class="nav-item mx-4 py-2">
                                        <a class="nav-link" href="<?= base_url('Program/AktivasiPeserta/').$replace_url_nama."/".$data_program['id_program'].$replace_url_tanggal?>"><i class="fa fa-check-square-o"></i> <b>Aktivasi Peserta</b></a>
                                      </li>
                                      <li class="nav-item mx-4 py-2 px-4 bg-white rounded">
                                        <?php
                                            $replace_url_nama = str_replace(" ", "-", $data_program['nama_program']);
                                            $replace_url_tanggal = str_replace("-", "", $data_program['tanggal_mulai_program']);
                                        ?>
                                        <?php
                                            if($hari_akan_berakhir > 0){
                                        ?>
                                            <a class="nav-link text-custom" 
                                            href="<?=base_url("Program/registrasi/").$replace_url_nama."/".$data_program['id_program'].$replace_url_tanggal?>" target="_blank">
                                            <b><i class="fa fa-paste"></i> Link Formulir Pendaftaran</b></a>

                                        <?php }else{?>
                                            <a class="btn nav-link text-custom" id="btn_link_formulir_berakhir">                                                
                                                <b><i class="fa fa-paste"></i> Link Formulir Pendaftaran</b>
                                            </a>
                                        <?php }?>
                                      </li>
                                    </ul>
                                  </div>
                                </nav>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="card-box rounded">
                                        <div class="widget-chart-one-content p-4 p-md-5 shadow-sm text-left">
                                            <div class="row mt-4 mb-2">
                                                <div class="col-md-8">
                                                    <h3 class=""><?= $data_program['nama_program']?></h3>
                                                </div>
                                                <div class="col- ml-auto mx-3 mt-2">

                                                    <?php
                                                        if($hari_akan_mulai > 0 && $hari_akan_berakhir > 0){
                                                    ?>
                                                        <p class="px-4 py-2 text-light bg-info btn-rounded waves-light waves-effect">
                                                            Akan Berlangsung
                                                        </p>
                                                    <?php
                                                        }else if($hari_akan_mulai <= 0 && $hari_akan_berakhir > 0){
                                                    ?>
                                                        <p class="px-4 py-2 text-light bg-success btn-rounded waves-light waves-effect">
                                                            Sedang Berlangsung
                                                        </p>
                                                    <?php
                                                        }else{
                                                    ?>
                                                        <p class="px-4 py-2 text-light bg-danger btn-rounded waves-light waves-effect">
                                                            Telah Berakhir
                                                        </p>
                                                    <?php
                                                        }
                                                    ?>
                                                </div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <h5>Tanggal / Waktu</h5>
                                                    <p class="">
                                                        <?= date("l, d F Y", strtotime($data_program['tanggal_mulai_program']))?>
                                                        - 
                                                        <?= date("l, d F Y", strtotime($data_program['tanggal_berakhir_program']))?>
                                                        <span class="text-custom">(<?= $hari?> Hari)</span>
                                                    </p>
                                                </div>
                                                <div class="col-md-4">
                                                    <h5>Lokasi</h5>
                                                    <p><a href="https://www.google.com/maps/search/<?= $data_program['lokasi_program']?>" target="_blank" class="text-custom"><i class="fa fa-map-o"></i> <?= $data_program['lokasi_program']?></a></p>
                                                </div>
                                                <div class="col-md-4">
                                                    <h5>Penyelenggara</h5>
                                                    <img src="<?= base_url('assets/logo_mitra/').$data_faskes['logo_mitra']?>" alt="logo" style="object-fit:scale-down; max-width:250px; max-height:100px;" class="img-responsive">
                                                    <h4><?= $data_faskes['nama_faskes']?></h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="card-box">
                                        <h4 class="header-title">Pengaturan Jadwal Program</h4>
                                        <form action="<?= base_url('Program/tambahSesiProgram')?>" method="POST" id="form_pengaturan_sesi_program">
                                            <input type="hidden" name="id_sesi" value="">
                                            <input type="hidden" name="id_program_sesi" value="<?= $data_program['id_program']?>">
                                            <input type="hidden" name="link_url" value="<?= $replace_url_nama.'/'.$data_program['id_program'].$replace_url_tanggal?>">
                                            <input type="hidden" name="edit_kuota" value="">
                                            <div class="text-center mt-4 mb-4">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                    <div class="card-box widget-flat border-info bg-info text-white">
                                                                        <h3 class="m-b-10">{{Target Kuota}}</h3>
                                                                        <p class="text-uppercase m-b-5 font-13 font-600">Target Kuota</p>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="card-box bg-warning widget-flat border-warning text-white">
                                                                        <!-- <h3 class="m-b-10"><?= count($data_peserta_program)?></h3> -->
                                                                        <h3 class="m-b-10">{{Kuota Terdaftar}}</h3>
                                                                        <p class="text-uppercase m-b-5 font-13 font-600">Kuota Terdaftar</p>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="card-box widget-flat border-danger bg-danger text-white">
                                                                        <h3 class="m-b-10">{{Sisa Kuota}}</h3>
                                                                        <p class="text-uppercase m-b-5 font-13 font-600">Sisa Kuota</p>
                                                                    </div>
                                                                </div>
                                                            <div class="col-12">
                                                                <div class="flash-messages">
                                                                    <?= $this->session->flashdata('message')?>
                                                                </div>
                                                            </div>
                                                            <div class="col-12">
                                                                <h4 class="mb-4">Tambahkan Sesi Program</h4>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <span><b>Nama Sesi</b></span>
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <input class="form-control" type="text" placeholder="Nama Sesi (isi jika perlu)" name="nama_jadwal_program_mulai" id="nama_jadwal_program_mulai" required/>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <span><b>Tanggal</b></span>
                                                                    <input type="date" class="form-control" name="tanggal_jadwal_program" id="tanggal_jadwal_program"
                                                                    min="<?= $data_program['tanggal_mulai_program']?>"
                                                                    max="<?= $data_program['tanggal_berakhir_program']?>"
                                                                    value="<?= $data_program['tanggal_mulai_program']?>"/>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <span><b>Waktu</b></span>
                                                                <div class="row">
                                                                    <div class="col-6">
                                                                        <div class="form-group">
                                                                            <div class="input-group">
                                                                                <input class="form-control" name="waktu_jadwal_program_mulai" id="waktu_jadwal_program_mulai" required/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <div class="form-group">
                                                                            <div class="input-group">
                                                                                <input class="form-control" name="waktu_jadwal_program_berakhir" id="waktu_jadwal_program_berakhir" required/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1 ml-auto">
                                                                <div class="form-group">
                                                                    <span><b>Kuota</b></span>
                                                                    <input type="text" class="form-control" name="kuota_jadwal_program" id="kuota_jadwal_program" required />
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <br>
                                                                    <button type="button" class="btn btn-success rounded" id="btn_submit_form_pengaturan_sesi">Submit</button>
                                                                    <button type="button" class="btn btn-danger rounded" onclick="location.reload();">Reset</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="table-rep-plugin">
                                                        <div class="table-responsive">
                                                            <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.1/css/jquery.dataTables.min.css">
                                                            <table id="example" class="table table-striped" style="width:100%">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Sesi</th>
                                                                        <th>Nama Sesi</th>
                                                                        <th>Tanggal Sesi</th>
                                                                        <th>Waktu Sesi</th>
                                                                        <th>Kuota Sesi</th>
                                                                        <th>Status</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php
                                                                    $i = 1;
                                                                    $total_kuota_sesi_terdaftar = 0;
                                                                    foreach($data_sesi as $ds){
                                                                        if($ds['status_sesi'] == "active"){
                                                                ?>
                                                                    <tr>
                                                                        <td><?= $i?></td>
                                                                        <td>Nama Sesi</td>
                                                                        <td><?= date('d F Y', strtotime($ds['tanggal_sesi']))?></td>
                                                                        <td><?= date('H:i', strtotime($ds['waktu_mulai_sesi'])). " - ".date('H:i', strtotime($ds['waktu_berakhir_sesi']))?></td>
                                                                        <td><?= $ds['kuota_sesi']?></td>
                                                                        <td>
                                                                            <button type="button" class="btn btn-warning rounded btn_edit_sesi" data-id_sesi="<?= $ds['id_sesi']?>">
                                                                                <i class="fa fa-edit"></i>
                                                                            </button>
                                                                            <?php
                                                                                if($hari_akan_mulai > 0){
                                                                            ?>
                                                                            <button type="button" class="btn btn-danger rounded btn_delete_sesi" data-id_sesi="<?= $ds['id_sesi']?>" data-link_url="<?= $replace_url_nama.'/'.$data_program['id_program'].$replace_url_tanggal?>">
                                                                                <i class="fa fa-trash"></i>
                                                                            </button>
                                                                            <?php }?>
                                                                        </td>
                                                                    </tr>
                                                                <?php
                                                                            $total_kuota_sesi_terdaftar+=$ds['kuota_sesi'];
                                                                            $i++;
                                                                        }
                                                                    }
                                                                ?>
                                                                    
                                                                </tbody>
                                                                <tfoot>
                                                                    <tr class="table-info">
                                                                        <td colspan="4" class="font-weight-bold">Total Kuota Yang Terdaftar</td>
                                                                        <td class="font-weight-bold"><?= $total_kuota_sesi_terdaftar?></td>
                                                                        <td></td>
                                                                    </tr>
                                                                </tfoot>
                                                            </table>
                                                            
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div><!-- end col -->


                                <div class="col-12">
                                    <div class="card-box">
                                        <form action="<?= base_url('Program/tambahSesiProgram')?>" method="POST" id="form_pengaturan_sesi_program">
                                            <div class="row d-flex justify-content-center">
                                                <div class="col-md-6">

                                                    <div class="card-box shadow-sm">
                                                        <h4 class="header-title">Jenis Vaksin</h4>
                                                        <p class="text-danger">
                                                            Belum ada Jenis Vaksin yang tersedia <button type="button" class="btn btn-outline-custom waves-light waves-effect" onClick="window.location.href='<?= base_url()?>program/tambahprogram'"><b>+</b> Tambah Jenis Vaksinasi</button>
                                                        </p>

                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div><!-- end col -->


                            </div>
                            <!-- end row -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>