<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Registrasi Program Vaksinasi - <?= $data_program['nama_program']?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="Dashboard Klinis for Partners." name="description" />
        <meta content="PT Kreasi Layanan Medis (Klinis)" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="<?=base_url();?>assets/images/favicon.ico">

        <!--Form Wizard-->
        <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/plugins/jquery.steps/css/jquery.steps.css" />

        <!-- Sweet Alert css -->
        <link href="<?=base_url();?>assets/plugins/sweet-alert/sweetalert2.min.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="<?=base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="<?=base_url();?>assets/js/modernizr.min.js"></script>

    </head>


    <body>
<?php 
    $replace_url_nama = str_replace(" ", "-", $data_program['nama_program']);
    $replace_url_tanggal = str_replace("-", "", $data_program['tanggal_mulai_program']);

    $tanggal_mulai_program = strtotime($data_program['tanggal_mulai_program']);
    $tanggal_berakhir_program = strtotime($data_program['tanggal_berakhir_program']); 

    $tanggal_sekarang = strtotime(date('Y-m-d'));

    $jarak = $tanggal_berakhir_program - $tanggal_mulai_program;
    $hari = $jarak / 60 / 60 / 24;


    $jarak_program_mulai = $tanggal_mulai_program - $tanggal_sekarang;
    $jarak_program_berakhir = $tanggal_berakhir_program - $tanggal_sekarang;
    $hari_akan_mulai = $jarak_program_mulai / 60 / 60 / 24;
    $hari_akan_berakhir = $jarak_program_berakhir / 60 / 60 / 24;

    $arr_target_klien = explode (",",$data_program['target_klien_program']);
?>
        <!-- Begin page -->
        <div id="wrapper">


            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->

            <nav class="navbar navbar-light bg-white">
                <div class="page-title-box">
                    <a class="py-3 px-4" href="#"><img src="<?=base_url()?>assets/images/logo.png" alt="" height="40"></a>
                    <h5 class="ml-4"><?= $data_program['nama_program']?></h5>
                </div>
                <!-- <button type="button" class="btn btn-lg btn-success waves-light waves-effect">Login / Register</button> -->
            </nav>

            <div class="container">
                <!-- Start Page content -->
                <div class="content mt-5">
                    <div class="container-fluid mt-5">

                        <!-- Basic Form Wizard -->
                        <div class="row">
                            <div class="col-12">
                                <div class="card-box shadow-sm rounded p-4 p-md-5">
                                    <div class="widget-chart-one-content text-left">
                                        <div class="row mt-4 mb-2">
                                            <div class="col-md-8">
                                                <h3 class=""><?= $data_program['nama_program']?></h3>
                                            </div>
                                            <div class="col- ml-auto mx-3 mt-2">
                                                <?php
                                                        if($hari_akan_mulai > 0 && $hari_akan_berakhir > 0){
                                                    ?>
                                                        <p class="px-4 py-2 mt-3 mr-3 text-light bg-info btn-rounded waves-light waves-effect">
                                                            Akan Berlangsung
                                                        </p>
                                                    <?php
                                                        }else if($hari_akan_mulai <= 0 && $hari_akan_berakhir > 0){
                                                    ?>
                                                        <p class="px-4 py-2 mt-3 mr-3 text-light bg-success btn-rounded waves-light waves-effect">
                                                            Sedang Berlangsung
                                                        </p>
                                                    <?php
                                                        }else{
                                                    ?>
                                                        <p class="px-4 py-2 mt-3 mr-3 text-light bg-danger btn-rounded waves-light waves-effect">
                                                            Telah Berakhir
                                                        </p>
                                                    <?php
                                                        }
                                                    ?>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn btn-outline-custom dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false" ><i class="fa fa-share-alt"></i> Share</button>
                                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 35px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                        <a class="dropdown-item py-3" href="void::javascript(0)" id="copy_link_url"><i class="fa fa-copy"></i> Copy Link Pendaftaran</a>
                                                        <a class="dropdown-item py-3" href="#"><i class="fa fa-whatsapp"></i> Whatsapp</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5>Tanggal / Waktu</h5>
                                                <p class="">
                                                    <?= date("l, d F Y", strtotime($data_program['tanggal_mulai_program']))?>
                                                    - 
                                                    <?= date("l, d F Y", strtotime($data_program['tanggal_berakhir_program']))?>
                                                    <span class="text-custom">(<?= $hari?> Hari)</span>
                                                </p>
                                            </div>
                                            <div class="col-md-4">
                                                <h5>Lokasi</h5>
                                                <p class=""><i class="fa fa-map-o"></i> <?= $data_program['lokasi_program']?></p>
                                            </div>
                                            <div class="col-md-4">
                                                <h5>Penyelenggara</h5>
                                                <img src="<?= base_url("assets/logo_mitra/").$data_faskes['logo_mitra']?>" alt="logo" height="50" class="img-responsive">
                                                <p><?= $data_faskes['nama_faskes']?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="m-t-0 header-title"><b>Catatan Penting!</b></h4>
                                    <p class="text-muted m-b-30 font-13">
                                        Harap isi data diri anda dengan benar dan periksa kembali sebelum mengirimkan data untuk menghindari kesalahan pengisian data !
                                    </p>
                                    <p class="text-muted m-b-30 font-13">
                                        Jika anda telah mendaftar, silahkan 
                                        <a href="<?= base_url('Program/CekPeserta/').$replace_url_nama.'/'.$data_program['id_program'].$replace_url_tanggal?>"> klik disini </a>
                                        untuk pengecekkan data !
                                    </p>

                                    <div class="">
                                        <div class="flash-messages">
                                            <?= $this->session->flashdata('message')?>
                                        </div>
                                        <form id="form_registrasi_peserta" action="<?= base_url('Program/registrasiPeserta')?>" method="POST">
                                            <input type="hidden" name="url_pendaftaran_peserta" value="<?= $replace_url_nama.'/'.$data_program['id_program'].$replace_url_tanggal?>">
                                            <div>
                                                <div class="form-group clearfix">
                                                    <label class="control-label ">(<span class="text-danger"> * </span>) Wajib Diisi</label>
                                                </div>

                                                <h3>Data Pendaftaran</h3>
                                                <section>
                                                    <div class="form-group clearfix">
                                                        <label class="control-label " for="nik">
                                                            NIK <span class="text-danger"> *</span>
                                                        </label>
                                                        <div class="">
                                                            <span id="span_nik_peserta_error" class="text-danger"></span>
                                                            <input id="nik" name="nik_peserta" type="text" class="form-control" required>
                                                            <span class="help-block"><small>NIK Wajib diisi untuk validasi data diri personal.</small></span>
                                                            <br>
                                                            <span id="span_nik_peserta_valid" class="text-success"></span>
                                                            <span id="span_nik_peserta_invalid" class="text-danger"></span>
                                                        </div>
                                                            <button type="button" class="btn btn-warning" id="btn-check-nik">Cek NIK</button>
                                                    </div>
                                                    
                                                    
                                                </section>

                                                <section id="data-diri-registrasi">
                                                    <div class="form-group clearfix">
                                                        <label for="sesi_program" class="col-form-label">
                                                            Sesi Program <span class="text-danger"> *</span>
                                                        </label>
                                                        <div class="">
                                                            <span id="span_tanggal_waktu_peserta_error" class="text-danger"></span>
                                                            <select id="sesi_program" class="form-control" name="sesi_program" required>
                                                                <option value="">Pilih Sesi</option>
                                                                <?php
                                                                    foreach ($data_sesi as $ds) {
                                                                        if($ds['status_sesi'] == "active"){
                                                                            if($ds['kuota_sesi'] - $ds['kuota_terdaftar'] <= 0){
                                                                ?>
                                                                                <option disabled value="<?= $ds['id_sesi']?>">
                                                                                    <?= date("d F Y",strtotime($ds['tanggal_sesi']))?> - 
                                                                                    Jam <?= date("H:i",strtotime($ds['waktu_mulai_sesi']))?> - 
                                                                                    <?= date("H:i",strtotime($ds['waktu_berakhir_sesi']))?>
                                                                                    (Kuota : <?= $ds['kuota_sesi']?>, Tersisa <?= $ds['kuota_sesi'] - $ds['kuota_terdaftar']?>)
                                                                                </option>;
                                                                <?php
                                                                            }else{
                                                                ?>
                                                                                <option value="<?= $ds['id_sesi']?>">
                                                                                    <?= date("d F Y",strtotime($ds['tanggal_sesi']))?> - 
                                                                                    Jam <?= date("H:i",strtotime($ds['waktu_mulai_sesi']))?> - 
                                                                                    <?= date("H:i",strtotime($ds['waktu_berakhir_sesi']))?>
                                                                                    (Kuota : <?= $ds['kuota_sesi']?>, Tersisa <?= $ds['kuota_sesi'] - $ds['kuota_terdaftar']?>)
                                                                                </option>;
                                                                <?php
                                                                            }
                                                                        }
                                                                    }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group clearfix">
                                                        <label for="kategori_program" class="col-form-label">
                                                            Kategori Program <span class="text-danger"> *</span>
                                                        </label>
                                                        <div class="">
                                                            <span id="span_kategori_program_peserta_error" class="text-danger"></span>
                                                            <select id="kategori_program" class="form-control" name="kategori_program" required>
                                                                <option value="">Pilih Kategori</option>
                                                                <?php for($i = 0; $i < count($arr_target_klien); $i++){?>
                                                                    <option value="<?= $arr_target_klien[$i]?>"><?= $arr_target_klien[$i]?></option>
                                                                <?php }?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <h3>Data Diri</h3>
                                                    <div class="form-row">
                                                        <div class="form-group col-md-6 clearfix">
                                                            <label class="control-label" for="nama_depan_peserta">
                                                                Nama Depan <span class="text-danger"> *</span>
                                                            </label>
                                                            <div class="">
                                                                <span id="span_nama_depan_peserta_error" class="text-danger"></span>
                                                                <input id="nama_depan_peserta" name="nama_depan_peserta" type="text" class="form-control" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6 clearfix">
                                                            <label class="control-label " for="nama_belakang_peserta">
                                                                Nama Belakang <span class="text-danger"> *</span>
                                                            </label>
                                                            <div class="">
                                                                <span id="span_nama_belakang_peserta_error" class="text-danger"></span>
                                                                <input id="nama_belakang_peserta" name="nama_belakang_peserta" type="text" class="form-control" required>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-row">
                                                        <div class="form-group col-md-6 clearfix">
                                                            <label class="control-label " for="email_peserta">Email <span class="text-danger"> *</span></label>
                                                            <div class="">
                                                                <span id="span_email_peserta_error" class="text-danger"></span>
                                                                <input id="email_peserta" name="email_peserta" type="email" class="email form-control" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6 clearfix">
                                                            <label class="control-label " for="phone_peserta">Nomor Handphone <span class="text-danger"> *</span></label>
                                                            <div class="">
                                                                <span id="span_phone_peserta_error" class="text-danger"></span>
                                                                <input id="phone_peserta" name="phone_peserta" type="text" class="form-control" required>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-row">
                                                        <div class="form-group col-md-6 clearfix">
                                                            <label>Tanggal Lahir <span class="text-danger"> *</span></label>
                                                            <div>
                                                                <div class="input-group">
                                                                    <span id="span_tanggal_lahir_peserta_error" class="text-danger"></span>
                                                                    <input type="date" class="form-control" name="tanggal_lahir_peserta" placeholder="mm/dd/yyyy" id="tanggal_lahir_peserta" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6 clearfix">
                                                            <label>Jenis Kelamin <span class="text-danger"> *</span></label>
                                                            <div class="">
                                                                <div class="custom-control custom-radio">
                                                                    <input type="radio" id="customRadio1" name="jenis_kelamin_peserta" value="Laki-laki" class="custom-control-input" checked>
                                                                    <label class="custom-control-label mt-0" for="customRadio1">Laki Laki</label>
                                                                </div>
                                                                <div class="custom-control custom-radio">
                                                                    <input type="radio" id="customRadio2" name="jenis_kelamin_peserta" value="Perempuan" class="custom-control-input">
                                                                    <label class="custom-control-label mt-0" for="customRadio2">Perempuan</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h3>Data Alamat</h3>
                                                    <?php
                                                        $this->db->ORDER_BY('nama_kota_kabupaten', "ASC");
                                                        $data_kota_kabupaten = $this->db->get('table_kota_kabupaten')->result_array();
                                                    ?>
                                                    <div class="form-row m-b-20">
                                                        <div class="form-group col-md-12 clearfix">
                                                            <label for="kota" class="col-form-label">Kota / Kabupaten<span class="text-danger"> *</span></label>
                                                            <span id="span_kota_peserta_error" class="text-danger"></span>
                                                            <select id="kota" class="form-control select2" name="kota_peserta" required>
                                                                <option disabled selected value="">Pilih kota / kabupaten</option>
                                                                <?php foreach($data_kota_kabupaten as $dkk):?>
                                                                    <option value="<?= $dkk['kode_kota_kabupaten']?>"><?= $dkk['nama_kota_kabupaten']?></option>
                                                                <?php endforeach;?>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group clearfix">
                                                        <label class="control-label " for="alamat_peserta">Alamat Lengkap<span class="text-danger"> *</span></label>
                                                        <div class="">
                                                            <span id="span_alamat_lengkap_peserta_error" class="text-danger"></span>
                                                            <input id="alamat_peserta" name="alamat_peserta" type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                    <?php
                                                        $this->db->ORDER_BY('kode_kategori', "ASC");
                                                        $data_instansi_kerja = $this->db->get('table_kategori_program')->result_array();
                                                    ?>
                                                    <div class="form-row">
                                                        <div class="form-group col-md-6 clearfix">
                                                            <label for="pekerjaan_peserta" class="col-form-label">Instansi Pekerjaan<span class="text-danger"> *</span></label>
                                                            <span id="span_pekerjaan_peserta_error" class="text-danger"></span>
                                                            <select id="pekerjaan_peserta" class="form-control select2" name="pekerjaan_peserta" required>
                                                                <option disabled selected value="">Pilih Instansi Pekerjaan</option>
                                                                <?php foreach($data_instansi_kerja as $dik):?>
                                                                    <option value="<?= $dik['kode_kategori']?>"><?= $dik['nama_kategori']?></option>
                                                                <?php endforeach;?>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-6 clearfix">
                                                            <label for="jenis_pekerjaan_peserta" class="col-form-label">Jenis Pekerjaan<span class="text-danger"> *</span></label>
                                                            <div class="">
                                                                <span id="span_jenis_pekerjaan_peserta_error" class="text-danger"></span>
                                                                <input id="jenis_pekerjaan_peserta" name="jenis_pekerjaan_peserta" type="text" class="required form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12 text-center">
                                                        <button type="button" class="btn btn-info" id="submit-pendaftaran-program">Submit</button>
                                                    </div>
                                                </section>

                                                <!-- MODAL KONFIRMASI REGISTRASI-->
                                                <div class="modal fade konfirmasi-registrasi-peserta" tabindex="-1" role="dialog" aria-labelledby="rescheduleBerakhir" aria-hidden="true" style="display: none;">
                                                    <div class="modal-dialog modal-lg">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                <h4 class="modal-title" id="rescheduleBerakhir">Konfirmasi Pendaftaran <?= $data_program['nama_program']?></h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <section>
                                                                    <div class="form-group clearfix">
                                                                        <div class="col-lg-12">
                                                                            <ul class="list-unstyled w-list">
                                                                                <li><b>Tanggal / Waktu :</b> <span id="span_tanggal_waktu_peserta"></span></li>
                                                                                <li><b>Kategori Vaksin :</b> <span id="span_kategori_program_peserta"></span> </li>
                                                                                <br>
                                                                                <li><b>NIK :</b> <span id="span_nik_peserta"></span></li>
                                                                                <li><b>Nama Lengkap :</b> <span id="span_nama_lengkap_peserta"></span></li>
                                                                                <li><b>Email :</b> <span id="span_email_peserta"></span></li>
                                                                                <li><b>No Handphone :</b> <span id="span_phone_peserta"></span></li>
                                                                                <li><b>Tanggal Lahir :</b> <span id="span_tanggal_lahir_peserta"></span></li>
                                                                                <li><b>Jenis Kelamin :</b> <span id="span_jenis_kelamin_peserta"></span> </li>
                                                                                <li><b>Kota :</b> <span id="span_kota_peserta"></span></li>
                                                                                <li><b>Alamat :</b> <span id="span_alamat_lengkap_peserta"></span></li>
                                                                                <li><b>Pekerjaan :</b> <span id="span_pekerjaan_peserta"></span></li>
                                                                                <li><b>Jenis Pekerjaan :</b> <span id="span_jenis_pekerjaan_peserta"></span></li>
                                                                            </ul>
                                                                            <div class="form-group clearfix">
                                                                                <label class="control-label text-danger">Note : 
                                                                                Mohon periksa kembali sebelum mengirimkan data untuk menghindari kesalahan pengisian data !</label>
                                                                            </div>

                                                                            <div class="checkbox checkbox-primary mt-2">
                                                                                <input id="checkbox-h" type="checkbox" required>
                                                                                <label for="checkbox-h">
                                                                                    Saya Setuju dengan Syarat dan Ketentuan dalam Pengisian Data Diri
                                                                                </label>
                                                                            </div>
                                                                        </div>
                                                                        <!-- <div class="col-lg-12 text-center mt-3">
                                                                            <a href="#" class="p-3 bg-info text-light text-right" id="sa-success">Daftar Program</a>
                                                                        </div> -->
                                                                    </div>
                                                                </section>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <div class="form-group text-right m-b-0">
                                                                    <button class="btn bg-info text-light text-right" type="submit">Daftar Program</button>
                                                                    <button type="button" class="btn btn-light waves-effect m-l-5" data-dismiss="modal">Cancel</button>
                                                                </div>
                                                            </div>

                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal -->
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <!-- End row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2018 © Highdmin. - Coderthemes.com
                </footer>

            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->
        </div>
        <!-- END wrapper -->


        <!-- jQuery  -->
        <script src="<?=base_url();?>assets/js/jquery.min.js"></script>
        <script src="<?=base_url();?>assets/js/popper.min.js"></script>
        <script src="<?=base_url();?>assets/js/bootstrap.min.js"></script>
        <script src="<?=base_url();?>assets/js/metisMenu.min.js"></script>
        <script src="<?=base_url();?>assets/js/waves.js"></script>
        <script src="<?=base_url();?>assets/js/jquery.slimscroll.js"></script>

        <!--Form Wizard-->
        <script src="<?=base_url();?>assets/plugins/jquery.steps/js/jquery.steps.min.js" type="text/javascript"></script>

        <!--wizard initialization-->
        <script src="<?=base_url();?>assets/pages/jquery.wizard-init.js" type="text/javascript"></script>

        <!-- Sweet Alert Js  -->
        <script src="<?=base_url();?>assets/plugins/sweet-alert/sweetalert2.min.js"></script>
            <script src="<?=base_url();?>assets/pages/jquery.sweet-alert.init.js"></script>

        <!-- App js -->
        <script src="<?=base_url();?>assets/js/jquery.core.js"></script>
        <script src="<?=base_url();?>assets/js/jquery.app.js"></script>
        
        <!-- Script menampilkan data di bagian konfirmasi setelah user isi data -->
        <script type="text/javascript">
            var validasi_nik = 0;
            var validasi_sesi = 0;
            var validasi_kategori = 0;
            var validasi_nama_depan = 0;
            var validasi_nama_belakang = 0;
            var validasi_email = 0;
            var validasi_phone = 0;
            var validasi_tanggal_lahir = 0;
            var validasi_kota = 0;
            var validasi_alamat = 0;
            var validasi_pekerjaan = 0;
            var validasi_jenis_pekerjaan = 0;

            $("#nik").keyup(function(){
                var nik = $('[name="nik_peserta"]').val();
                
                if(nik.length < 14 || nik.length > 16){
                    validasi_nik = 0;
                    document.getElementById("span_nik_peserta_error").innerHTML = "Pastikan Nomor Induk Kependudukan anda telah benar";
                }else{
                    validasi_nik = 1;
                    document.getElementById("span_nik_peserta_error").innerHTML = "";
                    document.getElementById("span_nik_peserta").innerHTML = nik;
                }
            });
            
            $("#sesi_program").change(function(){
                var sesi_program = $('[name="sesi_program"]').val();
                
                if(sesi_program == ""){
                    validasi_sesi = 0;
                    document.getElementById("span_tanggal_waktu_peserta_error").innerHTML = "Sesi program wajib dipilih";
                }else{
                    $.ajax({
                        type : "POST",
                        url: "<?= base_url('Get/getSesiById')?>",
                        dataType : "JSON",
                        data : {id_sesi:sesi_program},
                        success: function(data){
                            validasi_sesi = 1;
                            document.getElementById("span_tanggal_waktu_peserta_error").innerHTML = "";
                            document.getElementById("span_tanggal_waktu_peserta").innerHTML = data.tanggal_sesi+' / '+data.waktu_mulai_sesi+' : '+data.waktu_berakhir_sesi;
                        }
                    });
                }

            });

            $("#kategori_program").change(function(){
                var kategori_program = $('[name="kategori_program"]').val();
                
                if(kategori_program == ""){
                    validasi_kategori = 0;
                    document.getElementById("span_kategori_program_peserta_error").innerHTML = "Kategori program wajib dipilih";
                }else{
                    validasi_kategori = 1;
                    document.getElementById("span_kategori_program_peserta_error").innerHTML = "";
                    document.getElementById("span_kategori_program_peserta").innerHTML = kategori_program;
                }
            });

            $("#nama_depan_peserta").keyup(function(){
                var nama_depan_peserta = $('[name="nama_depan_peserta"]').val();
                var nama_belakang_peserta = $('[name="nama_belakang_peserta"]').val();

                if(nama_depan_peserta == ""){
                    validasi_nama_depan = 0;
                    document.getElementById("span_nama_depan_peserta_error").innerHTML = "Nama Depan wajib diisi";
                }else{
                    validasi_nama_depan = 1;
                    document.getElementById("span_nama_depan_peserta_error").innerHTML = "";
                    document.getElementById("span_nama_lengkap_peserta").innerHTML = nama_depan_peserta+" "+nama_belakang_peserta;
                }
            });

            $("#nama_belakang_peserta").keyup(function(){
                var nama_depan_peserta = $('[name="nama_depan_peserta"]').val();
                var nama_belakang_peserta = $('[name="nama_belakang_peserta"]').val();

                if(nama_belakang_peserta == ""){
                    validasi_nama_belakang = 0;
                    document.getElementById("span_nama_belakang_peserta_error").innerHTML = "Nama Belakang wajib diisi";
                }else{
                    validasi_nama_belakang = 1;
                    document.getElementById("span_nama_belakang_peserta_error").innerHTML = "";
                    document.getElementById("span_nama_lengkap_peserta").innerHTML = nama_depan_peserta+" "+nama_belakang_peserta;
                }
            });

            $("#email_peserta").keyup(function(){
                var email_peserta = $('[name="email_peserta"]').val();

                if(email_peserta == ""){
                    validasi_email = 0;
                    document.getElementById("span_email_peserta_error").innerHTML = "Email wajib diisi";
                }else{
                    validasi_email = 1;
                    document.getElementById("span_email_peserta_error").innerHTML = "";
                    document.getElementById("span_email_peserta").innerHTML = email_peserta;
                }
            });

            $("#phone_peserta").keyup(function(){
                var phone_peserta = $('[name="phone_peserta"]').val();

                if(phone_peserta == ""){
                    validasi_phone = 0;
                    document.getElementById("span_phone_peserta_error").innerHTML = "Nomor Handphone wajib diisi";
                }else{
                    validasi_phone = 1;
                    document.getElementById("span_phone_peserta_error").innerHTML = "";
                    document.getElementById("span_phone_peserta").innerHTML = phone_peserta;
                }
            });

            $("#tanggal_lahir_peserta").change(function(){
                var tanggal_lahir_peserta = $('[name="tanggal_lahir_peserta"]').val();
                if(tanggal_lahir_peserta == ""){
                    validasi_tanggal_lahir = 0;
                    document.getElementById("span_tanggal_lahir_peserta_error").innerHTML = "Tanggal lahir wajib dipilih";
                }else{
                    validasi_tanggal_lahir = 1;
                    document.getElementById("span_tanggal_lahir_peserta_error").innerHTML = "";
                    document.getElementById("span_tanggal_lahir_peserta").innerHTML = tanggal_lahir_peserta;
                }
            });
            document.getElementById("span_jenis_kelamin_peserta").innerHTML = $('#customRadio1').val();

            $("#customRadio1").click(function(){
                var jenis_kelamin_peserta = $('#customRadio1').val();

                document.getElementById("span_jenis_kelamin_peserta").innerHTML = jenis_kelamin_peserta;
            });

            $("#customRadio2").click(function(){
                var jenis_kelamin_peserta = $('#customRadio2').val();
                document.getElementById("span_jenis_kelamin_peserta").innerHTML = jenis_kelamin_peserta;
            });

            $("#kota").change(function(){
                var kota_peserta = $('[name="kota_peserta"]').val();

                if(kota_peserta == ""){
                    validasi_kota = 0;
                    document.getElementById("span_kota_peserta_error").innerHTML = "Kota wajib dipilih";
                }else{
                    $.ajax({
                        type : "POST",
                        url: "<?= base_url('Get/getKotaById')?>",
                        dataType : "JSON",
                        data : {id_kota:kota_peserta},
                        success: function(data){
                            validasi_kota = 1;
                            document.getElementById("span_kota_peserta_error").innerHTML = "";
                            document.getElementById("span_kota_peserta").innerHTML = data.nama_kota_kabupaten;
                        }
                    });
                }
            });

            $("#alamat_peserta").keyup(function(){
                var alamat_peserta = $('[name="alamat_peserta"]').val();

                if(alamat_peserta == ""){
                    validasi_alamat = 0;
                    document.getElementById("span_alamat_lengkap_peserta_error").innerHTML = "Alamat wajib diisi";
                }else{
                    validasi_alamat = 1;
                    document.getElementById("span_alamat_lengkap_peserta_error").innerHTML = "";
                    document.getElementById("span_alamat_lengkap_peserta").innerHTML = alamat_peserta;
                }
            });

            $("#pekerjaan_peserta").change(function(){
                var pekerjaan_peserta = $('[name="pekerjaan_peserta"]').val();

                if(pekerjaan_peserta == ""){
                    validasi_pekerjaan = 0;
                    document.getElementById("span_pekerjaan_peserta_error").innerHTML = "Pekerjaan wajib dipilih";
                }else{
                    $.ajax({
                        type : "POST",
                        url: "<?= base_url('Get/getKategoriById')?>",
                        dataType : "JSON",
                        data : {kode_kategori:pekerjaan_peserta},
                        success: function(data){
                            validasi_pekerjaan = 1;
                            document.getElementById("span_pekerjaan_peserta_error").innerHTML = "";
                            document.getElementById("span_pekerjaan_peserta").innerHTML = data.nama_kategori;
                        }
                    });
                }
            });

            $("#jenis_pekerjaan_peserta").keyup(function(){
                var jenis_pekerjaan_peserta = $('[name="jenis_pekerjaan_peserta"]').val();

                if(jenis_pekerjaan_peserta == ""){
                    validasi_jenis_pekerjaan = 0;
                    document.getElementById("span_jenis_pekerjaan_peserta_error").innerHTML = "Jenis Pekerjaan wajib diisi";
                }else{
                    validasi_jenis_pekerjaan = 1;
                    document.getElementById("span_jenis_pekerjaan_peserta_error").innerHTML = "";
                    document.getElementById("span_jenis_pekerjaan_peserta").innerHTML = jenis_pekerjaan_peserta;
                }
            });
        </script>

        <!-- Script validasi submit form registrasi -->
        <script type="text/javascript">
            $("#submit-pendaftaran-program").click(function(){
                var nik = $('[name="nik_peserta"]').val();
                var sesi_program = $('[name="sesi_program"]').val();
                var kategori_program = $('[name="kategori_program"]').val();
                var nama_depan_peserta = $('[name="nama_depan_peserta"]').val();
                var nama_belakang_peserta = $('[name="nama_belakang_peserta"]').val();
                var email_peserta = $('[name="email_peserta"]').val();
                var phone_peserta = $('[name="phone_peserta"]').val();
                var tanggal_lahir_peserta = $('[name="tanggal_lahir_peserta"]').val();
                var jenis_kelamin_peserta = $('#customRadio1').val();
                var jenis_kelamin_peserta = $('#customRadio2').val();
                var kota_peserta = $('[name="kota_peserta"]').val();
                var alamat_peserta = $('[name="alamat_peserta"]').val();
                var pekerjaan_peserta = $('[name="pekerjaan_peserta"]').val();
                var jenis_pekerjaan_peserta = $('[name="jenis_pekerjaan_peserta"]').val();

                if(
                    nik != "" && 
                    sesi_program != "" && 
                    kategori_program != "" &&
                    nama_depan_peserta != "" &&
                    nama_belakang_peserta != "" &&
                    email_peserta != "" &&
                    phone_peserta != "" &&
                    tanggal_lahir_peserta != "" &&
                    jenis_kelamin_peserta != "" &&
                    jenis_kelamin_peserta != "" &&
                    kota_peserta != "" &&
                    alamat_peserta != "" &&
                    pekerjaan_peserta != "" &&
                    jenis_pekerjaan_peserta != "" &&


                    validasi_nik != 0 &&
                    validasi_sesi != 0 &&
                    validasi_kategori != 0 &&
                    validasi_nama_depan != 0 &&
                    validasi_nama_belakang != 0 &&
                    validasi_email != 0 &&
                    validasi_phone != 0 &&
                    validasi_tanggal_lahir != 0 &&
                    validasi_kota != 0 &&
                    validasi_alamat != 0 &&
                    validasi_pekerjaan != 0 &&
                    validasi_jenis_pekerjaan != 0
                    ){
                    $(".konfirmasi-registrasi-peserta").modal("show");
                }else{
                    alert("Mohon lengkapi data !");
                }

            });
        </script>

        <!-- Script validasi inputan angka -->
        <script type="text/javascript">
            setInputFilter(document.getElementById("phone_peserta"), function(value) {
                return /^\d*\.?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
            });
            setInputFilter(document.getElementById("nik"), function(value) {
                return /^\d*\.?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
            });
            function setInputFilter(textbox, inputFilter) {
                ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
                    textbox.addEventListener(event, function() {
                        if (inputFilter(this.value)) {
                            this.oldValue = this.value;
                            this.oldSelectionStart = this.selectionStart;
                            this.oldSelectionEnd = this.selectionEnd;
                        } else if (this.hasOwnProperty("oldValue")) {
                            this.value = this.oldValue;
                            this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                        } else {
                            this.value = "";
                        }
                    });
                });
            }
        </script>

        <!-- Script alert message -->
        <script type="text/javascript">
            $(function(){
                $(".flash-messages").delay(2500).fadeOut();
                setTimeout(function(){ 
                    <?php $this->session->set_flashdata('message',"");?>
                }, 3000);
            });
        </script>

        <!-- Script cek NIK -->
        <script type="text/javascript">
            $("#data-diri-registrasi").hide();
            $("#btn-check-nik").click(function(){
                var nik = $("#nik").val();
                if(validasi_nik == 1){
                    $.ajax({
                        type : "POST",
                        url: "<?= base_url('Get/cekNIK')?>",
                        dataType : "JSON",
                        data : {nik:nik},
                        success: function(data){
                            if(data.response == "valid"){
                                document.getElementById("span_nik_peserta_valid").innerHTML = "NIK dapat digunakan !";
                                document.getElementById("span_nik_peserta_invalid").innerHTML = "";
                                $("#data-diri-registrasi").show();
                            }else{
                                document.getElementById("span_nik_peserta_valid").innerHTML = "";
                                document.getElementById("span_nik_peserta_invalid").innerHTML = "NIK anda telah terdaftar, silahkan hubungi CS !";
                                $("#data-diri-registrasi").hide();
                            }
                        }
                    });
                }else{
                    alert("Masukkan NIK yang benar sebelum pengecekkan !");
                }
            });
        </script>
    </body>
</html>