                <!-- Start Page content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row mt-3">
                            <div class="col-12 rounded">
                                <div class="card-box pt-0 px-0 pb-5">
                                    <span class="mb-4 font-weight-bold px-4 px-md-5 py-3 bg-custom text-light rounded">Program Yang Sedang Berlangsung</span>
                                    <div class="px-md-5 mt-5">
                                        <div class="card-box shadow-sm pt-0 px-0 pb-5">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="card-box">
                                                        <div class="row">
                                                            <div class="col- mx-3">
                                                                <h3 class=""><a href="<?=base_url()?>program" class="text-custom"><i class="fa fa-chevron-left mr-3"></i></a>Vaksinasi</h3>
                                                            </div>
                                                            <div class="col- mt-2">
                                                                <button type="button" class="btn btn-outline-custom waves-light waves-effect" onClick="window.location.href='<?= base_url()?>program/tambahprogram'"><b>+</b> Tambah Program</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row px-md-5 px-4 ">
                                                <?php foreach($list_program as $lp): ?>
                                                <div class="col-sm-6 col-lg-6 col-xl-4 px-1 px-md-3 d-flex align-items-stretch">
                                                    <div class="card-box shadow-sm rounded p-4 d-flex flex-column hover-custom">
                                                        <?php
                                                            $replace_url_nama = str_replace(" ", "-", $lp['nama_program']);
                                                            $replace_url_tanggal = str_replace("-", "", $lp['tanggal_mulai_program']);

                                                            $tanggal_sekarang = strtotime(date('Y-m-d'));

                                                            $tanggal_mulai_program = strtotime($lp['tanggal_mulai_program']);
                                                            $tanggal_berakhir_program = strtotime($lp['tanggal_berakhir_program']);

                                                            $jarak = $tanggal_berakhir_program - $tanggal_mulai_program;
                                                            $hari = $jarak / 60 / 60 / 24;
                                                        ?>
                                                        <div class="btn-group float-right" style="right: 30px !important;position: absolute;left: auto;">
                                                            <button type="button" class="btn bg-white text-dark waves-effect px-2" style="border: none !important" data-toggle="dropdown" aria-expanded="false"><i class="icon-options-vertical"></i></button>
                                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 35px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                            <a class="dropdown-item py-3 btn_edit_program" href="#" data-id_program="<?= $lp['id_program']?>"><i class="fa fa-pencil-square-o"></i> Edit Program</a>
                                                            <a class="dropdown-item py-3 btn_hapus_program" style="color: red !important" href="#" data-id_program="<?= $lp['id_program']?>"><i class=" icon-trash "></i> Hapus Program</a>
                                                            </div>
                                                        </div>
                                                        <!-- <a href="#" style="right: 30px !important;position: absolute;left: auto;"><i class="icon-options-vertical"></i></a> -->
                                                        <a href="<?=base_url("Program/List/").$replace_url_nama."/".$lp['id_program'].$replace_url_tanggal?>">
                                                            <div class="widget-chart-one-content text-left">
                                                                <h4 class="pr-4"><?= $lp['nama_program']?></h4>
                                                                <p class="mb-0 mt-2">
                                                                    <?= date("l, d F Y", strtotime($lp['tanggal_mulai_program']))?>
                                                                    - 
                                                                    <?= date("l, d F Y", strtotime($lp['tanggal_berakhir_program']))?>
                                                                    <span class="text-custom font-weight-bold"> - (<?= $hari?> Hari)</span> 
                                                                </p>
                                                                <p class="mb-5 mt-1"><i class="fa fa-map-o"></i> <?= $lp['lokasi_program']?></p>
                                                            </div>
                                                        </a>
                                                        <div class="row mt-auto">
                                                            <div class="col-12">
                                                            <?php
                                                                $jarak_program_mulai = $tanggal_mulai_program - $tanggal_sekarang;
                                                                $jarak_program_berakhir = $tanggal_berakhir_program - $tanggal_sekarang;
                                                                $hari_akan_mulai = $jarak_program_mulai / 60 / 60 / 24;
                                                                $hari_akan_berakhir = $jarak_program_berakhir / 60 / 60 / 24;

                                                                if($hari_akan_mulai > 0 && $hari_akan_berakhir > 0){
                                                            ?>
                                                                <p class="px-4 py-2 text-light bg-info btn-rounded waves-light waves-effect">
                                                                    Akan Berlangsung
                                                                </p>
                                                            </div>
                                                            <?php
                                                                }else if($hari_akan_mulai <= 0 && $hari_akan_berakhir > 0){
                                                            ?>
                                                                <p class="px-4 py-2 text-light bg-success btn-rounded waves-light waves-effect">
                                                                    Sedang Berlangsung
                                                                </p>
                                                            </div>
                                                            <?php
                                                                }else{
                                                            ?>
                                                                <p class="px-4 py-2 text-light bg-danger btn-rounded waves-light waves-effect">
                                                                    Telah Berakhir
                                                                </p>
                                                            </div>
                                                            <?php
                                                                }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php endforeach;?>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->