<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title><?=$title?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="Dashboard Klinis for Partners." name="description" />
        <meta content="PT Kreasi Layanan Medis (Klinis)" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="<?=base_url();?>assets/images/favicon.ico">

        <!-- App css -->
        <link href="<?=base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="<?=base_url();?>assets/js/modernizr.min.js"></script>

    </head>


    <body class="account-pages">

        <!-- Begin page -->
        <div class="accountbg d-flex justify-content-center align-items-center" style="/*background: url('<?=base_url();?>assets/images/bg-1.jpg');*/background-size: cover;"></div>

        <div class="wrapper-page col-md-4 mx-auto mt-5">

            <div class="card">
                <div class="card-block">

                    <div class="account-box">

                        <div class="card-box p-5">
                            <h2 class="text-uppercase text-center pb-4">
                                <a href="<?=base_url();?>" class="text-success">
                                    <span><img src="<?=base_url();?>assets/images/logo.png" alt="" height="50"></span>
                                </a>
                            </h2>

                            <form class="" action="<?=base_url("auth/login");?>" method="POST">
                                <div class="flash-messages">
                                    <?= $this->session->flashdata('message')?>
                                </div>
                                <div class="form-group m-b-20 row">
                                    <div class="col-12">
                                        <label for="emailaddress">Email address</label>
                                        <input class="form-control" type="email" id="emailaddress" name="email_login" placeholder="Masukkan email">
                                    </div>
                                </div>

                                <div class="form-group row m-b-20">
                                    <div class="col-12">
                                        <label for="password">Password</label>
                                        <input class="form-control" type="password" id="password" name="password_login" placeholder="Masukkan password">
                                        <a href="<?= base_url('Auth/LupaPassword')?>" class="text-custom pull-right"><small>Lupa Password?</small></a>
                                    </div>
                                </div>

                                <!-- <div class="form-group row m-b-20">
                                    <div class="col-12">

                                        <div class="checkbox checkbox-custom">
                                            <input id="remember" type="checkbox">
                                            <label for="remember">
                                                Ingat Saya
                                            </label>
                                        </div>

                                    </div>
                                </div> -->

                                <div class="form-group row text-center m-t-10">
                                    <div class="col-12">
                                        <button class="btn btn-block btn-custom waves-effect waves-light" type="submit" >Sign In</button>
                                    </div>
                                </div>

                            </form>

                            <div class="row m-t-50">
                                <div class="col-sm-12 text-center">
                                    <p class="text-muted">Ingin Mendaftar menjadi Mitra? <a href="<?=base_url();?>auth/daftarmitra" class="text-dark m-l-5"><b>Daftar Disini</b></a></p>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

            <div class="m-t-40 text-center">
                <p class="account-copyright">Powered by <b>Klinis</b> @ <b>PT. Kreasi Layanan Medis </b></p>
            </div>

        </div>



        <!-- jQuery  -->
        <script src="<?=base_url();?>assets/js/jquery.min.js"></script>
        <script src="<?=base_url();?>assets/js/popper.min.js"></script>
        <script src="<?=base_url();?>assets/js/bootstrap.min.js"></script>
        <script src="<?=base_url();?>assets/js/metisMenu.min.js"></script>
        <script src="<?=base_url();?>assets/js/waves.js"></script>
        <script src="<?=base_url();?>assets/js/jquery.slimscroll.js"></script>

        <!-- App js -->
        <script src="<?=base_url();?>assets/js/jquery.core.js"></script>
        <script src="<?=base_url();?>assets/js/jquery.app.js"></script>
        <script type="text/javascript">
            $(function(){
                $(".flash-messages").delay(1500).fadeOut();
                setTimeout(function(){ 
                    <?php $this->session->set_flashdata('message',"");?>
                }, 2000);
            });
        </script>
    </body>
</html>