<?php 
    $replace_url_nama = str_replace(" ", "-", $data_program['nama_program']);
    $replace_url_tanggal = str_replace("-", "", $data_program['tanggal_mulai_program']);

    $tanggal_mulai_program = strtotime($data_program['tanggal_mulai_program']);
    $tanggal_berakhir_program = strtotime($data_program['tanggal_berakhir_program']); 

    $tanggal_sekarang = strtotime(date('Y-m-d'));

    $jarak = $tanggal_berakhir_program - $tanggal_mulai_program;
    $hari = $jarak / 60 / 60 / 24;


    $jarak_program_mulai = $tanggal_mulai_program - $tanggal_sekarang;
    $jarak_program_berakhir = $tanggal_berakhir_program - $tanggal_sekarang;
    $hari_akan_mulai = $jarak_program_mulai / 60 / 60 / 24;
    $hari_akan_berakhir = $jarak_program_berakhir / 60 / 60 / 24;
?>
<div class="content">
    <div class="container-fluid">
        <div class="row mt-3">
            <div class="col-12 rounded">
                <div class="card-box pt-0 px-0 pb-5">
                    <span class="mb-4 font-weight-bold px-4 px-md-5 py-3 bg-custom text-light rounded">Program Yang Sedang Berlangsung</span>
                    <div class="px-md-5 mt-5">
                        <div class="card-box shadow-sm pt-0 px-0 pb-5">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card-box">
                                        <div class="row">
                                            <div class="col- mx-3">
                                                <h3 class=""><a href="<?=base_url()?>program/vaksinasi" class="text-custom"><i class="fa fa-chevron-left mr-3"></i></a><?= $data_program['nama_program']?></h3>
                                            </div>
                                            <div class="col- ml-auto mx-3 mt-2">
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn btn-outline-custom dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false" ><i class="fa fa-share-alt"></i> Share</button>
                                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 35px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                        <a class="dropdown-item py-3" href="#"><i class="fa fa-copy"></i> Copy Link Pendaftaran</a>
                                                        <a class="dropdown-item py-3" href="#"><i class="fa fa-whatsapp"></i> Whatsapp</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="">
                                <nav class="navbar navbar-expand-lg navbar-dark bg-custom">
                                  <a class="navbar-brand d-block d-md-none" href="#">Menu</a>
                                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon"></span>
                                  </button>

                                  <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0 m-auto py-2 d-flex justify-content-center">
                                      <li class="nav-item mx-4 py-2">
                                        <a class="nav-link" href="<?= base_url('Program/List/').$replace_url_nama."/".$data_program['id_program'].$replace_url_tanggal?>"><i class="fa fa-dashboard"></i> Dashboard</a>
                                      </li>
                                      <li class="nav-item mx-4 py-2">
                                        <a class="nav-link" href="<?= base_url('Program/Jadwal/').$replace_url_nama."/".$data_program['id_program'].$replace_url_tanggal?>"><i class="fa fa-calendar"></i> Atur Jadwal Program</a>
                                      </li>
                                      <li class="nav-item mx-4 py-2 active">
                                        <a class="nav-link" href="<?= base_url('Program/Peserta/').$replace_url_nama."/".$data_program['id_program'].$replace_url_tanggal?>"><i class="fa fa-users"></i> Peserta</a>
                                      </li>
                                      <li class="nav-item mx-4 py-2">
                                        <a class="nav-link" href="<?= base_url('Program/AktivasiPeserta/').$replace_url_nama."/".$data_program['id_program'].$replace_url_tanggal?>"><i class="fa fa-check-square-o"></i> <b>Aktivasi Peserta</b></a>
                                      </li>
                                      <li class="nav-item mx-4 py-2 px-4 bg-white rounded">
                                        <?php
                                            $replace_url_nama = str_replace(" ", "-", $data_program['nama_program']);
                                            $replace_url_tanggal = str_replace("-", "", $data_program['tanggal_mulai_program']);
                                        ?>
                                        <?php
                                            if($hari_akan_berakhir > 0){
                                        ?>
                                            <a class="nav-link text-custom" 
                                            href="<?=base_url("Program/registrasi/").$replace_url_nama."/".$data_program['id_program'].$replace_url_tanggal?>" target="_blank">
                                            <b><i class="fa fa-paste"></i> Link Formulir Pendaftaran</b></a>

                                        <?php }else{?>
                                            <a class="btn nav-link text-custom" id="btn_link_formulir_berakhir">                                                
                                                <b><i class="fa fa-paste"></i> Link Formulir Pendaftaran</b>
                                            </a>
                                        <?php }?>
                                      </li>
                                    </ul>
                                  </div>
                                </nav>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="card-box rounded">
                                        <div class="widget-chart-one-content p-4 p-md-5 shadow-sm text-left">
                                            <div class="row mt-4 mb-2">
                                                <div class="col-md-8">
                                                    <h3 class=""><?= $data_program['nama_program']?></h3>
                                                </div>
                                                <div class="col- ml-auto mx-3 mt-2">

                                                    <?php

                                                        if($hari_akan_mulai > 0 && $hari_akan_berakhir > 0){
                                                    ?>
                                                        <p class="px-4 py-2 text-light bg-info btn-rounded waves-light waves-effect">
                                                            Akan Berlangsung
                                                        </p>
                                                    <?php
                                                        }else if($hari_akan_mulai <= 0 && $hari_akan_berakhir > 0){
                                                    ?>
                                                        <p class="px-4 py-2 text-light bg-success btn-rounded waves-light waves-effect">
                                                            Sedang Berlangsung
                                                        </p>
                                                    <?php
                                                        }else{
                                                    ?>
                                                        <p class="px-4 py-2 text-light bg-danger btn-rounded waves-light waves-effect">
                                                            Telah Berakhir
                                                        </p>
                                                    <?php
                                                        }
                                                    ?>
                                                </div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <h5>Tanggal / Waktu</h5>
                                                    <p class="">
                                                        <?= date("l, d F Y", strtotime($data_program['tanggal_mulai_program']))?>
                                                        - 
                                                        <?= date("l, d F Y", strtotime($data_program['tanggal_berakhir_program']))?>
                                                        <span class="text-custom">(<?= $hari?> Hari)</span>
                                                    </p>
                                                </div>
                                                <div class="col-md-4">
                                                    <h5>Lokasi</h5>
                                                    <p class=""><i class="fa fa-map-o"></i> <?= $data_program['lokasi_program']?></p>
                                                </div>
                                                <div class="col-md-4">
                                                    <h5>Penyelenggara</h5>
                                                    <img src="<?= base_url('assets/logo_mitra/').$data_faskes['logo_mitra']?>" alt="logo" height="50" class="img-responsive">
                                                    <p><?= $data_faskes['nama_faskes']?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="card-box">
                                        <h4 class="header-title">Peserta Program</h4>

                                        <div class="text-center mt-4 mb-4">
                                            <div class="row">
                                            <?php
                                                $id_program = $data_program['id_program'];

                                                $sql_search_data_peserta = "
                                                    SELECT a.*, b.*, c.* FROM table_sesi_program a
                                                    LEFT JOIN table_koneksi_program b ON a.id_sesi = b.id_sesi
                                                    LEFT JOIN table_peserta c ON b.id_peserta = c.id_peserta
                                                    WHERE a.id_program = '$id_program';
                                                ";

                                                $data_peserta_program = $this->db->query($sql_search_data_peserta)->result_array();
                                            ?>
                                                <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.1/css/jquery.dataTables.min.css">
                                                <table id="example" class="display" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>NIK</th>
                                                            <th>Nama</th>
                                                            <th>Tanggal Lahir</th>
                                                            <th>L/P</th>
                                                            <th>Phone</th>
                                                            <th>Status</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                            <?php
                                                foreach($data_peserta_program as $dpp){
                                                    if($dpp['id_peserta'] != ""){
                                                        $nama_peserta = str_replace("-", " ", $dpp['nama_peserta']);
                                            ?>
                                                        <tr>
                                                            <td><?= $dpp['nik_peserta']?></td>
                                                            <td><?= $nama_peserta?></td>
                                                            <td><?= date('d-M-Y', strtotime($dpp['tanggal_lahir_peserta']))?></td>
                                                            <td><?= $dpp['jenis_kelamin_peserta']?></td>
                                                            <td><?= $dpp['phone_peserta']?></td>
                                                            <td><?= $dpp['status_koneksi']?></td>
                                                            <td>
                                                                <?php if($dpp['status_koneksi'] != "Reschedule"):?>
                                                                <button type="button" class="btn btn-info rounded btn_reschedule_peserta" 
                                                                data-id_koneksi="<?= $dpp['id_koneksi']?>"
                                                                data-id_peserta="<?= $dpp['id_peserta']?>"
                                                                data-link_url="<?= $replace_url_nama."/".$data_program['id_program'].$replace_url_tanggal?>"
                                                                data-tanggal_terdaftar="<?= $dpp['tanggal_sesi']?>"
                                                                data-sesi_terdaftar="<?= $dpp['id_sesi']?>"
                                                                data-tanggal_mulai_program="<?= $data_program['tanggal_mulai_program']?>"
                                                                >
                                                                        <i class="fa fa-edit"></i>
                                                                </button>
                                                                <?php endif;?>
                                                            </td>
                                                        </tr>
                                            <?php
                                                    }
                                                }
                                            ?>
                                                    </tbody>
                                                </table>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- end col -->
                            </div>
                            <!-- end row -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>