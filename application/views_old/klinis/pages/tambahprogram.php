<!-- Summernote css -->
        <link href="<?=base_url()?>assets/plugins/summernote/summernote-bs4.css" rel="stylesheet" />
        <link href="<?=base_url()?>assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css" rel="stylesheet" />
        
                <!-- Start Page content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row mt-3">
                            <div class="col-12 rounded">
                                <div class="card-box pt-0 px-0 pb-5">
                                    <span class="mb-4 font-weight-bold px-4 px-md-5 py-3 bg-custom text-light rounded">Tambah Program</span>
                                    <div class="px-md-5 mt-5">
                                        <div class="card-box shadow-sm pt-0 px-0 pb-5">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="card-box">
                                                        <div class="row">
                                                            <div class="col- mx-3">
                                                                <h3 class=""><a href="<?=base_url()?>program" class="text-custom"><i class="fa fa-chevron-left mr-3"></i></a>Tambah Program</h3>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-6">

                                                            <div class="card-box">
                                                                <form action="<?= base_url('Program/submitProgram')?>" method="POST">
                                                                    <input type="hidden" name="id_user" value="<?= $data_user['id_user']?>">
                                                                    <div class="form-group">
                                                                        <label>Tipe Program<span class="text-danger">*</span></label>
                                                                        <select class="form-control" name="tipe_program">
                                                                            <option>Vaksinasi Covid</option>
                                                                        </select>
                                                                    </div>
                                                                    
                                                                    <div class="form-group">
                                                                        <label>Nama Program<span class="text-danger">*</span></label>
                                                                        <input type="text" name="nama_program" class="form-control" placeholder="Masukkan Nama Program">
                                                                    </div>
                                                                    
                                                                    <div class="form-group">
                                                                        <label>Deskripsi Program</label>
                                                                        <textarea class="summernote" name="deskripsi_program">
                                                                            <h3>Masukkan Deskripsi</h3>
                                                                        </textarea>
                                                                    </div>
                                                            </div> <!-- end card-box -->
                                                        </div>

                                                        <div class="col-lg-6">

                                                            <div class="card-box">
                                                                <div class="form-group m-b-20">
                                                                    <label>Periode Program Acara</label>
                                                                    <div>
                                                                        <input class="form-control input-daterange-datepicker" type="text" name="daterange" value="<?= date('m-d-Y')?> - 01/31/2021"/>
                                                                    </div>
                                                                </div>

                                                                    <div class="form-group">
                                                                        <label>Lokasi Program</label>
                                                                        <div>
                                                                            <textarea required class="form-control" name="lokasi_program"></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Target Peserta</label>
                                                                        <div class="">
                                                                            <input type="number" class="form-control" name="target_peserta_program" placeholder="Masukkan Target Peserta"/>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Target Klien</label>
                                                                        <div class="tags-default">
                                                                            <input type="text" value="Mahasiswa, Difabel" data-role="tagsinput" name="target_klien_program" placeholder="Tekan [TAB] Untuk menambahkan Target Peserta"/>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Goals<span class="text-danger">*</span></label>
                                                                        <div>
                                                                            <textarea required class="form-control" name="goals_program"></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label>Anggaran (Rp.)</label>
                                                                        <input type="text" placeholder="" data-a-sign="Rp. " class="form-control autonumber" name="anggaran_program">
                                                                        <!-- <span class="font-14 text-muted">Masukkan Nominal Anggaran</span> -->
                                                                    </div>

                                                                    <div class="form-group text-right m-b-0">
                                                                        <button class="btn btn-custom waves-effect waves-light" type="submit">
                                                                            Submit
                                                                        </button>
                                                                        <a href="<?= base_url('Program')?>" class="btn btn-light waves-effect m-l-5">Cancel</a>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                        <!-- end col -->
                                                    </div>
                                                    <!-- end row -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->