                <!-- Start Page content -->
                <div class="content">
                    <div class="container-fluid">

                        <div class="row mt-3">
                            <div class="col-12 rounded">
                                <div class="card-box pt-0 px-0 pb-5">
                                    <span class="mb-4 font-weight-bold px-4 px-md-5 py-3 bg-custom text-light rounded">Program Yang Sedang Berlangsung</span>
                                    <div class="px-5 mt-5">
                                        <div class="row text-center">
                                            <div class="col-sm-6 col-lg-6 col-xl-3">
                                                <a href="<?=base_url();?>program/vaksinasi">
                                                    <div class="card-box bg-primary widget-flat border-primary text-white rounded">
                                                        <i><img src="<?=base_url();?>assets/image/icon/syringe.svg" width="80"></i>
                                                        <h2 class="m-b-10"><img src="<?=base_url();?>assets/image/icon/syringe.svg" width="40"> Vaksinasi</h2>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="col-sm-6 col-lg-6 col-xl-3">
                                                <a href="<?= base_url()?>program/tambahprogram">
                                                    <div class="card-box btn-outline-custom border-custom widget-flat waves-light waves-effect rounded">
                                                        <i class="fa fa-plus"></i>
                                                        <h2 class="m-b-10"><span class="fa fa-plus"></span> Tambah Program</h2>
                                                    </div>
                                                </a>
                                            </div>
                                            <!-- <div class="col-sm-6 col-lg-6 col-xl-3">
                                                    <btn class="btn p-4 btn-outline-custom waves-light waves-effect rounded" onClick="window.location.href='<?= base_url()?>program/tambahprogram'">
                                                        <h3 class="m-b-10 px-3">+ Tambah Program</h3>
                                                    </btn>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->