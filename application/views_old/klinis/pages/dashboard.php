                <!-- Start Page content -->
                <div class="content">
                    <div class="container-fluid">
                        <div class="row mt-3">
                            <div class="col-md-12 rounded">
                                <div class="flash-messages">
                                    <?= $this->session->flashdata('message')?>
                                </div>
                            </div>
                            <div class="col-md-6 rounded">
                                <div class="card-box pt-0 px-0 pb-5">
                                    <span class="mb-4 font-weight-bold px-4 px-md-5 py-3 bg-custom text-light rounded">Profil Fasilias Kesehatan</span>
                                    <a href="#" id="btn_modal_edit_faskes" data-id_faskes="<?= $data_faskes['id_faskes']?>">
                                        <span class="mt-3 mr-3 px-4 py-2 bg-custom text-light rounded float-right">
                                            <i class="fa fa-pencil-square-o"></i> Edit
                                        </span>
                                    </a>
                                    <div class="px-5 mt-5">
                                        <p class="mb-0">Kode Fasilitas Kesehatan / Mitra</p>
                                        <h3 class="mt-0"><?= $data_faskes['kode_faskes']?></h3>
                                        <hr>
                                        <div class="row">
                                            <div class="col-md-3 text-center">
                                                <img src="<?= base_url("assets/logo_mitra/").$data_faskes['logo_mitra']?>" alt="logo" height="100" width="100" class="img-responsive" style="object-fit:scale-down;">
                                            </div>
                                            <div class="col-md-9 pl-md-4">
                                                <h4><?= $data_faskes['nama_faskes']?></h4>
                                                <p class="mb-0">Lokasi</p>
                                                <h5 class="mt-0"><?= $data_faskes['alamat_faskes']?></h5>
                                            </div>
                                        </div>
                                        <hr>
                                        <p>Tipe : <span class="h6"><?= $data_faskes['tipe_faskes']?></span></p>
                                        <div class="row icon-list-demo">
                                                <div class="col-md-6">
                                                    <i class="fa fa-envelope"></i> <span class="p"><?= $data_faskes['email_faskes']?></span><br>
                                                    <i class="fa fa-phone"></i> <span class="p"><?= $data_faskes['phone_faskes']?></span>
                                                </div>
                                                <div class="col-md-6">
                                                    <i class="fa fa fa-map-pin"></i> <span class="p">Long : <?= $data_faskes['longtitude_faskes']?></span><br>
                                                    <i class="fa fa fa-map-pin"></i> <span class="p">Lat : <?= $data_faskes['latitude_faskes']?></span>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 rounded">
                                <div class="card-box pt-0 px-0 pb-5">
                                    <span class="mb-4 font-weight-bold px-4 px-md-5 py-3 bg-custom text-light rounded">Profil Administrator</span>
                                    <div class="btn-group float-right mt-3 mr-3">
										<button type="button" class="btn dropdown-toggle waves-effect bg-custom text-light rounded" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-pencil-square-o"></i> Edit</button>
										<div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 35px, 0px); top: 0px; left: 0px; will-change: transform;">
										<a class="dropdown-item py-3" href="#" id="btn_modal_edit_admin" data-id_user="<?= $data_user['id_user']?>"><i class="fa fa-pencil-square-o"></i> Edit Profil & Email</a>
										<a class="dropdown-item py-3" href="#" id="btn_modal_change_password" data-id_user="<?= $data_user['id_user']?>"><i class="fa fa-pencil-square-o"></i> Ubah Password</a>
										</div>
									</div>
                                    <div class="px-5 mt-5">
                                        <h4><?= $data_user['nama_user']?></h4>
                                        <p class="mb-0">Email : <?= $data_user['email_user']?></p>
                                        <p>Password : ********** </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    Powered by <b>Klinis</b> @ <b>PT. Kreasi Layanan Medis </b>
                </footer>

            </div>