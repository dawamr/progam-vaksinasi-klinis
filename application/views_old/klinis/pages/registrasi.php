<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Registrasi Program Vaksinasi - <?= $data_program['nama_program']?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="Dashboard Klinis for Partners." name="description" />
        <meta content="PT Kreasi Layanan Medis (Klinis)" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="<?=base_url();?>assets/images/favicon.ico">

        <!--Form Wizard-->
        <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/plugins/jquery.steps/css/jquery.steps.css" />

        <!-- Sweet Alert css -->
        <link href="<?=base_url();?>assets/plugins/sweet-alert/sweetalert2.min.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="<?=base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="<?=base_url();?>assets/js/modernizr.min.js"></script>

    </head>


    <body>
<?php 
    $replace_url_nama = str_replace(" ", "-", $data_program['nama_program']);
    $replace_url_tanggal = str_replace("-", "", $data_program['tanggal_mulai_program']);

    $tanggal_mulai_program = strtotime($data_program['tanggal_mulai_program']);
    $tanggal_berakhir_program = strtotime($data_program['tanggal_berakhir_program']); 

    $tanggal_sekarang = strtotime(date('Y-m-d'));

    $jarak = $tanggal_berakhir_program - $tanggal_mulai_program;
    $hari = $jarak / 60 / 60 / 24;


    $jarak_program_mulai = $tanggal_mulai_program - $tanggal_sekarang;
    $jarak_program_berakhir = $tanggal_berakhir_program - $tanggal_sekarang;
    $hari_akan_mulai = $jarak_program_mulai / 60 / 60 / 24;
    $hari_akan_berakhir = $jarak_program_berakhir / 60 / 60 / 24;

    $arr_target_klien = explode (",",$data_program['target_klien_program']);
?>
        <!-- Begin page -->
        <div id="wrapper">


            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->

            <nav class="navbar navbar-light bg-white">
                <div class="page-title-box">
                    <a class="py-3 px-4" href="#"><img src="<?=base_url()?>assets/images/logo.png" alt="" height="40"></a>
                    <h5 class="ml-4"><?= $data_program['nama_program']?></h5>
                </div>
                <!-- <button type="button" class="btn btn-lg btn-success waves-light waves-effect">Login / Register</button> -->
            </nav>

            <div class="container">
                <!-- Start Page content -->
                <div class="content mt-5">
                    <div class="container-fluid mt-5">
                        <div class="button-list">
                            <button type="button" class="btn py-3 btn-block btn btn-outline-custom waves-light waves-effect font-weight-bold rounded" data-toggle="collapse" href="#faq" role="button" aria-expanded="false" aria-controls="faq">Frequently Asked Questions (F.A.Q)</button>
                        </div>
                        <div class="collapse" id="faq">
                            <div class="card card-body">
                                <div class="row m-t-50 pt-3">
                                    <div class="col-lg-5 offset-lg-1">
                                        <!-- Question/Answer -->
                                        <div>
                                            <div class="question-q-box">Q.</div>
                                            <h4 class="question" data-wow-delay=".1s">What is Lorem Ipsum?</h4>
                                            <p class="answer">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                                        </div>

                                        <!-- Question/Answer -->

                                        <div>
                                            <div class="question-q-box">Q.</div>
                                            <h4 class="question">Why use Lorem Ipsum?</h4>
                                            <p class="answer">Lorem ipsum dolor sit amet, in mea nonumes dissentias dissentiunt, pro te solet oratio iriure. Cu sit consetetur moderatius intellegam, ius decore accusamus te. Ne primis suavitate disputando nam. Mutat convenirete.</p>
                                        </div>

                                    </div>
                                    <!--/col-md-5 -->

                                    <div class="col-lg-5">
                                        <!-- Question/Answer -->

                                        <div>
                                            <div class="question-q-box">Q.</div>
                                            <h4 class="question">Is safe use Lorem Ipsum?</h4>
                                            <p class="answer">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                                        </div>

                                        <!-- Question/Answer -->

                                        <div>
                                            <div class="question-q-box">Q.</div>
                                            <h4 class="question">When can be used?</h4>
                                            <p class="answer">Lorem ipsum dolor sit amet, in mea nonumes dissentias dissentiunt, pro te solet oratio iriure. Cu sit consetetur moderatius intellegam, ius decore accusamus te. Ne primis suavitate disputando nam. Mutat convenirete.</p>
                                        </div>

                                    </div>
                                    <!--/col-md-5-->
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="container-fluid mt-5">

                        <!-- Basic Form Wizard -->
                        <div class="row">
                            <div class="col-12">
                                <div class="card-box shadow-sm rounded p-4 p-md-5">
                                    <div class="widget-chart-one-content text-left">
                                        <div class="row mt-4 mb-2">
                                            <div class="col-md-8">
                                                <h3 class=""><?= $data_program['nama_program']?></h3>
                                            </div>
                                            <div class="col- ml-auto mx-3 mt-2">
                                                <?php
                                                        if($hari_akan_mulai > 0 && $hari_akan_berakhir > 0){
                                                    ?>
                                                        <p class="px-4 py-2 mt-3 mr-3 text-light bg-info btn-rounded waves-light waves-effect">
                                                            Akan Berlangsung
                                                        </p>
                                                    <?php
                                                        }else if($hari_akan_mulai <= 0 && $hari_akan_berakhir > 0){
                                                    ?>
                                                        <p class="px-4 py-2 mt-3 mr-3 text-light bg-success btn-rounded waves-light waves-effect">
                                                            Sedang Berlangsung
                                                        </p>
                                                    <?php
                                                        }else{
                                                    ?>
                                                        <p class="px-4 py-2 mt-3 mr-3 text-light bg-danger btn-rounded waves-light waves-effect">
                                                            Telah Berakhir
                                                        </p>
                                                    <?php
                                                        }
                                                    ?>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn btn-outline-custom dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false" ><i class="fa fa-share-alt"></i> Share</button>
                                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 35px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                        <a class="dropdown-item py-3" href="void::javascript(0)" id="copy_link_url"><i class="fa fa-copy"></i> Copy Link Pendaftaran</a>
                                                        <a class="dropdown-item py-3" href="#"><i class="fa fa-whatsapp"></i> Whatsapp</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5>Tanggal / Waktu</h5>
                                                <p class="">
                                                    <?= date("l, d F Y", strtotime($data_program['tanggal_mulai_program']))?>
                                                    - 
                                                    <?= date("l, d F Y", strtotime($data_program['tanggal_berakhir_program']))?>
                                                    <span class="text-custom">(<?= $hari?> Hari)</span>
                                                </p>
                                            </div>
                                            <div class="col-md-4">
                                                <h5>Lokasi</h5>
                                                <p class=""><i class="fa fa-map-o"></i> <?= $data_program['lokasi_program']?></p>
                                            </div>
                                            <div class="col-md-4">
                                                <h5>Penyelenggara</h5>
                                                <img src="<?= base_url("assets/logo_mitra/").$data_faskes['logo_mitra']?>" alt="logo" height="50" class="img-responsive">
                                                <p><?= $data_faskes['nama_faskes']?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="card-box">
                                    <h4 class="m-t-0 header-title"><b>Catatan Penting!</b></h4>
                                    <p class="text-muted m-b-30 font-13">
                                        Harap isi data diri anda dengan benar dan periksa kembali sebelum mengirimkan data untuk menghindari kesalahan pengisian data !
                                    </p>

                                    <div class="pull-in">
                                        <form id="basic-form" action="<?= base_url('Program/registrasiPeserta')?>" method="POST">
                                            <div>
                                                <h3>Data Vaksin</h3>
                                                <section>
                                                    <div class="form-group clearfix">
                                                        <label class="control-label " for="nik">NIK *</label>
                                                        <div class="">
                                                            <input id="nik" name="nik_peserta" type="text" class="form-control" required>
                                                            <span class="help-block"><small>NIK Wajib diisi untuk validasi data diri personal.</small></span>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group clearfix">
                                                        <label for="sesi_program" class="col-form-label">Sesi Vaksinasi</label>
                                                        <select id="sesi_program" class="form-control" name="sesi_program" required>
                                                            <option value="">Pilih Sesi</option>
                                                            <?php
                                                                foreach ($data_sesi as $ds) {
                                                                    if($ds['status_sesi'] == "active"){
                                                            ?>
                                                                <option value="<?= $ds['id_sesi']?>">
                                                                    <?= date("d F Y",strtotime($ds['tanggal_sesi']))?> - 
                                                                    Jam <?= date("H:i",strtotime($ds['waktu_mulai_sesi']))?> - 
                                                                    <?= date("H:i",strtotime($ds['waktu_berakhir_sesi']))?>
                                                                    (Kuota : <?= $ds['kuota_sesi']?>, Tersisa <?= $ds['kuota_sesi'] - $ds['kuota_terdaftar']?>)
                                                                </option>;
                                                            <?php
                                                                    }
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group clearfix">
                                                        <label for="kategori_program" class="col-form-label">Kategori Vaksin</label>
                                                        <select id="kategori_program" class="form-control" name="kategori_program" required>
                                                            <option value="">Pilih Kategori</option>
                                                            <?php for($i = 0; $i < count($arr_target_klien); $i++){?>
                                                                <option value="<?= $arr_target_klien[$i]?>"><?= $arr_target_klien[$i]?></option>
                                                            <?php }?>
                                                        </select>
                                                    </div>
                                                </section>
                                                <h3>Data Diri</h3>
                                                <section>
                                                    <div class="form-row">
                                                        <div class="form-group col-md-6 clearfix">
                                                            <label class="control-label" for="nama_depan_peserta"> Nama Depan *</label>
                                                            <div class="">
                                                                <input id="nama_depan_peserta" name="nama_depan_peserta" type="text" class="required form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6 clearfix">
                                                            <label class="control-label " for="nama_belakang_peserta"> Nama Belakang *</label>
                                                            <div class="">
                                                                <input id="nama_belakang_peserta" name="nama_belakang_peserta" type="text" class="required form-control">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-row">
                                                        <div class="form-group col-md-6 clearfix">
                                                            <label class="control-label " for="email_peserta">Email *</label>
                                                            <div class="">
                                                                <input id="email_peserta" name="email_peserta" type="text" class="required email form-control">
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6 clearfix">
                                                            <label class="control-label " for="phone_peserta">Nomor Handphone *</label>
                                                            <div class="">
                                                                <input id="phone_peserta" name="phone_peserta" type="text" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-row">
                                                        <div class="form-group col-md-6 clearfix">
                                                            <label>Tanggal Lahir *</label>
                                                            <div>
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" name="tanggal_lahir_peserta" placeholder="mm/dd/yyyy" id="datepicker-autoclose">
                                                                    <div class="input-group-append">
                                                                        <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group col-md-6 clearfix">
                                                            <label>Jenis Kelamin *</label>
                                                            <div class="">
                                                                <div class="custom-control custom-radio">
                                                                    <input type="radio" id="customRadio1" name="jenis_kelamin_peserta" value="Laki-laki" class="custom-control-input">
                                                                    <label class="custom-control-label mt-0" for="customRadio1">Laki Laki</label>
                                                                </div>
                                                                <div class="custom-control custom-radio">
                                                                    <input type="radio" id="customRadio2" name="jenis_kelamin_peserta" value="Perempuan" class="custom-control-input">
                                                                    <label class="custom-control-label mt-0" for="customRadio2">Perempuan</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group clearfix">
                                                        <label class="control-label ">(*) Wajib Diisi</label>
                                                    </div>

                                                </section>
                                                <h3>Data Alamat</h3>
                                                <section>
                                                    <div class="form-group clearfix">
                                                        <div class="col-lg-12">
                                                            <?php
                                                                $this->db->ORDER_BY('nama_kota_kabupaten', "ASC");
                                                                $data_kota_kabupaten = $this->db->get('table_kota_kabupaten')->result_array();
                                                            ?>
                                                            <div class="form-row  m-b-20">
                                                                <div class="form-group col-md-12 clearfix">
                                                                    <label for="kota" class="col-form-label">Kota / Kabupaten<span class="text-danger"> *</span></label>
                                                                    <select id="kota" class="form-control select2" name="kota_peserta" required>
                                                                        <option disabled selected value="">Pilih kota / kabupaten</option>
                                                                        <?php foreach($data_kota_kabupaten as $dkk):?>
                                                                            <option value="<?= $dkk['kode_kota_kabupaten']?>"><?= $dkk['nama_kota_kabupaten']?></option>
                                                                        <?php endforeach;?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group clearfix">
                                                                <label class="control-label " for="alamat_peserta">Alamat Lengkap*</label>
                                                                <div class="">
                                                                    <input id="alamat_peserta" name="alamat_peserta" type="text" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="form-row">
                                                                <div class="form-group col-md-6 clearfix">
                                                                    <label class="control-label" for="pekerjaan_peserta"> Instansi Pekerjaan</label>
                                                                    <div class="">
                                                                        <input id="pekerjaan_peserta" name="pekerjaan_peserta" type="text" class="required form-control">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-md-6 clearfix">
                                                                    <label class="control-label " for="jenis_pekerjaan_peserta"> Jenis Pekerjaan</label>
                                                                    <div class="">
                                                                        <input id="jenis_pekerjaan_peserta" name="jenis_pekerjaan_peserta" type="text" class="required form-control">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                                <h3>Konfirmasi</h3>
                                                <section>
                                                    <div class="form-group clearfix">
                                                        <div class="col-lg-12">
                                                            <ul class="list-unstyled w-list">
                                                                <h3 class="">Pendaftaran <?= $data_program['nama_program']?></h3>
                                                                <li><b>Tanggal / Waktu :</b> <span id="span_tanggal_waktu_peserta"></span></li>
                                                                <li><b>Kategori Vaksin :</b> <span id="span_kategori_program_peserta"></span> </li>
                                                                <li><b>NIK :</b> <span id="span_nik_peserta"></span></li>
                                                                <li><b>Nama Lengkap :</b> <span id="span_nama_lengkap_peserta"></span></li>
                                                                <li><b>Email :</b> <span id="span_email_peserta"></span></li>
                                                                <li><b>No Handphone :</b> <span id="span_phone_peserta"></span></li>
                                                                <li><b>Tanggal Lahir :</b> <span id="span_tanggal_lahir_peserta"></span></li>
                                                                <li><b>Jenis Kelamin :</b> <span id="span_jenis_kelamin_peserta"></span> </li>
                                                                <li><b>Kota :</b> <span id="span_kota_peserta"></span></li>
                                                                <li><b>Alamat :</b> <span id="span_alamat_lengkap_peserta"></span></li>
                                                                <li><b>Pekerjaan :</b> <span id="span_pekerjaan_peserta"></span></li>
                                                                <li><b>Jenis Pekerjaan :</b> <span id="span_jenis_pekerjaan_peserta"></span></li>
                                                            </ul>
                                                            <div class="form-group clearfix">
                                                                <label class="control-label ">Note : Mohon cek keseluruhan Data untuk memastikan keakuratan data.</label>
                                                            </div>

                                                            <div class="checkbox checkbox-primary mt-4">
                                                                <input id="checkbox-h" type="checkbox" required>
                                                                <label for="checkbox-h">
                                                                    Saya Setuju dengan Syarat dan Ketentuan dalam Pengisian Data Diri
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <a href="#" class="p-4 bg-danger text-light text-right" id="sa-success">Kalau Sudah Submit</a>
                                                </section>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <!-- End row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2018 © Highdmin. - Coderthemes.com
                </footer>



            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->



        <!-- jQuery  -->
        <script src="<?=base_url();?>assets/js/jquery.min.js"></script>
        <script src="<?=base_url();?>assets/js/popper.min.js"></script>
        <script src="<?=base_url();?>assets/js/bootstrap.min.js"></script>
        <script src="<?=base_url();?>assets/js/metisMenu.min.js"></script>
        <script src="<?=base_url();?>assets/js/waves.js"></script>
        <script src="<?=base_url();?>assets/js/jquery.slimscroll.js"></script>

        <!--Form Wizard-->
        <script src="<?=base_url();?>assets/plugins/jquery.steps/js/jquery.steps.min.js" type="text/javascript"></script>

        <!--wizard initialization-->
        <script src="<?=base_url();?>assets/pages/jquery.wizard-init.js" type="text/javascript"></script>

        <!-- Sweet Alert Js  -->
        <script src="<?=base_url();?>assets/plugins/sweet-alert/sweetalert2.min.js"></script>
        <script src="<?=base_url();?>assets/pages/jquery.sweet-alert.init.js"></script>

        <!-- App js -->
        <script src="<?=base_url();?>assets/js/jquery.core.js"></script>
        <script src="<?=base_url();?>assets/js/jquery.app.js"></script>
        
        <!-- script menampilkan data di bagian konfirmasi setelah user isi data -->
        <script type="text/javascript">
            $("#nik").keyup(function(){
                var nik = $('[name="nik_peserta"]').val();
                document.getElementById("span_nik_peserta").innerHTML = nik;
            });
            
            $("#sesi_program").change(function(){
                var sesi_program = $('[name="sesi_program"]').val();
                document.getElementById("span_tanggal_waktu_peserta").innerHTML = sesi_program;
            });

            $("#kategori_program").change(function(){
                var kategori_program = $('[name="kategori_program"]').val();
                document.getElementById("span_kategori_program_peserta").innerHTML = kategori_program;
            });

            $("#nama_depan_peserta").keyup(function(){
                var nama_depan_peserta = $('[name="nama_depan_peserta"]').val();
                var nama_belakang_peserta = $('[name="nama_belakang_peserta"]').val();
                document.getElementById("span_nama_lengkap_peserta").innerHTML = nama_depan_peserta+" "+nama_belakang_peserta;
            });
            $("#nama_belakang_peserta").keyup(function(){
                var nama_depan_peserta = $('[name="nama_depan_peserta"]').val();
                var nama_belakang_peserta = $('[name="nama_belakang_peserta"]').val();
                document.getElementById("span_nama_lengkap_peserta").innerHTML = nama_depan_peserta+" "+nama_belakang_peserta;
            });

            $("#email_peserta").keyup(function(){
                var email_peserta = $('[name="email_peserta"]').val();
                document.getElementById("span_email_peserta").innerHTML = email_peserta;
            });

            $("#phone_peserta").keyup(function(){
                var phone_peserta = $('[name="phone_peserta"]').val();
                document.getElementById("span_phone_peserta").innerHTML = phone_peserta;
            });


            $("#datepicker-autoclose").keyup(function(){
                var tanggal_lahir_peserta = $('[name="tanggal_lahir_peserta"]').val();
                document.getElementById("span_tanggal_lahir_peserta").innerHTML = tanggal_lahir_peserta;
            });

            $("#customRadio1").click(function(){
                var jenis_kelamin_peserta = $('#customRadio1').val();
                document.getElementById("span_jenis_kelamin_peserta").innerHTML = jenis_kelamin_peserta;
            });

            $("#customRadio2").click(function(){
                var jenis_kelamin_peserta = $('#customRadio2').val();
                document.getElementById("span_jenis_kelamin_peserta").innerHTML = jenis_kelamin_peserta;
            });

            $("#kota").change(function(){
                var kota_peserta = $('[name="kota_peserta"]').val();
                document.getElementById("span_kota_peserta").innerHTML = kota_peserta;
            });

            $("#alamat_peserta").keyup(function(){
                var alamat_peserta = $('[name="alamat_peserta"]').val();
                document.getElementById("span_alamat_lengkap_peserta").innerHTML = alamat_peserta;
            });

            $("#pekerjaan_peserta").keyup(function(){
                var pekerjaan_peserta = $('[name="pekerjaan_peserta"]').val();
                document.getElementById("span_pekerjaan_peserta").innerHTML = pekerjaan_peserta;
            });

            $("#jenis_pekerjaan_peserta").keyup(function(){
                var jenis_pekerjaan_peserta = $('[name="jenis_pekerjaan_peserta"]').val();
                document.getElementById("span_jenis_pekerjaan_peserta").innerHTML = jenis_pekerjaan_peserta;
            });
        </script>

    </body>
</html>