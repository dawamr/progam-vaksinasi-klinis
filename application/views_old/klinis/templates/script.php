
        <!-- jQuery  -->
        <script src="<?=base_url()?>assets/js/jquery.min.js"></script>
        <script src="<?=base_url()?>assets/js/popper.min.js"></script>
        <script src="<?=base_url()?>assets/js/bootstrap.min.js"></script>
        <script src="<?=base_url()?>assets/js/metisMenu.min.js"></script>
        <script src="<?=base_url()?>assets/js/waves.js"></script>
        <script src="<?=base_url()?>assets/js/jquery.slimscroll.js"></script>

        <!-- plugin js -->
        <script src="<?=base_url()?>assets/plugins/moment/moment.js"></script>
        <script src="<?=base_url()?>assets/plugins/bootstrap-timepicker/bootstrap-timepicker.js"></script>
        <script src="<?=base_url()?>assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/clockpicker/js/bootstrap-clockpicker.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="<?=base_url()?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

        <!-- KNOB JS -->
        <!--[if IE]>
        <script type="text/javascript" src="<?=base_url()?>assets/plugins/jquery-knob/excanvas.js"></script>
        <![endif]
        <script src="<?=base_url()?>assets/plugins/jquery-knob/jquery.knob.js"></script>-->

        <!-- Dashboard Init -->
        <script src="<?=base_url()?>assets/pages/jquery.dashboard.init.js"></script>

        <!-- App js -->
        <script src="<?=base_url()?>assets/js/jquery.core.js"></script>
        <script src="<?=base_url()?>assets/js/jquery.app.js"></script>

        <!-- Init js -->
        <script src="<?=base_url()?>assets/pages/jquery.form-pickers.init.js"></script>

        <script src="<?=base_url()?>assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js"></script>

        <script src="<?=base_url()?>assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
        <script src="<?=base_url()?>assets/plugins/autoNumeric/autoNumeric.js" type="text/javascript"></script>

        <!--Summernote js-->
        <script src="<?=base_url()?>assets/plugins/summernote/summernote-bs4.min.js"></script>

        <!-- Modal-Effect -->
        <script src="<?=base_url()?>assets/plugins/custombox/js/custombox.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/custombox/js/legacy.min.js"></script>

        <!-- Datatables -->
        <script src="<?=base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>
        <script src="<?=base_url()?>assets/plugins/datatables/dataTables.responsive.min.js"></script>

        <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>

        <?php include "js_dashboard.php"?>
        <?php include "js_program.php"?>

        <script>
            jQuery(document).ready(function(){
                $('.summernote').summernote({
                    height: 350,                 // set editor height
                    minHeight: null,             // set minimum height of editor
                    maxHeight: null,             // set maximum height of editor
                    focus: false                 // set focus to editable area after initializing summernote
                });

                $('.inline-editor').summernote({
                    airMode: true
                });
            });
        </script>

        <script type="text/javascript">
            jQuery(function($) {
                $('.autonumber').autoNumeric('init');
            });
            jQuery.browser = {};
            (function () {
                jQuery.browser.msie = false;
                jQuery.browser.version = 0;
                if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
                    jQuery.browser.msie = true;
                    jQuery.browser.version = RegExp.$1;
                }
            })();
        </script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#datatable').dataTable();
                $('#example').DataTable({
                    "dom":' <"search"f><"top p">rt<"bottom"p><"clear">',
                    // "order": [[ 0, "ASC" ]],
                    "lengthMenu": [[10, 25, 50, -1], [5, 25, 50, "All"]]
                });
            });
            
            $(function(){
                $(".flash-messages").delay(2500).fadeOut();
                setTimeout(function(){ 
                    <?php $this->session->set_flashdata('message',"");?>
                }, 3000);
            });
        </script>

        <script>
        $('#timepicker').timepicker({
            uiLibrary: 'bootstrap4'
        });
      
        $('#timepicker2').timepicker({
            uiLibrary: 'bootstrap4'
        });
        $('#datepicker').datepicker();
        $('#datepicker2').datepicker();
    </script>

    </body>
</html>