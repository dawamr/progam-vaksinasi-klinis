<script type="text/javascript">
	$(".btn_edit_program").click(function(){

		var id_program = $(this).data("id_program");
		$.ajax({
            type : "POST",
            url: "<?= base_url('Get/getProgramById')?>",
            dataType : "JSON",
            data : {id_program:id_program},
            success: function(data){
    			$('.edit-program').modal('show');

                $('[name="edit_id_program"]').val(data.id_program);
                $('[name="edit_id_user"]').val(data.id_user);
                $('[name="edit_tipe_program"]').val(data.tipe_program);
                $('[name="edit_nama_program"]').val(data.nama_program);
                $('[name="edit_deskripsi_program"]').val(data.deskripsi_program);
                $('#edit_program_start_date').datepicker('setDate', new Date(data.tanggal_mulai_program));
                $('#edit_program_end_date').datepicker('setDate', new Date(data.tanggal_berakhir_program));
                $('[name="edit_lokasi_program"]').val(data.lokasi_program);
                $('[name="edit_target_peserta_program"]').val(data.target_peserta_program);
                $('input[name="edit_target_klien_program"]').tagsinput( "add", data.target_klien_program );
                $('[name="edit_goals_program"]').val(data.goals_program);
                $('[name="edit_anggaran_program"]').val(data.anggaran_program);
            }
        });
	});
	$(".btn_hapus_program").click(function(){
		var id_program = $(this).data("id_program");
        $('[name="id_program"]').val(id_program);
        $('.hapus-program').modal('show');
    });
    
    $(".btn_decline_aktivasi_peserta").click(function(){
        var id_koneksi = $(this).data("id_koneksi");
        var link_url = $(this).data("link_url");
        $('[name="id_koneksi"]').val(id_koneksi);
        $('[name="link_url"]').val(link_url);
        $('.decline-peserta-program').modal('show');
    });

    $("#btn_link_formulir_berakhir").click(function(){
        $('.link-formulir-berakhir').modal('show');
    });

    $(".btn_edit_sesi").click(function(){
        var id_sesi = $(this).data("id_sesi");

        $.ajax({
            type : "POST",
            url: "<?= base_url('Get/getSesiById')?>",
            dataType : "JSON",
            data : {id_sesi:id_sesi},
            success: function(data){
                $('[name="id_sesi"]').val(data.id_sesi);
                $('#tanggal_jadwal_program').datepicker('setDate', new Date(data.tanggal_sesi));
                // $('[name="tanggal_jadwal_program"]').val(data.tanggal_sesi);
                $('[name="waktu_jadwal_program_mulai"]').val(data.waktu_mulai_sesi);
                $('[name="waktu_jadwal_program_berakhir"]').val(data.waktu_berakhir_sesi);
                $('[name="kuota_jadwal_program"]').val(data.kuota_sesi);
            }
        });
    });


    $(".btn_reset_input_sesi").click(function(){
        $('[name="id_sesi"]').val("");
        $('#tanggal_jadwal_program').datepicker('setDate', new Date());
        $('[name="waktu_jadwal_program_mulai"]').val("");
        $('[name="waktu_jadwal_program_berakhir"]').val("");
        $('[name="kuota_jadwal_program"]').val("");    
    });

    $(".btn_delete_sesi").click(function(){
        var id_sesi = $(this).data("id_sesi");
        var link_url = $(this).data("link_url");
        $('[name="id_sesi_hapus"]').val(id_sesi);
        $('[name="link_url_hapus_sesi"]').val(link_url);
        $('.hapus-sesi-program').modal('show');
    });

    $(".btn_reschedule_peserta").click(function(){
        var id_koneksi = $(this).data("id_koneksi");
        var id_peserta = $(this).data("id_peserta");
        var tanggal_terdaftar = $(this).data("tanggal_terdaftar");
        var link_url = $(this).data("link_url");
        var tanggal_mulai_program = $(this).data("tanggal_mulai_program");
        var sesi_terdaftar = $(this).data("sesi_terdaftar");

        $('[name="id_koneksi"]').val(id_koneksi);
        $('[name="id_peserta"]').val(id_peserta);
        $('[name="tanggal_terdaftar"]').val(tanggal_terdaftar);
        $('[name="link_url"]').val(link_url);
        $('[name="reschedule_sesi_program"]').val(sesi_terdaftar);
        $('[name="reschedule_sesi_program_input"]').val(sesi_terdaftar);

        var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
        var firstDate = new Date();
        var secondDate = new Date(tanggal_mulai_program);
        var diffDays = Math.round(Math.round((secondDate.getTime() - firstDate.getTime()) / (oneDay)));
        if(diffDays < 3){
            $('.reschedule-berakhir').modal('show');
        }else{
            $('.reschedule-peserta-program').modal('show');
        }
    });
    function cekReschedule(){
        var reschedule_sesi_program = $("#reschedule_sesi_program").val();
        var new_reschedule_sesi_program = $("#new_reschedule_sesi_program").val();
        if(reschedule_sesi_program == new_reschedule_sesi_program){
            alert("Sesi reschedule tidak boleh sama dengan yang sebelumnya");
            $("#new_reschedule_sesi_program").val("");
        }
    };
</script>