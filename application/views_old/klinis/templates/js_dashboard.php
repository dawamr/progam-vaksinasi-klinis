<script type="text/javascript">
	$("#btn_modal_edit_faskes").click(function(){
		var id_faskes = $(this).data("id_faskes");
		$.ajax({
            type : "POST",
            url: "<?= base_url('Get/getFaskesById')?>",
            dataType : "JSON",
            data : {id_faskes:id_faskes},
            success: function(data){
                $.each(data,function(id){
                    $('.edit-faskes').modal('show');
                    document.getElementById("img_temp_faskes").src = "<?= base_url('assets/logo_mitra/')?>"+data.logo_mitra;
                    $('[name="id_faskes"]').val(data.id_faskes);
                    $('[name="foto_faskes"]').val(data.logo_mitra);
                    $('[name="edit_nama_faskes"]').val(data.nama_faskes);
                    $('[name="edit_tipe_faskes"]').val(data.tipe_faskes);
                    $('[name="edit_alamat_faskes"]').val(data.alamat_faskes);
                    $('[name="edit_kota_faskes"]').val(data.kota_faskes);
                    $('[name="edit_email_faskes"]').val(data.email_faskes);
                    $('[name="edit_phone_faskes"]').val(data.phone_faskes);
                    $('[name="edit_longtitude_faskes"]').val(data.longtitude_faskes);
                    $('[name="edit_latitude_faskes"]').val(data.latitude_faskes);
                });
            }
        });
	});
	$("#btn_modal_edit_admin").click(function(){
        var id_user = $(this).data("id_user");
        $.ajax({
            type : "POST",
            url: "<?= base_url('Get/getUserById')?>",
            dataType : "JSON",
            data : {id_user:id_user},
            success: function(data){
                $.each(data,function(id){
                    $('.edit-admin').modal('show');
                    $('[name="id_user"]').val(data.id_user);
                    $('[name="edit_nama_user"]').val(data.nama_user);
                    $('[name="edit_email_user"]').val(data.email_user);
                });
            }
        });
	});

    $("#btn_modal_change_password").click(function(){
        var id_user = $(this).data("id_user");
        $.ajax({
            type : "POST",
            url: "<?= base_url('Get/getUserById')?>",
            dataType : "JSON",
            data : {id_user:id_user},
            success: function(data){
                $.each(data,function(id){
                    $(".change-password-admin").modal("show");
                    $('[name="id_user"]').val(data.id_user);
                });
            }
        });
    });
    

</script>