<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('id_user')){
        	redirect("Auth");
        }
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){
		$x['title'] = "Dashboard - CMS Klinis";
		$x['page_title'] = "Dashboard";
		$data_user = $this->db->get_where('table_user', ['id_user'=>$this->session->userdata('id_user')])->row_array();
		$x['data_user'] = $data_user;
		$id_faskes = $data_user['id_faskes'];
		$x['data_faskes'] = $this->db->get_where('table_faskes', ['id_faskes'=>$id_faskes])->row_array();

		$this->load->view('klinis/templates/header', $x);
		$this->load->view('klinis/templates/style');
		$this->load->view('klinis/templates/sidebar', $x);
		$this->load->view('klinis/pages/dashboard');
		$this->load->view('klinis/templates/script');
		$this->load->view('klinis/templates/modal');
		$this->load->view('klinis/templates/footer');
	}
	public function logout(){
		$this->session->unset_userdata('id_user');
		$this->session->unset_userdata('id_faskes');
		$this->session->unset_userdata('logo_faskes');
		$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Anda telah keluar, silahkan masuk lagi !</div>');
		redirect('auth');
	}

	public function updateFaskes(){
		$id_faskes = $this->input->post("id_faskes");
		$foto_faskes = $this->input->post("foto_faskes");
		$edit_foto_faskes = $this->input->post("edit_foto_faskes");		
		$edit_nama_faskes = $this->input->post("edit_nama_faskes");
		$edit_tipe_faskes = $this->input->post("edit_tipe_faskes");
		$edit_alamat_faskes = $this->input->post("edit_alamat_faskes");
		$edit_kota_faskes = $this->input->post("edit_kota_faskes");
		$edit_email_faskes = $this->input->post("edit_email_faskes");
		$edit_phone_faskes = $this->input->post("edit_phone_faskes");
		$edit_longtitude_faskes = $this->input->post("edit_longtitude_faskes");
		$edit_latitude_faskes = $this->input->post("edit_latitude_faskes");

		$config['upload_path']          = './assets/logo_mitra/';
        $config['allowed_types']        = 'jpg|png|jpeg';
        $config['max_size']             = 0;

        $this->load->library('upload', $config);
		$this->upload->initialize($config);
        if ( ! $this->upload->do_upload('edit_foto_faskes'))
        {
            $logo_mitra = $foto_faskes;
        }
        else
        {
            $logo_mitra = $this->upload->data('file_name');
            $this->session->unset_userdata('logo_faskes');
            $this->session->set_userdata('logo_faskes', $logo_mitra);
        }

        $data_faskes = [
        	"nama_faskes" => $edit_nama_faskes,
        	"tipe_faskes" => $edit_tipe_faskes,
        	"alamat_faskes" => $edit_alamat_faskes,
        	"kota_faskes" => $edit_kota_faskes,
        	"email_faskes" => $edit_email_faskes,
        	"phone_faskes" => $edit_phone_faskes,
        	"longtitude_faskes" => $edit_longtitude_faskes,
        	"latitude_faskes" => $edit_latitude_faskes,
        	"logo_mitra" => $logo_mitra,
        ];
        $this->db->WHERE('id_faskes', $id_faskes);
        $this->db->UPDATE('table_faskes', $data_faskes);
		$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data berhasil diubah!</div>');
        redirect('Dashboard');
	}

	public function updateUser(){
		$id_user = $this->input->post("id_user");
		$edit_nama_user = $this->input->post("edit_nama_user");
		$edit_email_user = $this->input->post("edit_email_user");

		$this->db->SET('nama_user', $edit_nama_user);
		$this->db->SET('email_user', $edit_email_user);
		$this->db->WHERE('id_user', $id_user);
		$this->db->UPDATE('table_user');
		redirect('Dashboard');
	}
	public function changePassword(){

		$id_user = $this->input->post('id_user');
		$current_password = $this->input->post('old_password');
		$new_password = $this->input->post('new_password');
		$new_password1 = $this->input->post('new_password1');
		
		$user = $this->db->get_where('table_user',['id_user' => $id_user])->row_array();
		
		if(!password_verify($current_password, $user['password_user'])){
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Password lama salah!</div>');
		}else{
			if($current_password == $new_password){
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Password Baru tidak boleh sama dengan Password Sekarang!</div>');
			}else{
				if($new_password != $new_password1){
					$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Password Konfirmasi tidak sama!</div>');
				}else{
					$new_password_hash = password_hash($new_password, PASSWORD_DEFAULT);
					$this->db->SET("password_user", $new_password_hash);
					$this->db->WHERE('id_user', $user['id_user']);
					$this->db->UPDATE('table_user');
					$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil ganti password! Silahkan Login Ulang</div>');

					$this->session->unset_userdata('id_user');
					$this->session->unset_userdata('id_faskes');
					$this->session->unset_userdata('logo_faskes');
					redirect('auth');
				}
			}
		}
		redirect('Dashboard');
	}

}
