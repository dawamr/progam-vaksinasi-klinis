<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Get extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->Model('M_get');
    }
    public function getFaskesById(){
        $id_faskes = $this->input->post("id_faskes");
        $result = $this->M_get->getFaskesById($id_faskes);
        echo json_encode($result);
    }
    public function getUserById(){
        $id_user = $this->input->post("id_user");
        $result = $this->M_get->getUserById($id_user);
        echo json_encode($result);
    }
    public function getProgramById(){
        $id_program = $this->input->post("id_program");
        $result = $this->M_get->getProgramById($id_program);
        echo json_encode($result);
    }
    public function getSesiById(){
        $id_sesi = $this->input->post("id_sesi");
        $result = $this->M_get->getSesiById($id_sesi);
        echo json_encode($result);
    }
    
}
