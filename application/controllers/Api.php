<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	public function listProgam()
	{
		$abc = $this->db->get_where('table_program', ['status_program'=> 'active', 'tanggal_berakhir_program >=' => date('Y-m-d')])->result_array();

		$test = [];
		for ($i=0; $i < count($abc); $i++) { 
			$replace_url_nama = str_replace(" ", "-", $abc[$i]['nama_program']);
			$replace_url_tanggal = str_replace("-", "", $abc[$i]['tanggal_mulai_program']);
			
			$abc[$i]["url"] = base_url("Registrasi/Form/").$replace_url_nama."/".$abc[$i]['id_program'].$replace_url_tanggal;
		}

		// $x['data'] =$abc;

		header('Content-Type: application/json');
		echo json_encode($abc);
	}

	public function vaksin5()
	{
		$response = WpOrg\Requests\Requests::get('https://1cb6d402-793c-425a-a6db-5aec712f7ea2.mock.pstmn.io/vaksin-5');
		$result = json_decode($response->body);

		$kategori_all = $this->db->get_where('table_kategori_program')->result_array();
		$sesi_all = $this->db->get_where('table_sesi_program', ['id_program'=> 26])->result_array();


		$data = [];
		$data_koneksi = [];
		for ($i=0; $i < count($result); $i++) { 

			$id = array_search($result[$i]->kode_kategori, array_column($kategori_all, 'kode_kategori'));
			$id2 = array_search($result[$i]->date_book_patient, array_column($sesi_all, 'tanggal_sesi'));

			array_push($data, [
				"id_peserta" => $i + 6,
				"nik_peserta" => $result[$i]->nik,
				"nama_peserta" => $result[$i]->nama,
				"email_peserta" => null,
				"phone_peserta" => $result[$i]->no_hp,
				"tanggal_lahir_peserta" => $result[$i]->tanggal_lahir,
				"jenis_kelamin_peserta" => ($result[$i]->kelamin == "L" || $result[$i]->kelamin == "1" ) ? "Laki-laki" : "Perempuan",
				"kota_peserta" => $result[$i]->kode_kabupaten_tempat_kerja,
				"alamat_peserta" => $result[$i]->alamat_ktp,
				"pekerjaan_peserta" => $result[$i]->kode_kategori, //"jenis_pekerjaan_peserta" => $jenis_pekerjaan_peserta,
				"jenis_pekerjaan_peserta" => $result[$i]->jenis_pekerjaan,
				"instansi_pekerjaan" => $result[$i]->instansi_pekerjaan,
				"status_peserta" => ($result[$i]->order_status == "approved") ? "active" : "waiting",
				
			]);

			array_push($data_koneksi, [
                "id_peserta" => $i + 6,
                "id_sesi" => $sesi_all[$id2]['id_sesi'],
                "kategori_program" => $kategori_all[$id]['nama_kategori'],
                "status_koneksi" => ($result[$i]->order_status == "approved") ? "Confirmed" : "Waiting",
            ]);
		}

		// $this->db->insert_batch('table_peserta', $data);
		// $this->db->insert_batch('table_koneksi_program', $data_koneksi);

		

		return var_dump('ok');
	}

}
