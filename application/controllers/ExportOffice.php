<?php defined('BASEPATH') OR die('No direct script access allowed');

require('./application/third_party/phpoffice/vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ExportOffice extends CI_Controller {

     public function __construct()
     {
          parent::__construct();
     }

     public function index()
     {
        Echo "File not Found";
     }

     public function Excel(){
        $id_program = $this->input->post("id_program_filter");
        $tanggal_program_start = $this->input->post("pilih_tanggal_export_start");
        $tanggal_program_end = $this->input->post("pilih_tanggal_export_end");
        $status_peserta = $this->input->post("pilih_status_export");

        if($status_peserta == "Semua"){
            if($tanggal_program_start == $tanggal_program_end){
                $sql_search_data_peserta = "
                    SELECT a.*, b.*, c.*, d.*, e.* FROM table_sesi_program a
                    LEFT JOIN table_koneksi_program b ON a.id_sesi = b.id_sesi
                    LEFT JOIN table_peserta c ON b.id_peserta = c.id_peserta
                    LEFT JOIN table_kota_kabupaten d ON c.kota_peserta = d.kode_kota_kabupaten
                    LEFT JOIN table_kategori_program e ON c.pekerjaan_peserta = e.kode_kategori
                    WHERE a.id_program = '$id_program'
                    AND a.tanggal_sesi = '$tanggal_program_start'
                    ORDER BY a.tanggal_sesi ASC
                ";
            }else{
                $sql_search_data_peserta = "
                    SELECT a.*, b.*, c.*, d.*, e.* FROM table_sesi_program a
                    LEFT JOIN table_koneksi_program b ON a.id_sesi = b.id_sesi
                    LEFT JOIN table_peserta c ON b.id_peserta = c.id_peserta
                    LEFT JOIN table_kota_kabupaten d ON c.kota_peserta = d.kode_kota_kabupaten
                    LEFT JOIN table_kategori_program e ON c.pekerjaan_peserta = e.kode_kategori
                    WHERE a.id_program = '$id_program'
                    AND a.tanggal_sesi >= '$tanggal_program_start'
                    AND a.tanggal_sesi <= '$tanggal_program_end'
                    ORDER BY a.tanggal_sesi ASC
                ";
            }
        }else{
            if($tanggal_program_start == $tanggal_program_end){
                $sql_search_data_peserta = "
                    SELECT a.*, b.*, c.*, d.*, e.* FROM table_sesi_program a
                    LEFT JOIN table_koneksi_program b ON a.id_sesi = b.id_sesi
                    LEFT JOIN table_peserta c ON b.id_peserta = c.id_peserta
                    LEFT JOIN table_kota_kabupaten d ON c.kota_peserta = d.kode_kota_kabupaten
                    LEFT JOIN table_kategori_program e ON c.pekerjaan_peserta = e.kode_kategori
                    WHERE a.id_program = '$id_program'
                    AND a.tanggal_sesi = '$tanggal_program_start'
                    AND b.status_koneksi = '$status_peserta'
                    ORDER BY a.tanggal_sesi ASC
                ";
            }else{
                $sql_search_data_peserta = "
                    SELECT a.*, b.*, c.*, d.*, e.* FROM table_sesi_program a
                    LEFT JOIN table_koneksi_program b ON a.id_sesi = b.id_sesi
                    LEFT JOIN table_peserta c ON b.id_peserta = c.id_peserta
                    LEFT JOIN table_kota_kabupaten d ON c.kota_peserta = d.kode_kota_kabupaten
                    LEFT JOIN table_kategori_program e ON c.pekerjaan_peserta = e.kode_kategori
                    WHERE a.id_program = '$id_program'
                    AND a.tanggal_sesi >= '$tanggal_program_start'
                    AND a.tanggal_sesi <= '$tanggal_program_end'
                    AND b.status_koneksi = '$status_peserta'
                    ORDER BY a.tanggal_sesi ASC
                ";
            }
        }

        $data_peserta_program = $this->db->query($sql_search_data_peserta)->result_array();

        $data_program = $this->db->get_where('table_program', ['id_program'=>$id_program])->row_array();
        $tanggal_hari_ini = date("d F Y");




        $spreadsheet = new Spreadsheet;

        $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'No')
                    ->setCellValue('B1', 'NIK')
                    ->setCellValue('C1', 'Name')
                    ->setCellValue('D1', 'L/P')
                    ->setCellValue('E1', 'Birth')
                    ->setCellValue('F1', 'Age')
                    ->setCellValue('G1', 'Instansi')
                    ->setCellValue('H1', 'Pekerjaan')
                    ->setCellValue('I1', 'Phone')
                    ->setCellValue('J1', 'Alamat')
                    ->setCellValue('K1', 'Kota / Kab');

        $kolom = 2;
        $nomor = 1;
        foreach($data_peserta_program as $dpp){
            if($dpp['id_peserta'] != ""){
                $nama_peserta = str_replace("-", " ", $dpp['nama_peserta']);
                
                // change echo gender
                if($dpp['jenis_kelamin_peserta'] == "Laki-laki"){
                    $jenis_kelamin = "L";
                }else{
                    $jenis_kelamin = "P";
                }

                //tanggal lahir
                $tanggal_lahir = new DateTime($dpp['tanggal_lahir_peserta']);
                //tanggal hari ini
                $today = new DateTime('today');
                //tahun
                $umur = $today->diff($tanggal_lahir)->y;

                $string_instansi_pekerjaan = $dpp['nama_kategori'];
                $string_jenis_pekerjaan = $dpp['jenis_pekerjaan_peserta'];
                $string_alamat_peserta = $dpp['alamat_peserta'];
                $string_nama_kota = $dpp['nama_kota_kabupaten'];

                // Set cell A9 with a numeric value
                // $spreadsheet->getActiveSheet()->setCellValue('B'.$kolom, 1513789642);
                // Set a number format mask to display the value as 11 digits with leading zeroes
                $spreadsheet->getActiveSheet()->getStyle('B'.$kolom)
                    ->getNumberFormat()
                    ->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER );
                
                $spreadsheet->getActiveSheet()->getStyle('I'.$kolom)
                    ->getNumberFormat()
                    ->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_NUMBER );
                
                $spreadsheet->getActiveSheet()->getStyle('E'.$kolom)
                    ->getNumberFormat()
                    ->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_DATE_DDMMYYYY );
                    
                $spreadsheet->setActiveSheetIndex(0)
                            ->setCellValue('A'.$kolom, $nomor)
                            ->setCellValue('B'.$kolom, $dpp['nik_peserta'])
                            ->setCellValue('C'.$kolom, $nama_peserta)
                            ->setCellValue('D'.$kolom, $jenis_kelamin)
                            ->setCellValue('E'.$kolom, date('j F Y', strtotime($dpp['tanggal_lahir_peserta'])))
                            ->setCellValue('F'.$kolom, $umur)
                            ->setCellValue('G'.$kolom, $string_instansi_pekerjaan)
                            ->setCellValue('H'.$kolom, $string_jenis_pekerjaan)
                            ->setCellValue('I'.$kolom, $dpp['phone_peserta'])
                            ->setCellValue('J'.$kolom, $string_alamat_peserta)
                            ->setCellValue('K'.$kolom, $string_nama_kota);
               $kolom++;
               $nomor++;
            }
        }

        $writer = new Xlsx($spreadsheet);
        $filename="Daftar Peserta ".$data_program['nama_program']." - ".$tanggal_hari_ini.".xlsx";
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename='.$filename);
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
     }

     public function PDF(){
        $id_program = $this->input->post("id_program_filter");
        $tanggal_program_start = $this->input->post("pilih_tanggal_export_start");
        $tanggal_program_end = $this->input->post("pilih_tanggal_export_end");
        $status_peserta = $this->input->post("pilih_status_export");

        if($status_peserta == "Semua"){
            if($tanggal_program_start == $tanggal_program_end){
                $sql_search_data_peserta = "
                    SELECT a.*, b.*, c.*, d.*, e.* FROM table_sesi_program a
                    LEFT JOIN table_koneksi_program b ON a.id_sesi = b.id_sesi
                    LEFT JOIN table_peserta c ON b.id_peserta = c.id_peserta
                    LEFT JOIN table_kota_kabupaten d ON c.kota_peserta = d.kode_kota_kabupaten
                    LEFT JOIN table_kategori_program e ON c.pekerjaan_peserta = e.kode_kategori
                    WHERE a.id_program = '$id_program'
                    AND a.tanggal_sesi = '$tanggal_program_start'
                    ORDER BY a.tanggal_sesi ASC
                ";
            }else{
                $sql_search_data_peserta = "
                    SELECT a.*, b.*, c.*, d.*, e.* FROM table_sesi_program a
                    LEFT JOIN table_koneksi_program b ON a.id_sesi = b.id_sesi
                    LEFT JOIN table_peserta c ON b.id_peserta = c.id_peserta
                    LEFT JOIN table_kota_kabupaten d ON c.kota_peserta = d.kode_kota_kabupaten
                    LEFT JOIN table_kategori_program e ON c.pekerjaan_peserta = e.kode_kategori
                    WHERE a.id_program = '$id_program'
                    AND a.tanggal_sesi >= '$tanggal_program_start'
                    AND a.tanggal_sesi <= '$tanggal_program_end'
                    ORDER BY a.tanggal_sesi ASC
                ";
            }
        }else{
            if($tanggal_program_start == $tanggal_program_end){
                $sql_search_data_peserta = "
                    SELECT a.*, b.*, c.*, d.*, e.* FROM table_sesi_program a
                    LEFT JOIN table_koneksi_program b ON a.id_sesi = b.id_sesi
                    LEFT JOIN table_peserta c ON b.id_peserta = c.id_peserta
                    LEFT JOIN table_kota_kabupaten d ON c.kota_peserta = d.kode_kota_kabupaten
                    LEFT JOIN table_kategori_program e ON c.pekerjaan_peserta = e.kode_kategori
                    WHERE a.id_program = '$id_program'
                    AND a.tanggal_sesi = '$tanggal_program_start'
                    AND b.status_koneksi = '$status_peserta'
                    ORDER BY a.tanggal_sesi ASC
                ";
            }else{
                $sql_search_data_peserta = "
                    SELECT a.*, b.*, c.*, d.*, e.* FROM table_sesi_program a
                    LEFT JOIN table_koneksi_program b ON a.id_sesi = b.id_sesi
                    LEFT JOIN table_peserta c ON b.id_peserta = c.id_peserta
                    LEFT JOIN table_kota_kabupaten d ON c.kota_peserta = d.kode_kota_kabupaten
                    LEFT JOIN table_kategori_program e ON c.pekerjaan_peserta = e.kode_kategori
                    WHERE a.id_program = '$id_program'
                    AND a.tanggal_sesi >= '$tanggal_program_start'
                    AND a.tanggal_sesi <= '$tanggal_program_end'
                    AND b.status_koneksi = '$status_peserta'
                    ORDER BY a.tanggal_sesi ASC
                ";
            }
        }

        $data_peserta_program = $this->db->query($sql_search_data_peserta)->result_array();

        $data_program = $this->db->get_where('table_program', ['id_program'=>$id_program])->row_array();
        $tanggal_hari_ini = date("d F Y");

        $pdf = new FPDF('P','mm','A4');

        // membuat halaman baru
        $title = 'Daftar Peserta '.$data_program['nama_program'];
        $pdf->SetTitle($title );

        $pdf->AddPage('L');
        // $pdf->SetAutoPageBreak(false);
        // Jarak garis kiri, jarak atas, jarak kiri ke kanan, jarak atas ke bawah
        // $pdf->Rect(5,5,287,200);

        // jarak panjang cell , jarak lebar cell, isi data,
        // 0 tidak ada garis, 1 ada garis
        // 0 tidak enter, 1 enter
        // posisi text R right, C center, L left
        // $pdf->Cell(50, 20,'Tgl / Date :',1,0,'R');


        $pdf->SetFont('Arial','B',15);
        $pdf->Cell(277, 8,"DAFTAR PESERTA PROGRAM",0,1,'C');
        $pdf->Cell(277, 8,"'".$data_program['nama_program']."'",0,1,'C');

        $pdf->SetFont('Arial','',9);
        $pdf->Cell(277, 5,"Dicetak tanggal : ".$tanggal_hari_ini,0,1,'L');
        $pdf->SetFont('Arial','B',8);

        $pdf->Cell(30, 6,"NIK",1,0,'C');
        $pdf->Cell(45, 6,"Name" ,1,0,'C');
        $pdf->Cell(8, 6,"L/P" ,1,0,'C');
        $pdf->Cell(20, 6,"Birth" ,1,0,'C');
        $pdf->Cell(8, 6,"Age" ,1,0,'C');
        $pdf->Cell(40, 6,"Instansi" ,1,0,'C');
        $pdf->Cell(30, 6,"Pekerjaan" ,1,0,'C');
        $pdf->Cell(23, 6,"Phone" ,1,0,'C');
        $pdf->Cell(38, 6,"Alamat" ,1,0,'C');
        $pdf->Cell(35, 6,"Kota/Kab" ,1,1,'C');

        foreach($data_peserta_program as $dpp){
            if($dpp['id_peserta'] != ""){
                $nama_peserta = str_replace("-", " ", $dpp['nama_peserta']);
                
                // change echo gender
                if($dpp['jenis_kelamin_peserta'] == "Laki-laki"){
                    $jenis_kelamin = "L";
                }else{
                    $jenis_kelamin = "P";
                }

                //tanggal lahir
                $tanggal_lahir = new DateTime($dpp['tanggal_lahir_peserta']);
                //tanggal hari ini
                $today = new DateTime('today');
                //tahun
                $umur = $today->diff($tanggal_lahir)->y;

                $string_instansi_pekerjaan = $dpp['nama_kategori'];
                $string_jenis_pekerjaan = $dpp['jenis_pekerjaan_peserta'];
                $string_alamat_peserta = $dpp['alamat_peserta'];
                $string_nama_kota = $dpp['nama_kota_kabupaten'];

                $cellHeight=5; //tinggi sel satu baris normal

                $cellWidth_nama=45; //lebar sel nama
                $line_nama = 0;
                if($pdf->GetStringWidth($nama_peserta) > $cellWidth_nama){
                    //jika ya, maka hitung ketinggian yang dibutuhkan untuk sel akan dirapikan
                    //dengan memisahkan teks agar sesuai dengan lebar sel
                    //lalu hitung berapa banyak baris yang dibutuhkan agar teks pas dengan sel
                    
                    $textLength=strlen($nama_peserta);	//total panjang teks
                    $errMargin=5;		//margin kesalahan lebar sel, untuk jaga-jaga
                    $startChar=0;		//posisi awal karakter untuk setiap baris
                    $maxChar=0;			//karakter maksimum dalam satu baris, yang akan ditambahkan nanti
                    $textArray=array();	//untuk menampung data untuk setiap baris
                    $tmpString="";		//untuk menampung teks untuk setiap baris (sementara)
                    
                    while($startChar < $textLength){ //perulangan sampai akhir teks
                        //perulangan sampai karakter maksimum tercapai
                        while( 
                        $pdf->GetStringWidth( $tmpString ) < ($cellWidth_nama-$errMargin) && ($startChar+$maxChar) < $textLength ) {
                            $maxChar++;
                            $tmpString=substr($nama_peserta,$startChar,$maxChar);
                            $tmpString;
                        }
                        //pindahkan ke baris berikutnya
                        $startChar=$startChar+$maxChar;
                        //kemudian tambahkan ke dalam array sehingga kita tahu berapa banyak baris yang dibutuhkan
                        array_push($textArray,$tmpString);
                        //reset variabel penampung
                        $maxChar=0;
                        $tmpString='';
                        
                    }
                    //dapatkan jumlah baris
                    $line_nama=count($textArray);
                }else{
                    //jika tidak, maka tidak melakukan apa-apa
                    $line_nama=1;
                }

                $cellWidth_instansi=40; //lebar sel Instansi
                $line_instansi = 0;
                if($pdf->GetStringWidth($string_instansi_pekerjaan) > $cellWidth_instansi){
                    //jika ya, maka hitung ketinggian yang dibutuhkan untuk sel akan dirapikan
                    //dengan memisahkan teks agar sesuai dengan lebar sel
                    //lalu hitung berapa banyak baris yang dibutuhkan agar teks pas dengan sel
                    
                    $textLength=strlen($string_instansi_pekerjaan);	//total panjang teks
                    $errMargin=5;		//margin kesalahan lebar sel, untuk jaga-jaga
                    $startChar=0;		//posisi awal karakter untuk setiap baris
                    $maxChar=0;			//karakter maksimum dalam satu baris, yang akan ditambahkan nanti
                    $textArray=array();	//untuk menampung data untuk setiap baris
                    $tmpString="";		//untuk menampung teks untuk setiap baris (sementara)
                    
                    while($startChar < $textLength){ //perulangan sampai akhir teks
                        //perulangan sampai karakter maksimum tercapai
                        while( 
                        $pdf->GetStringWidth( $tmpString ) < ($cellWidth_instansi-$errMargin) && ($startChar+$maxChar) < $textLength ) {
                            $maxChar++;
                            $tmpString=substr($string_instansi_pekerjaan,$startChar,$maxChar);
                            $tmpString;
                        }
                        //pindahkan ke baris berikutnya
                        $startChar=$startChar+$maxChar;
                        //kemudian tambahkan ke dalam array sehingga kita tahu berapa banyak baris yang dibutuhkan
                        array_push($textArray,$tmpString);
                        //reset variabel penampung
                        $maxChar=0;
                        $tmpString='';
                        
                    }
                    //dapatkan jumlah baris
                    $line_instansi=count($textArray);
                }else{
                    //jika tidak, maka tidak melakukan apa-apa
                    $line_instansi=1;
                }

                $cellWidth_jenis_pekerjaan=30; //lebar sel Jenis Pekerjaan
                $line_jenis_pekerjaan = 0;
                if($pdf->GetStringWidth($string_jenis_pekerjaan) > $cellWidth_jenis_pekerjaan){
                    //jika ya, maka hitung ketinggian yang dibutuhkan untuk sel akan dirapikan
                    //dengan memisahkan teks agar sesuai dengan lebar sel
                    //lalu hitung berapa banyak baris yang dibutuhkan agar teks pas dengan sel
                    
                    $textLength=strlen($string_jenis_pekerjaan);	//total panjang teks
                    $errMargin=5;		//margin kesalahan lebar sel, untuk jaga-jaga
                    $startChar=0;		//posisi awal karakter untuk setiap baris
                    $maxChar=0;			//karakter maksimum dalam satu baris, yang akan ditambahkan nanti
                    $textArray=array();	//untuk menampung data untuk setiap baris
                    $tmpString="";		//untuk menampung teks untuk setiap baris (sementara)
                    
                    while($startChar < $textLength){ //perulangan sampai akhir teks
                        //perulangan sampai karakter maksimum tercapai
                        while( 
                        $pdf->GetStringWidth( $tmpString ) < ($cellWidth_jenis_pekerjaan-$errMargin) && ($startChar+$maxChar) < $textLength ) {
                            $maxChar++;
                            $tmpString=substr($string_jenis_pekerjaan,$startChar,$maxChar);
                            $tmpString;
                        }
                        //pindahkan ke baris berikutnya
                        $startChar=$startChar+$maxChar;
                        //kemudian tambahkan ke dalam array sehingga kita tahu berapa banyak baris yang dibutuhkan
                        array_push($textArray,$tmpString);
                        //reset variabel penampung
                        $maxChar=0;
                        $tmpString='';
                        
                    }
                    //dapatkan jumlah baris
                    $line_jenis_pekerjaan=count($textArray);
                }else{
                    //jika tidak, maka tidak melakukan apa-apa
                    $line_jenis_pekerjaan=1;
                }
                
                $cellWidth_alamat=38; //lebar sel alamat
                $line_alamat = 0;
                //periksa apakah teksnya melibihi kolom?
                if($pdf->GetStringWidth($string_alamat_peserta) > $cellWidth_alamat){
                    //jika ya, maka hitung ketinggian yang dibutuhkan untuk sel akan dirapikan
                    //dengan memisahkan teks agar sesuai dengan lebar sel
                    //lalu hitung berapa banyak baris yang dibutuhkan agar teks pas dengan sel
                    
                    $textLength=strlen($string_alamat_peserta);	//total panjang teks
                    $errMargin=5;		//margin kesalahan lebar sel, untuk jaga-jaga
                    $startChar=0;		//posisi awal karakter untuk setiap baris
                    $maxChar=0;			//karakter maksimum dalam satu baris, yang akan ditambahkan nanti
                    $textArray=array();	//untuk menampung data untuk setiap baris
                    $tmpString="";		//untuk menampung teks untuk setiap baris (sementara)
                    
                    while($startChar < $textLength){ //perulangan sampai akhir teks
                        //perulangan sampai karakter maksimum tercapai
                        while( 
                        $pdf->GetStringWidth( $tmpString ) < ($cellWidth_alamat-$errMargin) && ($startChar+$maxChar) < $textLength ) {
                            $maxChar++;
                            $tmpString=substr($string_alamat_peserta,$startChar,$maxChar);
                            $tmpString;
                        }
                        //pindahkan ke baris berikutnya
                        $startChar=$startChar+$maxChar;
                        //kemudian tambahkan ke dalam array sehingga kita tahu berapa banyak baris yang dibutuhkan
                        array_push($textArray,$tmpString);
                        //reset variabel penampung
                        $maxChar=0;
                        $tmpString='';
                    }
                    //dapatkan jumlah baris
                    $line_alamat=count($textArray);
                }else{
                    //jika tidak, maka tidak melakukan apa-apa
                    $line_alamat=1;
                }

                $cellWidth_nama_kota=35; //lebar sel nama kota
                $line_nama_kota = 0;
                //periksa apakah teksnya melibihi kolom?
                if($pdf->GetStringWidth($string_nama_kota) > $cellWidth_nama_kota){
                    //jika ya, maka hitung ketinggian yang dibutuhkan untuk sel akan dirapikan
                    //dengan memisahkan teks agar sesuai dengan lebar sel
                    //lalu hitung berapa banyak baris yang dibutuhkan agar teks pas dengan sel
                    
                    $textLength=strlen($string_nama_kota);	//total panjang teks
                    $errMargin=5;		//margin kesalahan lebar sel, untuk jaga-jaga
                    $startChar=0;		//posisi awal karakter untuk setiap baris
                    $maxChar=0;			//karakter maksimum dalam satu baris, yang akan ditambahkan nanti
                    $textArray=array();	//untuk menampung data untuk setiap baris
                    $tmpString="";		//untuk menampung teks untuk setiap baris (sementara)
                    
                    while($startChar < $textLength){ //perulangan sampai akhir teks
                        //perulangan sampai karakter maksimum tercapai
                        while( 
                        $pdf->GetStringWidth( $tmpString ) < ($cellWidth_nama_kota-$errMargin) && ($startChar+$maxChar) < $textLength ) {
                            $maxChar++;
                            $tmpString=substr($string_nama_kota,$startChar,$maxChar);
                            $tmpString;
                        }
                        //pindahkan ke baris berikutnya
                        $startChar=$startChar+$maxChar;
                        //kemudian tambahkan ke dalam array sehingga kita tahu berapa banyak baris yang dibutuhkan
                        array_push($textArray,$tmpString);
                        //reset variabel penampung
                        $maxChar=0;
                        $tmpString='';
                    }
                    //dapatkan jumlah baris
                    $line_nama_kota=count($textArray);
                }else{
                    //jika tidak, maka tidak melakukan apa-apa
                    $line_nama_kota=1;
                }
                
                $max_line = $line_alamat;
                if ($line_nama > $max_line) {
                    $max_line = $line_nama;
                }
                if ($line_jenis_pekerjaan > $max_line) {
                    $max_line = $line_jenis_pekerjaan;
                }
                if ($line_instansi > $max_line) {
                    $max_line = $line_instansi;
                }
                if ($line_nama_kota > $max_line) {
                    $max_line = $line_nama_kota;
                }
                
                //tulis selnya
                $pdf->SetFillColor(255,255,255);

                //memanfaatkan MultiCell sebagai ganti Cell
                //atur posisi xy untuk sel berikutnya menjadi di sebelahnya.
                //ingat posisi x dan y sebelum menulis MultiCell
                $xPos=$pdf->GetX();
                $yPos=$pdf->GetY();
                // $pdf->Rect($xPos, $yPos, 30 + $cellWidth_nama + 8 + 20 + 8 + $cellWidth_instansi + $cellWidth_jenis_pekerjaan + 23 + $cellWidth_alamat + $cellWidth_nama_kota, $max_line * $cellHeight);

                $pdf->Cell(30,($max_line * $cellHeight),$dpp['nik_peserta'],1,0,'C',true); //sesuaikan ketinggian dengan jumlah garis
                
                $pdf->SetLineWidth(0.3);
                $pdf->Rect($xPos, $yPos, 30 + $cellWidth_nama, $max_line * $cellHeight);
                $pdf->MultiCell($cellWidth_nama, $cellHeight,$nama_peserta,0, "C");

                //kembalikan posisi untuk sel berikutnya di samping MultiCell 
                //dan offset x dengan lebar MultiCell
                $pdf->SetXY($xPos + 30 + $cellWidth_nama , $yPos);
                $pdf->Cell(8,($max_line * $cellHeight),$jenis_kelamin,1,0,'C');
                $pdf->Cell(20,($max_line * $cellHeight),date('d M Y', strtotime($dpp['tanggal_lahir_peserta'])),1,0,'C');
                $pdf->Cell(8,($max_line * $cellHeight),$umur,1,0, 'C');

                $pdf->Rect($xPos, $yPos, 30 + $cellWidth_nama + 8 + 20 + 8 + $cellWidth_instansi, $max_line * $cellHeight);
                $pdf->MultiCell($cellWidth_instansi,$cellHeight,$string_instansi_pekerjaan,"R", "C");
                
                $pdf->SetXY($xPos + 30 + $cellWidth_nama + 8 + 20 + 8 + $cellWidth_instansi, $yPos);
                $pdf->Rect($xPos, $yPos, 30 + $cellWidth_nama + 8 + 20 + 8 + $cellWidth_instansi + $cellWidth_jenis_pekerjaan, $max_line * $cellHeight);
                $pdf->MultiCell($cellWidth_jenis_pekerjaan,$cellHeight,$string_jenis_pekerjaan,0, "C");

                $pdf->SetXY($xPos + 30 + $cellWidth_nama + 8 + 20 + 8 + $cellWidth_instansi + $cellWidth_jenis_pekerjaan, $yPos);
                $pdf->Cell(23, ($max_line * $cellHeight) ,$dpp['phone_peserta'],1,0,'C');

                $pdf->Rect($xPos, $yPos, 30 + $cellWidth_nama + 8 + 20 + 8 + $cellWidth_instansi + $cellWidth_jenis_pekerjaan + 23 + $cellWidth_alamat, $max_line * $cellHeight);
                $pdf->MultiCell($cellWidth_alamat,$cellHeight,$string_alamat_peserta,"R", "C");
                
                $pdf->SetXY($xPos + 30 + $cellWidth_nama + 8 + 20 + 8 + $cellWidth_instansi + $cellWidth_jenis_pekerjaan + 23 + $cellWidth_alamat, $yPos);
                $pdf->Rect($xPos, $yPos, 30 + $cellWidth_nama + 8 + 20 + 8 + $cellWidth_instansi + $cellWidth_jenis_pekerjaan + 23 + $cellWidth_alamat + $cellWidth_nama_kota, $max_line * $cellHeight);
                $pdf->MultiCell($cellWidth_nama_kota,$cellHeight,$string_nama_kota,0, "C");

                $pdf->SetXY($xPos + 30 + $cellWidth_nama + 8 + 20 + 8 + $cellWidth_instansi + $cellWidth_jenis_pekerjaan + 23 + $cellWidth_alamat, $yPos);
                $pdf->Cell(0,($max_line * $cellHeight),"",0,1,'C');

            }
        }

        $pdf->Output();
    }
}
