<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrasi extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->Model('M_get');
    }
    
    public function form($nama_program, $id_tanggal_mulai_program){
        $replace_url_nama = str_replace("-", " ", $nama_program);
        $panjang_angka = strlen($id_tanggal_mulai_program);
        $id_program = substr($id_tanggal_mulai_program,0,($panjang_angka-8));
        $tanggal_program = substr($id_tanggal_mulai_program,($panjang_angka-8),$panjang_angka);
        
        $x['title'] = $replace_url_nama." - CMS Klinis";
        $x['page_title'] = "Program";
        $this->db->ORDER_BY('tanggal_sesi', "ASC");
        $this->db->ORDER_BY('waktu_mulai_sesi', "ASC");
        
        $x['data_sesi'] = $this->db->get_where('table_sesi_program', ['id_program'=>$id_program])->result_array();
        $cek_id_program = $this->db->get_where('table_program', ['id_program'=>$id_program]);
        if($cek_id_program->num_rows() > 0){
            $x['data_registrasi'] = $this->M_get->getDataRegistrasi($id_program);
            $this->load->view('klinis/pages/registrasi', $x);
        }else{
            $this->load->view('klinis/pages/registrasi_error', $x);
        }
        
    }

	private function hp($nohp) {
		// kadang ada penulisan no hp 0811 239 345
		$nohp = str_replace(" ","",$nohp);
		// kadang ada penulisan no hp (0274) 778787
		$nohp = str_replace("(","",$nohp);
		// kadang ada penulisan no hp (0274) 778787
		$nohp = str_replace(")","",$nohp);
		// kadang ada penulisan no hp 0811.239.345
		$nohp = str_replace(".","",$nohp);
	
		// cek apakah no hp mengandung karakter + dan 0-9
		if(!preg_match('/[^+0-9]/',trim($nohp))){
			// cek apakah no hp karakter 1-3 adalah +62
			if(substr(trim($nohp), 0, 3)=='+62'){
				$hp = trim($nohp);
			}
			// cek apakah no hp karakter 1 adalah 0
			elseif(substr(trim($nohp), 0, 1)=='0'){
				$hp = '62'.substr(trim($nohp), 1);
			}
		}
		return $hp;
	}

    public function registrasiPeserta(){
		
		// return false;
        $url_pendaftaran_peserta = $this->input->post('url_pendaftaran_peserta');
        // hitung panjang angka url pendaftaran
        $panjang_angka = strlen($url_pendaftaran_peserta);

        // ambil tanggal dan hitung panjang tanggal
        $tanggal_program = substr($url_pendaftaran_peserta,($panjang_angka-8),$panjang_angka);
        $panjang_tanggal = strlen($tanggal_program);

        // Cek tanda miring ada di array ke berapa
        $tandamiring = strpos($url_pendaftaran_peserta, '/');
        
        // ambil angka dari tanda / hingga habis
        $id_tanggal = substr($url_pendaftaran_peserta,$tandamiring+1,$panjang_angka);
        $panjang_id = strlen($id_tanggal);
 		$id_program = substr($id_tanggal,0,($panjang_id-$panjang_tanggal));

        $nik_peserta = $this->input->post('nik_peserta');
        $group_antrian = $this->input->post('group_antrian');
        $sesi_program = $this->input->post('sesi_program_input_text');
        // $kategori_program = $this->input->post('kategori_program_input_text');
        $nama_depan_peserta = $this->input->post('nama_depan_peserta');
        $nama_belakang_peserta = $this->input->post('nama_belakang_peserta');
        $email_peserta = $this->input->post('email_peserta');
        $phone_peserta = $this->input->post('phone_peserta');
        $tanggal_lahir_peserta = $this->input->post('tanggal_lahir_peserta');
        $jenis_kelamin_peserta = $this->input->post('jenis_kelamin_peserta');
        $kota_peserta = $this->input->post('kota_peserta');
        $alamat_peserta = $this->input->post('alamat_peserta');
        $pekerjaan_peserta = $this->input->post('pekerjaan_peserta');
        $jenis_pekerjaan_peserta = $this->input->post('jenis_pekerjaan_peserta');
		$instansi_pekerjaan_peserta = $this->input->post('instansi_pekerjaan_peserta');
        // $tanggal_vaksin2 = $this->input->post('tanggal_vaksin2');

		$check_nik = $this->db->get_where('table_peserta', ['nik_peserta'=>$nik_peserta])->num_rows();
		$kategori_all = $this->db->get_where('table_kategori_program')->result_array();
		$id = array_search($pekerjaan_peserta, array_column($kategori_all, 'kode_kategori'));

        // Update kuota sesi
        $sesi = $this->db->get_where('table_sesi_program', ['id_sesi'=>$sesi_program])->row_array();

		// return var_dump($sesi);
        $kuota = $sesi['kuota_terdaftar'];


        $url_bukti_pendaftaran = $id_program."/".$nik_peserta;

        $data_peserta = [
			"id_program" => $id_program,
			"id_sesi" => $sesi_program,
			"no_antrian" => 0,
            "nik_peserta" => $nik_peserta,
            "nama_peserta" => $nama_depan_peserta." ".$nama_belakang_peserta,
            "email_peserta" => $email_peserta,
            "phone_peserta" => $phone_peserta,
            "tanggal_lahir_peserta" => $tanggal_lahir_peserta,
            "jenis_kelamin_peserta" => $jenis_kelamin_peserta,
            "kota_peserta" => $kota_peserta,
            "alamat_peserta" => $alamat_peserta,
            "pekerjaan_peserta" => $pekerjaan_peserta, 
			"jenis_pekerjaan_peserta" => $jenis_pekerjaan_peserta,
			"instansi_pekerjaan" => $instansi_pekerjaan_peserta,
            "status_peserta" => "active",
			"program_tanggal" => $sesi["tanggal_sesi"],
			"progam_jenis_vaksin" => $sesi['jenis_vaksin']
        ];
		$progam = $this->db->get_where('table_program', ['id_program'=> $id_program])->row_object();

        // Cek apakah peserta sudah terdaftar atau belum
        if($check_nik == 0){
			
            // Jika peserta belum terdaftar
            $insert_peserta = $this->db->INSERT('table_peserta', $data_peserta);

			$this->load->library('ciqrcode'); //pemanggilan library QR CODE
 
			$config['cacheable']    = true; //boolean, the default is true
			// $config['cachedir']     = './assets/qrcodesregister'; //string, the default is application/cache/
			// $config['errorlog']     = './assets/qrcodesregister'; //string, the default is application/logs/
			$config['imagedir']     = './assets/qrcodesregister/'; //direktori penyimpanan qr code
			$config['quality']      = true; //boolean, the default is true
			$config['size']         = '1024'; //interger, the default is 1024
			$config['black']        = array(224,255,255); // array, default is array(255,255,255)
			$config['white']        = array(70,130,180); // array, default is array(0,0,0)
			$this->ciqrcode->initialize($config);

			$qrcodesname=$nik_peserta.'.png';

			$params['data'] = base_url('Registrasi/BuktiPendaftaran/').$id_program.'/'.$nik_peserta; //data yang akan di jadikan QR CODE
			$params['level'] = 'H'; //H=High
			$params['size'] = 10;
			$params['savename'] = FCPATH.$config['imagedir'].$qrcodesname; //simpan image QR CODE ke folder assets/images/
			$this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
	

			$curl = curl_init();

			curl_setopt_array($curl, array(
				CURLOPT_URL => 'https://klinis-adonis.herokuapp.com/v1/notification/whatsapp3',
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'POST',
				CURLOPT_POSTFIELDS =>'{
					"receiver": '. $this->hp($phone_peserta) .',
					"image": "https://vaksin.klinis.id/assets/qrcodesregister/'. $nik_peserta .'.png",
					"body": "Pendaftaran vaksinasi di *'. $progam->nama_program .'* berhasil dengan data sebagai berikut :\n\n*Nama* : '.$nama_depan_peserta.'\n*Tanggal Lahir* : '. $tanggal_lahir_peserta .'\n*Jenis Kelamin* : '. $jenis_kelamin_peserta .'\n*Tanggal Vaksinasi* : '. $sesi["tanggal_sesi"] .'\n*Jam* : '. $sesi["waktu_mulai_sesi"] .' - '. $sesi["waktu_berakhir_sesi"] .'\n\nSilahkan simpan url sebagai bukti : '. base_url("Registrasi/BuktiPendaftaran/" . $url_bukti_pendaftaran) .'.\nJika ada pertanyaan silahkan menghubungi  penyelenggara di wa.me/6212345678\n"
				}',
				CURLOPT_HTTPHEADER => array(
					'Content-Type: application/json',
				)
			));

			$response = curl_exec($curl);

			curl_close($curl);
			// return var_dump($response);

            // Insert data peserta ke database
            if($insert_peserta){
                // Query get last id_peserta from table_peserta
                $query = "SELECT id_peserta FROM table_peserta ORDER BY id_peserta DESC LIMIT 1";
                $result_id_peserta = $this->db->query($query)->row_array();
                $id_peserta = $result_id_peserta['id_peserta'];
            }else{
                $id_peserta = "0000000000";
            }

            $data_koneksi = [
                "id_peserta" => $id_peserta,
                "id_sesi" => $sesi_program,
                "kategori_program" => $kategori_all[$id]['nama_kategori'],
                "status_koneksi" => "Waiting",
            ];
            // Hubungkan peserta dengan sesi program melalui table koneksi
            $this->db->INSERT('table_koneksi_program', $data_koneksi);

            // Update sisa kuota yang tersedia pada sesi yang di daftarkan peserta
            $this->db->SET('kuota_terdaftar', $kuota+1);
            $this->db->WHERE('id_sesi', $sesi_program);
            $this->db->UPDATE('table_sesi_program');

            // $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Pendaftaran Berhasil</div>');
            redirect('Registrasi/BuktiPendaftaran/'.$url_bukti_pendaftaran);
            // $this->BuktiPendaftaran();


        }else{
            // ambil data peserta yang telah terdaftar
            $getPeserta = $this->db->get_where('table_peserta', ['nik_peserta'=>$nik_peserta])->row_array();
        
            $id_peserta = $getPeserta['id_peserta'];
            // cek status peserta yang telah terdaftar apakah masih memungkinkan untuk di daftarkan atau tidak.
            // Jika status != Rejected & Reschedule maka peserta tidak bisa mendaftar lagi
            $check_koneksi = $this->db->query("
                SELECT * FROM table_koneksi_program WHERE id_peserta=$id_peserta AND status_koneksi!='Rejected' AND status_koneksi!='Reschedule'
                ")->row_array();
            
            // Update data peserta
            $this->db->WHERE('id_peserta', $id_peserta);
            $this->db->UPDATE('table_peserta', $data_peserta);

            // Kalau peserta sudah terdaftar dengan status Waiting atau Confirmed
            // Maka peserta tidak bisa mendaftar lagi
            if($check_koneksi){
                // $this->session->set_flashdata('message', '<div class="alert alert-info" role="alert">Anda sudah terdaftar pada program ini !, sistem hanya akan menyimpan perubahan data diri anda (Jika ada).</div>');
                // $this->session->set_flashdata('message', '<div class="alert alert-info" role="alert">Failed</div>');
                redirect('Registrasi/BuktiPendaftaran/'.$url_bukti_pendaftaran);
                // redirect('Registrasi/Form/'.$url_pendaftaran_peserta);
            }else{
                $data_koneksi = [
                    "id_peserta" => $id_peserta,
                    "id_sesi" => $sesi_program,
                    "kategori_program" => $kategori_all[$id]['nama_kategori'],
                    "status_koneksi" => "Waiting",
                ];

                $this->db->INSERT('table_koneksi_program', $data_koneksi);

                $this->db->SET('kuota_terdaftar', $kuota+1);
                $this->db->WHERE('id_sesi', $sesi_program);
                $this->db->UPDATE('table_sesi_program');

                // $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Updated</div>');
                redirect('Registrasi/BuktiPendaftaran/'.$url_bukti_pendaftaran);
                // redirect('Registrasi/Form/'.$url_pendaftaran_peserta);
            }
        }
    }
    
    
    public function BuktiPendaftaran($id_program,$nik_peserta_bukti_save){

        // $this->load->library('ciqrcode'); //pemanggilan library QR CODE
 
        // $config['cacheable']    = true; //boolean, the default is true
        // // $config['cachedir']     = './assets/qrcodesregister'; //string, the default is application/cache/
        // // $config['errorlog']     = './assets/qrcodesregister'; //string, the default is application/logs/
        // $config['imagedir']     = './assets/qrcodesregister/'; //direktori penyimpanan qr code
        // $config['quality']      = true; //boolean, the default is true
        // $config['size']         = '1024'; //interger, the default is 1024
        // $config['black']        = array(224,255,255); // array, default is array(255,255,255)
        // $config['white']        = array(70,130,180); // array, default is array(0,0,0)
        // $this->ciqrcode->initialize($config);
 
        $qrcodesname=$nik_peserta_bukti_save.'.png'; //buat name dari qr code sesuai dengan nim
 
        // $params['data'] = base_url('Registrasi/BuktiPendaftaran/').$id_program.'/'.$nik_peserta_bukti_save; //data yang akan di jadikan QR CODE
        // $params['level'] = 'H'; //H=High
        // $params['size'] = 10;
        // $params['savename'] = FCPATH.$config['imagedir'].$qrcodesname; //simpan image QR CODE ke folder assets/images/
        // $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE


        $query_data_program = "SELECT * FROM table_program WHERE id_program = '$id_program'";
        $data_program = $this->db->query($query_data_program)->row_array();

        $query_data_peserta = "
            SELECT a.*, b.*, c.*, d.*,e.* FROM table_peserta a
            LEFT JOIN table_koneksi_program b ON a.id_peserta = b.id_peserta
            LEFT JOIN table_sesi_program c ON b.id_sesi = c.id_sesi
            LEFT JOIN table_kategori_program d ON d.kode_kategori = a.pekerjaan_peserta
            LEFT JOIN table_kota_kabupaten e ON a.kota_peserta = e.kode_kota_kabupaten
            WHERE a.nik_peserta = '$nik_peserta_bukti_save'
        ";
        $data_peserta = $this->db->query($query_data_peserta)->row_array();
        
		$replace_nama_peserta = str_replace("-", " ", $data_peserta['nama_peserta']);
        
        $query_data_faskes = "
            SELECT a.*, b.*, c.* FROM table_program a
            LEFT JOIN table_user b ON a.id_user = b.id_user
            LEFT JOIN table_faskes c ON b.id_faskes = c.id_faskes
            WHERE a.id_program = '$id_program'
        ";
        $data_faskes = $this->db->query($query_data_faskes)->row_array();

        $id_sesi = $data_peserta['id_sesi'];
        $data_sesi =$this->db->query("SELECT * FROM table_sesi_program WHERE id_sesi='$id_sesi'")->row_array();

        $tanggal_hari_ini = date("d F Y");

        $pdf = new FPDF('P','mm','A4');
        // membuat halaman baru
        $title = 'Bukti Pendaftaran Program '.$data_program['nama_program'];
        $pdf->SetTitle($title);
        $pdf->AddPage('P');
        // $pdf->Rect(5,5,200,200);
        $pdf->SetFont('Arial','B',15);

        $image1 = "assets/images/logo.png";
        $logo_mitra = "assets/logo_mitra/".$data_faskes['logo_mitra'];
        $titik_logo_klikis_x = 50;
        $titik_logo_mitra_x = 110;
        $titik_logo_y = 20;
        $size_logo = 50;
        
        $titik_qr_code_x = 80;
        $titik_qr_code_y = 60;

        // Watermark
        $pdf->Cell(190, 20, $pdf->Image("assets/images/Watermark.png", 0, 0, 230), 0, 0, 'C');
        
        $pdf->ln(3);
        
        $pdf->SetFont('Arial','',9);
        $pdf->Cell(190, 5,"Dicetak tanggal : ".$tanggal_hari_ini,0,1,'R');
        $pdf->ln(8);

        $pdf->Cell(190, 20, $pdf->Image($image1, $titik_logo_klikis_x, $titik_logo_y, $size_logo), 0, 0, 'C');
        $pdf->Cell(190, 20, $pdf->Image($logo_mitra, $titik_logo_mitra_x, $titik_logo_y, $size_logo), 0, 0, 'C');
        
        $pdf->Cell(190, 20, "" ,0, 1, 'C');

        $pdf->SetFont('Arial','B',15);
        $pdf->Cell(190, 8,"BUKTI PENDAFTARAN PESERTA PROGRAM",0,1,'C');
        $pdf->Cell(190, 8,"'".$data_program['nama_program']."'",0,1,'C');

        $pdf->Cell(190, 20, $pdf->Image('assets/qrcodesregister/'.$qrcodesname, $titik_qr_code_x, $titik_qr_code_y, $size_logo), 0, 0, 'C');
                
        $pdf->ln(50);
        $pdf->SetFont('Arial','',10);

        $pdf->Cell(55, 6,"",0,0,'C');
        $pdf->Cell(40, 6,"NIK" ,0,0,'L');
        $pdf->Cell(80, 6,$data_peserta["nik_peserta"] ,0,1,'L');

        $pdf->Cell(55, 6,"",0,0,'C');
        $pdf->Cell(40, 6,"Nama" ,0,0,'L');
        $pdf->MultiCell(90, 6,$replace_nama_peserta ,0, "L");
        
        $pdf->Cell(55, 6,"",0,0,'C');
        $pdf->Cell(40, 6,"Tanggal Lahir" ,0,0,'L');
        $pdf->Cell(80, 6,date('d-F-Y', strtotime($data_peserta['tanggal_lahir_peserta'])) ,0,1,'L');

        $pdf->Cell(55, 6,"",0,0,'C');
        $pdf->Cell(40, 6,"Nomor HP" ,0,0,'L');
        $pdf->Cell(80, 6,$data_peserta['phone_peserta'] ,0,1,'L');

        $pdf->Cell(55, 6,"",0,0,'C');
        $pdf->Cell(40, 6,"Email" ,0,0,'L');
        $pdf->Cell(80, 6,$data_peserta['email_peserta'] ,0,1,'L');

        $pdf->Cell(55, 6,"",0,0,'C');
        $pdf->Cell(40, 6,"Alamat" ,0,0,'L');
        $pdf->MultiCell(90, 6,$data_peserta['alamat_peserta'] ,0);

        $pdf->ln(5);
        $pdf->Cell(55, 6,"",0,0,'C');
        $pdf->Cell(40, 6,"Tanggal Program" ,0,0,'L');
        $pdf->Cell(80, 6,date('d-F-Y', strtotime($data_sesi['tanggal_sesi'])),0,1,'L');
        
        $pdf->Cell(55, 6,"",0,0,'C');
        $pdf->Cell(40, 6,"Waktu Program" ,0,0,'L');
        $pdf->Cell(80, 6,date('H:i', strtotime($data_sesi['waktu_mulai_sesi']))." - ".date('H:i', strtotime($data_sesi['waktu_berakhir_sesi'])),0,1,'L');

        $pdf->Cell(55, 6,"",0,0,'C');
        $pdf->Cell(40, 6,"Lokasi Program" ,0,0,'L');
        $pdf->MultiCell(90, 6,$data_program['lokasi_program'].', '.$data_program['kecamatan_program'] ,0);
        
        $pdf->ln(10);
        $pdf->Cell(190, 5,"* Bukti ini harap disimpan dan dibawa pada saat mengikuti program.",0,1,'L');
        $pdf->Cell(190, 5,"* Dimohon untuk tiba 15 menit sebelum waktu yang dipilih.",0,1,'L');
        $pdf->Cell(190, 5,"* Diwajibkan membawa KTP / KK.",0,1,'L');
        $pdf->Cell(190, 5,"* Peserta boleh didampingi maksimal hanya 1 (satu) pendamping.",0,1,'L');
        $pdf->Cell(190, 5,"* Setiap peserta dan pendamping WAJIB untuk mengikuti protokol kesehatan.",0,1,'L');

        $pdf->Output();
    }
}
