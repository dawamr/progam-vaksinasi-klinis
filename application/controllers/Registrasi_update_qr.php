<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrasi extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->Model('M_get');
    }
    
    public function form($nama_program, $id_tanggal_mulai_program){
        $replace_url_nama = str_replace("-", " ", $nama_program);
        $panjang_angka = strlen($id_tanggal_mulai_program);
        $id_program = substr($id_tanggal_mulai_program,0,($panjang_angka-8));
        $tanggal_program = substr($id_tanggal_mulai_program,($panjang_angka-8),$panjang_angka);
        
        $x['title'] = $replace_url_nama." - CMS Klinis";
        $x['page_title'] = "Program";
        $this->db->ORDER_BY('tanggal_sesi', "ASC");
        $this->db->ORDER_BY('waktu_mulai_sesi', "ASC");
        
        $x['data_sesi'] = $this->db->get_where('table_sesi_program', ['id_program'=>$id_program])->result_array();
        $cek_id_program = $this->db->get_where('table_program', ['id_program'=>$id_program]);
        if($cek_id_program->num_rows() > 0){
            $x['data_registrasi'] = $this->M_get->getDataRegistrasi($id_program);
            $this->load->view('klinis/pages/registrasi', $x);
        }else{
            $this->load->view('klinis/pages/registrasi_error', $x);
        }
        
    }

    public function registrasiPeserta(){
        $url_pendaftaran_peserta = $this->input->post('url_pendaftaran_peserta');
        $nik_peserta = $this->input->post('nik_peserta');
        $sesi_program = $this->input->post('sesi_program_input_text');
        $kategori_program = $this->input->post('kategori_program_input_text');
        $nama_depan_peserta = $this->input->post('nama_depan_peserta');
        $nama_belakang_peserta = $this->input->post('nama_belakang_peserta');
        $email_peserta = $this->input->post('email_peserta');
        $phone_peserta = $this->input->post('phone_peserta');
        $tanggal_lahir_peserta = $this->input->post('tanggal_lahir_peserta');
        $jenis_kelamin_peserta = $this->input->post('jenis_kelamin_peserta');
        $kota_peserta = $this->input->post('kota_peserta');
        $alamat_peserta = $this->input->post('alamat_peserta');
        $pekerjaan_peserta = $this->input->post('pekerjaan_peserta');
        $jenis_pekerjaan_peserta = $this->input->post('jenis_pekerjaan_peserta');
        
        $data_peserta = [
            "nik_peserta" => $nik_peserta,
            "nama_peserta" => $nama_depan_peserta."-".$nama_belakang_peserta,
            "email_peserta" => $email_peserta,
            "phone_peserta" => $phone_peserta,
            "tanggal_lahir_peserta" => $tanggal_lahir_peserta,
            "jenis_kelamin_peserta" => $jenis_kelamin_peserta,
            "kota_peserta" => $kota_peserta,
            "alamat_peserta" => $alamat_peserta,
            "pekerjaan_peserta" => $pekerjaan_peserta,
            "jenis_pekerjaan_peserta" => $jenis_pekerjaan_peserta,
            "status_peserta" => "active",
        ];

        $check_nik = $this->db->get_where('table_peserta', ['nik_peserta'=>$nik_peserta])->num_rows();

        // Update kuota sesi
        $sesi = $this->db->get_where('table_sesi_program', ['id_sesi'=>$sesi_program])->row_array();
        $kuota = $sesi['kuota_terdaftar'];

        if($check_nik == 0){
            $insert_peserta = $this->db->INSERT('table_peserta', $data_peserta);
            if($insert_peserta){
                // Query get last id_peserta from table_peserta
                $query = "SELECT id_peserta FROM table_peserta ORDER BY id_peserta DESC LIMIT 1";
                $result_id_peserta = $this->db->query($query)->row_array();
                $id_peserta = $result_id_peserta['id_peserta'];
            }else{
                $id_peserta = "0000000000";
            }

            $data_koneksi = [
                "id_peserta" => $id_peserta,
                "id_sesi" => $sesi_program,
                "kategori_program" => $kategori_program,
                "status_koneksi" => "Waiting",
            ];

            $this->db->INSERT('table_koneksi_program', $data_koneksi);

            $this->db->SET('kuota_terdaftar', $kuota+1);
            $this->db->WHERE('id_sesi', $sesi_program);
            $this->db->UPDATE('table_sesi_program');

            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Registrasi berhasil !</div>');
            redirect('Registrasi/Form/'.$url_pendaftaran_peserta);
        }else{
            $getPeserta = $this->db->get_where('table_peserta', ['nik_peserta'=>$nik_peserta])->row_array();
        
            $id_peserta = $getPeserta['id_peserta'];
            $check_koneksi = $this->db->query("
                SELECT * FROM table_koneksi_program WHERE id_peserta=$id_peserta AND status_koneksi!='Rejected' AND status_koneksi!='Reschedule'
                ")->row_array();

            $this->db->WHERE('id_peserta', $id_peserta);
            $this->db->UPDATE('table_peserta', $data_peserta);

            // Kalau peserta sudah terdaftar dengan status Waiting atau Confirmed
            // Maka peserta tidak bisa mendaftar lagi
            if($check_koneksi){
                $this->session->set_flashdata('message', '<div class="alert alert-info" role="alert">Anda sudah terdaftar pada program ini !</div>');
                redirect('Registrasi/Form/'.$url_pendaftaran_peserta);
            }else{
                $data_koneksi = [
                    "id_peserta" => $id_peserta,
                    "id_sesi" => $sesi_program,
                    "kategori_program" => $kategori_program,
                    "status_koneksi" => "Waiting",
                ];

                $this->db->INSERT('table_koneksi_program', $data_koneksi);

                $this->db->SET('kuota_terdaftar', $kuota+1);
                $this->db->WHERE('id_sesi', $sesi_program);
                $this->db->UPDATE('table_sesi_program');

                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Registrasi berhasil !</div>');
                redirect('Registrasi/Form/'.$url_pendaftaran_peserta);
            }
        }
    }
}