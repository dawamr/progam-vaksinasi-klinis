<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class InvoicePDF extends CI_Controller {
    public function UiInvoice(){
        $this->load->view('ui_email');
    }
    
    public function eInvoice(){
        $tanggal_hari_ini = date("d F Y");

        $pdf = new FPDF('P','mm','A4');

        // membuat halaman baru
        $title = 'E-Invoice KLINIS Medicare';
        $pdf->SetTitle($title );

        $pdf->AddPage('P');

        // $pdf->SetAutoPageBreak(false);
        // Jarak garis kiri, jarak atas, jarak kiri ke kanan, jarak atas ke bawah


        $pdf->SetFont('Arial','B',15);
        $pdf->Cell(190, 20, $pdf->Image("assets/images/Watermark.png", 0, 0, 230), 0, 0, 'C'); // Watermark

        $pdf->ln(10);
        $pdf->Cell(190, 8,"INVOICE",0,1,'L');


        // Baris Invoice 1
        $pdf->SetFont('Arial','',9);
        $pdf->SetTextColor(110, 110, 110); // warna grey
        $pdf->Cell(35, 5,"No. Invoice",0,0,'L');
        $pdf->SetTextColor(71, 211, 205); // warna grey
        $pdf->Cell(100, 5,": *NOMOR INVOICE*",0,0,'L');

        $pdf->SetFont('Arial','B',10);
        $pdf->SetTextColor(0, 0, 0); // warna hitam
        $pdf->Cell(55, 5,"PT. KREASI LAYANAN MEDIS",0,1,'L');


        // Baris Invoice 2
        $pdf->SetFont('Arial','',9);
        $pdf->SetTextColor(110, 110, 110); // warna grey
        $pdf->Cell(35, 5,"Tanggal",0,0,'L');
        $pdf->SetTextColor(0, 0, 0); // warna hitam
        $pdf->Cell(100, 5,": *TANGGAL INVOICE*",0,0,'L');

        $pdf->SetFont('Arial','',8);
        $pdf->SetTextColor(110, 110, 110); // warna grey
        $pdf->Cell(55, 5,"Gedung CNI Creative Center (C3), Lt.2.",0,1,'L');

        // Baris Invoice 3
        $pdf->SetFont('Arial','',9);
        $pdf->SetTextColor(110, 110, 110); // warna grey
        $pdf->Cell(35, 5,"Pembayaran",0,0,'L');
        $pdf->SetTextColor(0, 0, 0); // warna hitam
        $pdf->Cell(100, 5,": *PEMBAYARAN INVOICE*",0,0,'L');

        $pdf->SetFont('Arial','',9);
        $pdf->SetTextColor(110, 110, 110); // warna grey
        $pdf->Cell(55, 5,"Jalan Puri Indah Blok O2 No.1-3",0,1,'L');

        // Baris Invoice 4
        $pdf->SetFont('Arial','',9);
        $pdf->SetTextColor(110, 110, 110); // warna grey
        $pdf->Cell(35, 5,"Status",0,0,'L');
        $pdf->SetTextColor(0, 0, 0); // warna hitam
        $pdf->Cell(100, 5,": *STATUS INVOICE*",0,0,'L');

        $pdf->SetFont('Arial','',9);
        $pdf->SetTextColor(110, 110, 110); // warna grey
        $pdf->Cell(55, 5,"Kembangan, Jakarta Barat 11610.",0,1,'L');

        // Baris Invoice 5
        $pdf->Cell(135, 5,"",0,0,'L');
        $pdf->SetFont('Arial','',10);
        $pdf->SetTextColor(110, 110, 110); // warna grey
        $pdf->Cell(55, 5,"*NPWP PERUSAHAAN*",0,1,'L');

        $pdf->ln(8);

        // Baris Diterbitkan 1
        $pdf->SetFont('Arial','',9);
        $pdf->SetTextColor(110, 110, 110); // warna grey
        $pdf->Cell(135, 5,"Diterbitkan Kepada",0,1,'L');

        
        // Baris Diterbitkan 2
        $pdf->SetFont('Arial','',9);
        $pdf->SetTextColor(0, 0, 0); // warna hitam
        $pdf->Cell(3, 5,"",0,0,'L');
        $pdf->Cell(132, 5,"*NAMA COSTUMER*",0,1,'L');
        
        // Baris Diterbitkan 3
        $pdf->SetFont('Arial','',9);
        $pdf->SetTextColor(0, 0, 0); // warna hitam
        $pdf->Cell(3, 5,"",0,0,'L');
        $pdf->Cell(132, 5,"*NOMOR HP COSTUMER*",0,1,'L');
        
        // Baris Diterbitkan 4
        $pdf->SetFont('Arial','',9);
        $pdf->SetTextColor(0, 0, 0); // warna hitam
        $pdf->Cell(3, 5,"",0,0,'L');
        $pdf->Cell(132, 5,"*EMAIL COSTUMER*",0,1,'L');

        $pdf->ln(5);
        
        // Table Header Product

        $pdf->Cell(3, 10,"",0,0,'L'); // spasi jarak luar table ke garis luar

        $pdf->SetFillColor(225, 225, 225); // warna grey
        $pdf->SetTextColor(100, 100, 100); // warna grey

        $pdf->Cell(5, 10,"",0,0,'L', true); // spasi jarak dalam table ke tulisan
        
        $pdf->Cell(85, 10,"Nama Produk",0,0,'L', true);
        $pdf->Cell(13, 10,"Qty",0,0,'L', true);
        
        $pdf->MultiCell(32, 5,"Harga Sebelum Pajak",0,'L', true);

        $xPos=$pdf->GetX();
        $yPos=$pdf->GetY();
        $pdf->SetXY($xPos + 138 , $yPos - 10);
        $pdf->Cell(20, 10,"Pajak 10%",0,0,'L', true);
        $pdf->Cell(24, 10,"Harga Nett",0,0,'R', true);
        $pdf->Cell(5, 10,"",0,1,'L', true);

        $content_rect_height = 0;
        $total_bayar = 0;
        $harga_nett = 25000;

        for($i = 0; $i < 3; $i++){
            $content_rect_height = 10 * $i;
            // Table Content Product
            $pdf->Cell(3, 10,"",0,0,'L'); // spasi jarak luar table ke garis luar

            $pdf->SetFillColor(255, 255, 255); // warna grey
            $pdf->SetTextColor(80, 80, 80); // warna grey

            $pdf->Cell(5, 10,"",0,0,'L', true); // spasi jarak dalam table ke tulisan
            
            $pdf->MultiCell(85, 10,"Produk aa ",0,'L', true);
            
            $xPos=$pdf->GetX();
            $yPos=$pdf->GetY();
            $pdf->SetXY($xPos + 93 , $yPos - 10);
            $pdf->Cell(13, 10,"1",0,0,'L', true);
            $pdf->Cell(32, 10,"Rp. 10.990",0,0,'L', true);
            $pdf->Cell(20, 10,"Rp. 1.010",0,0,'L', true);
            $pdf->Cell(24, 10, "Rp. ".number_format($harga_nett),0,0,'R', true);
            $pdf->Cell(5, 10,"",0,1,'L', true);
            $total_bayar += $harga_nett;
        }        

        // Table Footer Product
        $pdf->SetFont('Arial','',8);
        $pdf->Cell(7, 10,"",0,0,'L');
        $pdf->Cell(131, 10,'*'.$this->terbilang($total_bayar).'*',0,0,'L');
        $pdf->SetFont('Arial','',9);
        $pdf->SetTextColor(71, 211, 205); // warna grey
        $pdf->Cell(44, 10,"Total Bayar : Rp. ".number_format($total_bayar),0,1,'R');
        
        $pdf->SetDrawColor(200, 200, 200); // setting warna border

        $pdf->ln(5);
        $pdf->SetTextColor(80, 80, 80); // warna grey
        $pdf->Cell(3, 10,"",0,0,'L'); // spasi jarak dalam table ke tulisan
        $pdf->Cell(184, 10,"Bila memiliki pertanyaan silakan hubungi kami via email di support@klinis.com atau di nomor telepon (021) 1234567",1,1,'C');
        $pdf->SetFont('Arial','',7);
        $pdf->Cell(184, 10,"Dokumen ini dicetak secara digital pada DD/MM/YYYY HH:ii",0,1,'C');


        // Setting border
        $pdf->SetDrawColor(200, 200, 200);
        $pdf->Rect(8,10,194,140 + $content_rect_height); // garis border luar invoice

        $pdf->Rect(13,86,184,10); // garis border table header
        $pdf->Rect(13,96,184,10 + $content_rect_height); // garis border table content
        $pdf->Rect(13,106 + $content_rect_height,184,10); // garis border table footer

        // LOGO KLINIS
        $pdf->SetFont('Arial','B',15);
        $pdf->SetTextColor(0, 0, 0); // warna hitam
        $image1 = "assets/images/logo.png";
        $titik_logo_klikis_x = 145;
        $titik_logo_y = 15;
        $size_logo = 50;
        $pdf->Cell(190, 20, $pdf->Image($image1, $titik_logo_klikis_x, $titik_logo_y, $size_logo), 0, 1, 'C');

        $pdf->Output();
    }

    public function terbilang($nilai) {
        if($nilai<0) {
            $hasil = "minus ". trim($this->penyebut($nilai))." Rupiah";
        } else {
            $hasil = trim($this->penyebut($nilai))." Rupiah";
        }
        return $hasil;
    }
    
    public function penyebut($nilai) {
        $nilai = abs($nilai);
        $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
        $temp = "";
        if ($nilai < 12) {
            $temp = " ". $huruf[$nilai];
        } else if ($nilai <20) {
            $temp = $this->penyebut($nilai - 10). " Belas";
        } else if ($nilai < 100) {
            $temp = $this->penyebut($nilai/10)." Puluh". $this->penyebut($nilai % 10);
        } else if ($nilai < 200) {
            $temp = " Seratus" . $this->penyebut($nilai - 100);
        } else if ($nilai < 1000) {
            $temp = $this->penyebut($nilai/100) . " Ratus" . $this->penyebut($nilai % 100);
        } else if ($nilai < 2000) {
            $temp = " Seribu" . $this->penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
            $temp = $this->penyebut($nilai/1000) . " Ribu" . $this->penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
            $temp = $this->penyebut($nilai/1000000) . " Juta" . $this->penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
            $temp = $this->penyebut($nilai/1000000000) . " Milyar" . $this->penyebut(fmod($nilai,1000000000));
        } else if ($nilai < 1000000000000000) {
            $temp = $this->penyebut($nilai/1000000000000) . " Trilyun" . $this->penyebut(fmod($nilai,1000000000000));
        }     
        return $temp;
    }
    
    public function sendInvoice() {
		//pengaturan smtp gmail
        $config = array();
		$config['charset'] = 'utf-8';
		$config['useragent'] = 'Codeigniter';
		$config['protocol']= "smtp";
		$config['mailtype']= "html";
		$config['smtp_host']= "ssl://mail.klinis.id";
		$config['smtp_port']= "465";
		$config['smtp_timeout']= "400";
		$config['smtp_user']= "invoice@klinis.id"; 
		$config['smtp_pass']= "@Klm2020";
		$config['crlf']="\r\n";
		
		//pengaturan email
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from('invoice@klinis.id'); //email pengirim
		$this->email->to("thoberlinzhuo@gmail.com"); //email penerima
		$this->email->subject("Tes send mail codeiginiter"); // subject email
	    $this->email->message("Message Send Email"); // konten dalam email

        //Send mail
        if($this->email->send()){
            echo "Sukses";
        }else{
            echo "Failed";
        }
    }
}