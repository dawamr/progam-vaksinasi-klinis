<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('id_user')){
        	redirect("Dashboard");
        }
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){
		$x['title'] = "Login - CMS Klinis";

		$this->load->view('klinis/pages/login', $x);
	}

	public function daftarmitra(){
			$x['title'] = "Pendaftaran Mitra Klinis - CMS Klinis";
			$this->load->view('klinis/pages/daftarmitra', $x);
	}

	public function registerMitra(){
		$nama_mitra = $this->input->post('nama_mitra');
		$tipe_mitra = $this->input->post('tipe_mitra');
		$alamat_mitra = $this->input->post('alamat_mitra');
		$kota_kabupaten_mitra = $this->input->post('kota_kabupaten_mitra');
		$email_mitra = $this->input->post('email_mitra');
		$phone_mitra = $this->input->post('phone_mitra');
		$longtitude_mitra = $this->input->post('longtitude_mitra');
		$latitude_mitra = $this->input->post('latitude_mitra');
		$nama_user = $this->input->post('nama_user');
		$email_user = $this->input->post('email_user');
		$password_user = $this->input->post('password_user');
		$password_confirm_user = $this->input->post('password_confirm_user');

		if($password_user == $password_confirm_user){
			$password = password_hash($password_user, PASSWORD_DEFAULT);
		}else{
			$password = "1234";
		}
		$config['upload_path']          = './assets/logo_mitra/';
        $config['allowed_types']        = 'jpg|png|jpeg';
        $config['max_size']             = 0;

        $this->load->library('upload', $config);
		$this->upload->initialize($config);
        if ( ! $this->upload->do_upload('logo_mitra'))
        {
            $logo_mitra = $this->upload->display_errors();
        }
        else
        {
            $logo_mitra = $this->upload->data('file_name');
        }


        $kode_faskes = "";
        // Generate KodeFaskes
        if($tipe_mitra == "Klinik Umum / Mandiri"){
        	$kode_faskes = "KLU";
        }else if($tipe_mitra == "Rumah Sakit"){
        	$kode_faskes = "RS";
        }else if($tipe_mitra == "Organisasi"){
        	$kode_faskes = "ORG";
        }else{
        	$kode_faskes = "KLU";
        }


        $countFaskesbyKota = "SELECT * FROM table_faskes WHERE kota_faskes = '$kota_kabupaten_mitra'";
        $queryCountFaskes = $this->db->query($countFaskesbyKota);
        $nomor_faskes = $queryCountFaskes->num_rows();
        $nomor_urut_faskes = "";
        for ($a = 1; $a <= strlen($nomor_faskes); $a++){
			$nomor_urut_faskes = str_pad($nomor_faskes+1, 4, "0", STR_PAD_LEFT);
		}

        $merge_kode_faskes = "#".$kode_faskes.$kota_kabupaten_mitra.$nomor_urut_faskes;

		$data_faskes = [
			"kode_faskes" => $merge_kode_faskes,
			"nama_faskes" => $nama_mitra,
			"tipe_faskes" => $tipe_mitra,
			"alamat_faskes" => $alamat_mitra,
			"kota_faskes" => $kota_kabupaten_mitra,
			"email_faskes" => $email_mitra,
			"phone_faskes" => $phone_mitra,
			"longtitude_faskes" => $longtitude_mitra,
			"latitude_faskes" => $latitude_mitra,
			"logo_mitra" => $logo_mitra,
			"status_faskes" => "active",
		];
		$insert_faskes = $this->db->INSERT('table_faskes', $data_faskes);
		if($insert_faskes){
			// Query get last id_faskes from table_faskes
			$query = "SELECT id_faskes FROM table_faskes ORDER BY id_faskes DESC LIMIT 1";
			$result_id_faskes = $this->db->query($query)->result_array();
			$id_faskes = $result_id_faskes[0];
		}else{
			$id_faskes = "0000000000";
		}

		$data_user = [
			"id_faskes" => $id_faskes['id_faskes'],
			"nama_user" => $nama_user,
			"email_user" => $email_user,
			"password_user" => $password,
			"status_user" => "active",
		];
		$insert_user = $this->db->INSERT('table_user', $data_user);
		if($insert_user){
			$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Registrasi berhasil ! Silahkan login</div>');
			redirect('auth');
		}else{
			redirect('auth/daftarmitra');
		}
	}

	public function login(){
		$email = $this->input->post('email_login');
		$password = $this->input->post('password_login');

		$user = $this->db->get_where('table_user', ['email_user'=>$email])->row_array();
		if($user){
			if($user['status_user'] == "active"){
				if(password_verify($password, $user['password_user'])){
					$data_faskes = $this->db->get_where('table_faskes', ['id_faskes'=>$user['id_faskes']])->row_array();
					$data = [
						'id_user' => $user['id_user'],
						'id_faskes' => $user['id_faskes'],
						'logo_faskes' => $data_faskes['logo_mitra'],
					];

					$this->session->set_userdata($data);
					redirect('dashboard');
				}else{
					// password salah
					$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Kata sandi anda salah !</div>');
					redirect('auth');
				}
			}else{
				// status user tidak aktif
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Status akun anda telah dinonaktifkan !</div>');
				redirect('auth');
			}
		}else{
			// User belum terdaftar
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Akun anda belum terdaftar !</div>');
			redirect('auth');
		}
	}

	public function LupaPassword(){
		$x['title'] = "Lupa Password - CMS Klinis";

		$this->load->view('klinis/pages/lupaPassword', $x);
	}
	public function submitLupaPassword(){
		$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Link reset password telah dikirim, Silahkan cek</div>');
		redirect("Auth/LupaPassword");

		$email_lupa_password = $this->input->post("email_lupa_password");
		echo $email_lupa_password;
	}
}
