<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ExportPDF extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('id_user')){
        	redirect("Auth");
        }
    }
    public function dataPeserta($id_program){
         $sql_search_data_peserta = "
            SELECT a.*, b.*, c.* FROM table_sesi_program a
            LEFT JOIN table_koneksi_program b ON a.id_sesi = b.id_sesi
            LEFT JOIN table_peserta c ON b.id_peserta = c.id_peserta
            WHERE a.id_program = '$id_program'
            -- ORDER BY a.tanggal_sesi ASC
            -- AND a.tanggal_sesi = '2020-09-21'
        ";
        $data_peserta_program = $this->db->query($sql_search_data_peserta)->result_array();
        
        $data_program = $this->db->get_where('table_program', ['id_program'=>$id_program])->row_array();
        $tanggal_hari_ini = date("d F Y");

        $pdf = new FPDF('P','mm','A4');
        // membuat halaman baru
        $title = 'Judul PDF' ;
        $pdf->SetTitle($title );

        $pdf->AddPage();
        // $pdf->SetAutoPageBreak(false);
        // Jarak garis kiri, jarak atas, jarak kiri ke kanan, jarak atas ke bawah
        // $pdf->Rect(5,5,200,285);

        // jarak panjang cell , jarak lebar cell, isi data,
        // 0 tidak ada garis, 1 ada garis
        // 0 tidak enter, 1 enter
        // posisi text R right, C center, L left
        // $pdf->Cell(50, 20,'Tgl / Date :',1,0,'R');


        $pdf->SetFont('Arial','B',15);
        $pdf->Cell(190, 8,"DAFTAR PESERTA PROGRAM",0,1,'C');
        $pdf->Cell(190, 8,"'".$data_program['nama_program']."'",0,1,'C');
        $pdf->Ln(5);

        $pdf->SetFont('Arial','',9);
        $pdf->Cell(190, 8,"Dicetak tanggal : ".$tanggal_hari_ini,0,1,'L');
        $pdf->SetFont('Arial','B',9);

        $pdf->Cell(30, 8,"NIK",1,0,'C');
        $pdf->Cell(45, 8,"Name" ,1,0,'C');
        $pdf->Cell(25, 8,"Birth" ,1,0,'C');
        $pdf->Cell(15, 8,"Gender" ,1,0,'C');
        $pdf->Cell(25, 8,"Phone" ,1,0,'C');
        $pdf->Cell(25, 8,"Vaccination" ,1,0,'C');
        $pdf->Cell(25, 8,"Status" ,1,1,'C');

        foreach($data_peserta_program as $dpp){
            if($dpp['id_peserta'] != ""){
                if($dpp['status_koneksi'] == "Confirmed"){
                    $nama_peserta = str_replace("-", " ", $dpp['nama_peserta']);
                    if($dpp['jenis_kelamin_peserta'] == "Laki-laki"){
                        $jenis_kelamin = "L";
                    }else{
                        $jenis_kelamin = "P";
                    }
                    $pdf->SetFont('Arial','',8);
                    $pdf->Cell(30, 6,$dpp['nik_peserta'],1,0,'C');
                    $pdf->Cell(45, 6,$nama_peserta,1,0,'C');
                    $pdf->Cell(25, 6,date('d M Y', strtotime($dpp['tanggal_lahir_peserta'])),1,0,'C');
                    $pdf->Cell(15, 6,$jenis_kelamin,1,0,'C');
                    $pdf->Cell(25, 6,$dpp['phone_peserta'],1,0,'C');
                    $pdf->Cell(25, 6,date('d M Y', strtotime($dpp['tanggal_sesi'])),1,0,'C');
                    $pdf->Cell(25, 6,$dpp['status_koneksi'],1,1,'C');
                }
            }
        }

        $pdf->Output();
    }

    function laporanAbsenKaryawan($id_karyawan){
        $bulan_ini = date('m');
        // $absen_bulanan = $this->db->get('tbl_absensi_karyawan')->result();
        $absen_bulanan = $this->db->get_where('tbl_absensi_karyawan', ['month(user_jam_absensi)'=>$bulan_ini, 'user_id_absensi' => $id_karyawan])->result();

        $date_in = date('d-M-Y');

        $pdf = new FPDF('P','mm','A4');

        $count_halaman = 0;
        $total_absen = 0;
        foreach ($absen_bulanan as $a){
            $total_absen++;
        }

        $count_halaman = $total_absen / 50;
        $count = 1;
        if($count_halaman < 1){
            $count_halaman = 1;
        }
        for($page = 1; $page <= $count_halaman; $page++){
            // membuat halaman baru
            $pdf->AddPage();
            $pdf->SetAutoPageBreak(false);
            // $pdf->Rect(5,1,200,290);

            // Judul / Tanggal / Kode
            $pdf->SetFont('Arial','B',11);
            $pdf->Cell(30,-10,'Tgl / Date :',0,0,'R');
            $pdf->Cell(140,-10, $date_in ,0,0,'L');

            // Tanggal
            $pdf->SetFont('Arial','',10);
            $pdf->Cell(10,-10,'Halaman :',0,0,'R');
            $pdf->Cell(25,-10,$page,0,0,'L');

            // Enter
            $pdf->Ln(0);

            // Title
            $pdf->SetFont('Arial','B',12);
            $pdf->Cell(190,7,'Absensi Karyawan',0,1,'C');

            $pdf->Ln(8);

            $pdf->SetFont('Arial','B',10);
            $pdf->Cell(10,5,'No',1,0,'C');
            $pdf->Cell(40,5,'Nama Karyawan',1,0,'C');
            $pdf->Cell(40,5,'Jam Masuk',1,0,'C');
            $pdf->Cell(40,5,'Jam Keluar',1,0,'C');
            $pdf->Cell(30,5,'Status',1,1,'C');
            $pdf->SetFont('Arial','',10);

            // QUERY DATA BARANG PER HALAMAN
            // Jumlah data per halaman
            $limit = 50;

            $limitStart = ($page - 1) * $limit;
            if($limitStart < 50){
                $limitStart = 0;
            }

            $query_karyawan = "
                  SELECT * FROM tbl_absensi_karyawan WHERE MONTH(user_jam_absensi) = '$bulan_ini' AND user_id_absensi = '$id_karyawan' ORDER BY user_jam_absensi, user_name_absensi DESC
                  LIMIT $limitStart,$limit
                ";
            $absen_karyawan = $this->db->query($query_karyawan)->result_array();
            
            foreach ($absen_karyawan as $b){
                if($count <= ($page*40)){
                    if($b['user_status_absensi'] == "OUT"){
                        $pdf->SetTextColor(128 , 0, 0);
                        $pdf->SetDrawColor(128 , 0, 0);
                    }else{
                        $pdf->SetTextColor(0 , 128, 0);
                        $pdf->SetDrawColor(0 , 128, 0); 
                    }
                    $pdf->Cell(10, 5, $count, 1, 0,'C');
                    $pdf->Cell(40, 5, $b['user_name_absensi'], 1, 0,'C');
                    if($b['user_status_absensi'] == "IN"){
                        $pdf->Cell(40,5,$b['user_jam_absensi'],1,0);
                        $pdf->Cell(40,5,"",1,0);
                    }else{
                        $pdf->Cell(40,5,"",1,0);
                        $pdf->Cell(40,5,$b['user_jam_absensi'],1,0);
                    }
                    
                    $pdf->Cell(30,5,$b['user_status_absensi'],1,1, 'C');

                    $count++;
                }
            
            }

            $pdf->SetTextColor(0 , 0, 0);
            $pdf->SetDrawColor(0 , 0, 0);
            // END QUERY DATA BARANG PER HALAMAN


            $pdf->Ln(4);
            $pdf->Cell(45, 4, '      Applicant,', 0, 0, 'L');
            $pdf->Cell(100, 4, '', 0, 0, 'C');
            $pdf->Cell(35, 4, 'Approved by,', 0, 0, 'R');

            $pdf->Cell(140,33,'',0,1,'L');


            $pdf->Cell(40, 4, '(                                )', 0, 0, 'L');
            $pdf->Cell(115, 4, '', 0, 0, 'C');
            $pdf->Cell(35, 4, 'Thoberlin Viriyananda Putra S.Kom', 0, 0, 'R');
        }

        $pdf->Output();
    }

}