<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dash extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){
		$x['title'] = "Dashboard - CMS Klinis";

		$this->load->view('klinis/templates/header', $x);
		$this->load->view('klinis/templates/style');
		$this->load->view('klinis/templates/sidebar');
		$this->load->view('klinis/pages/dashboard');
		$this->load->view('klinis/templates/script');
		$this->load->view('klinis/templates/footer');
	}

}
