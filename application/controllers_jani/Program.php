<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Program extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('id_user')){
        	redirect("Auth");
        }
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){
		$x['title'] = "Program - CMS Klinis";
		$x['page_title'] = "Program";
		$x['data_user'] = $this->db->get_where('table_user', ['id_user'=>$this->session->userdata('id_user')])->row_array();

		$this->load->view('klinis/templates/header', $x);
		$this->load->view('klinis/templates/style');
		$this->load->view('klinis/templates/sidebar', $x);
		$this->load->view('klinis/pages/program', $x);
		$this->load->view('klinis/templates/script');
		$this->load->view('klinis/templates/footer');
	}

	public function vaksinasi(){
		$x['title'] = "List Program Vaksinasi - CMS Klinis";
		$x['page_title'] = "Program";
		$x['data_user'] = $this->db->get_where('table_user', ['id_user'=>$this->session->userdata('id_user')])->row_array();

		$x['list_program'] = $this->db->get_where('table_program', ['id_user'=>$x['data_user']['id_user']])->result_array();

		$this->load->view('klinis/templates/header', $x);
		$this->load->view('klinis/templates/style');
		$this->load->view('klinis/templates/sidebar');
		$this->load->view('klinis/pages/listprogram');
		$this->load->view('klinis/templates/modal');
		$this->load->view('klinis/templates/script');
		$this->load->view('klinis/templates/footer');
	}

	public function tambahprogram(){
		$x['title'] = "Tambah Program - CMS Klinis";
		$x['page_title'] = "Program";
		$x['data_user'] = $this->db->get_where('table_user', ['id_user'=>$this->session->userdata('id_user')])->row_array();

		$this->load->view('klinis/templates/header', $x);
		$this->load->view('klinis/templates/style');
		$this->load->view('klinis/templates/sidebar');
		$this->load->view('klinis/pages/tambahprogram');
		$this->load->view('klinis/templates/script');
		$this->load->view('klinis/templates/footer');
	}

	public function submitProgram(){
		$id_user = $this->input->post('id_user');
		$tipe_program = $this->input->post('tipe_program');
		$nama_program = $this->input->post('nama_program');
		$deskripsi_program = $this->input->post('deskripsi_program');
		$daterange = $this->input->post('daterange');
		$lokasi_program = $this->input->post('lokasi_program');
		$target_peserta_program = $this->input->post('target_peserta_program');
		$target_klien_program = $this->input->post('target_klien_program');
		$goals_program = $this->input->post('goals_program');
		$anggaran_program = $this->input->post('anggaran_program');

		// memecahkan tanggal program
		$pecah_tanggal = explode("-", $daterange);
		$tanggal_mulai_program = $pecah_tanggal[0];
		$tanggal_berakhir_program = $pecah_tanggal[1];

		// menghapus koma target peserta
		// $sortir_angka_peserta = preg_replace('/[^0-9]/','',$target_peserta_program);
		// $panjang_angka_peserta = strlen($sortir_angka_peserta);
 	// 	$new_target_peserta_program = substr($sortir_angka_peserta,0,($panjang_angka_peserta-2));

		// menghapus rupiah & koma anggaran
		$sortir_angka = preg_replace('/[^0-9]/','',$anggaran_program);
		$sortir_angka1 = preg_replace('/[Rp. ]/','',$sortir_angka);
		$panjang_angka = strlen($sortir_angka1);
 		$new_anggaran = substr($sortir_angka1,0,($panjang_angka-2));

		$data_program = [
			"id_user" => $id_user,
			"tipe_program" => $tipe_program,
			"nama_program" => $nama_program,
			"deskripsi_program" => $deskripsi_program,
			"tanggal_mulai_program" => date('Y-m-d', strtotime($tanggal_mulai_program)),
			"tanggal_berakhir_program" => date("Y-m-d", strtotime($tanggal_berakhir_program)),
			"lokasi_program" => $lokasi_program,
			"target_peserta_program" => $target_peserta_program,
			"target_klien_program" => $target_klien_program,
			"goals_program" => $goals_program,
			"anggaran_program" => $new_anggaran,
			"status_program" => "active",
		];

		$this->db->INSERT('table_program', $data_program);
		redirect("Program/vaksinasi");
	}

	public function editProgram(){
		$edit_id_program = $this->input->post('edit_id_program');
		$edit_id_user = $this->input->post('edit_id_user');
		$edit_tipe_program = $this->input->post('edit_tipe_program');
		$edit_nama_program = $this->input->post('edit_nama_program');
		$edit_deskripsi_program = $this->input->post('edit_deskripsi_program');
		$edit_program_start_date = $this->input->post('edit_program_start_date');
		$edit_program_end_date = $this->input->post('edit_program_end_date');
		$edit_lokasi_program = $this->input->post('edit_lokasi_program');
		$edit_target_peserta_program = $this->input->post('edit_target_peserta_program');
		$edit_target_klien_program = $this->input->post('edit_target_klien_program');
		$edit_goals_program = $this->input->post('edit_goals_program');
		$edit_anggaran_program = $this->input->post('edit_anggaran_program');

		//cek apakah mengandung , atau tidak
		// if (strpos($edit_target_peserta_program, ',') !== false) {
		// 	// menghapus koma target peserta
		//     $sortir_angka_peserta = preg_replace('/[^0-9]/','',$edit_target_peserta_program);
		// 	$panjang_angka_peserta = strlen($sortir_angka_peserta);
	 // 		$new_edit_target_peserta_program = substr($sortir_angka_peserta,0,($panjang_angka_peserta-2));
		// }else{
		// 	$new_edit_target_peserta_program = $edit_target_peserta_program;
		// }

		//cek apakah mengandung , atau tidak
		if (strpos($edit_anggaran_program, ',') !== false) {
			// menghapus rupiah & koma anggaran
			$sortir_angka = preg_replace('/[^0-9]/','',$edit_anggaran_program);
			$sortir_angka1 = preg_replace('/[Rp. ]/','',$sortir_angka);
			$panjang_angka = strlen($sortir_angka1);
	 		$new_anggaran = substr($sortir_angka1,0,($panjang_angka-2));
		}else{
			$new_anggaran = $edit_anggaran_program;
		}
		
		$data_program = [
			"tipe_program" => $edit_tipe_program,
			"nama_program" => $edit_nama_program,
			"deskripsi_program" => $edit_deskripsi_program,
			"tanggal_mulai_program" => date('Y-m-d', strtotime($edit_program_start_date)),
			"tanggal_berakhir_program" => date("Y-m-d", strtotime($edit_program_end_date)),
			"lokasi_program" => $edit_lokasi_program,
			"target_peserta_program" => $edit_target_peserta_program,
			"target_klien_program" => $edit_target_klien_program,
			"goals_program" => $edit_goals_program,
			"anggaran_program" => $new_anggaran,
		];
		$this->db->WHERE('id_program', $edit_id_program);
		$this->db->UPDATE('table_program', $data_program);
		redirect('Program/vaksinasi');
	}

	public function hapusProgram(){
		$id_program = $this->input->post('id_program');
		$this->db->WHERE('id_program', $id_program);
		$this->db->DELETE('table_program');
		redirect('Program/vaksinasi');
	}

	public function list($nama_program, $id_tanggal_mulai_program){
		$replace_url_nama = str_replace("-", " ", $nama_program);
		$panjang_angka = strlen($id_tanggal_mulai_program);
 		$id_program = substr($id_tanggal_mulai_program,0,($panjang_angka-8));
 		$tanggal_program = substr($id_tanggal_mulai_program,($panjang_angka-8),$panjang_angka);
        
		$x['title'] = $replace_url_nama." - CMS Klinis";
		$x['page_title'] = "Program";
		$x['data_user'] = $this->db->get_where('table_user', ['id_user'=>$this->session->userdata('id_user')])->row_array();
		$x['data_program'] = $this->db->get_where('table_program', ['id_program'=>$id_program])->row_array();
		$id_faskes = $x['data_user']['id_faskes'];
		$x['data_faskes'] = $this->db->get_where('table_faskes', ['id_faskes'=>$id_faskes])->row_array();
		
		$this->load->view('klinis/templates/header', $x);
		$this->load->view('klinis/templates/style');
		$this->load->view('klinis/templates/sidebar');
		$this->load->view('klinis/pages/vaksinasi/dashboard');
		$this->load->view('klinis/templates/script');
		$this->load->view('klinis/templates/modal');
		$this->load->view('klinis/templates/footer');
	}

	public function jadwal($nama_program, $id_tanggal_mulai_program){
		$replace_url_nama = str_replace("-", " ", $nama_program);
		$panjang_angka = strlen($id_tanggal_mulai_program);
 		$id_program = substr($id_tanggal_mulai_program,0,($panjang_angka-8));
 		$tanggal_program = substr($id_tanggal_mulai_program,($panjang_angka-8),$panjang_angka);
        
		$x['title'] = $replace_url_nama." - CMS Klinis";
		$x['page_title'] = "Program";
		$x['data_user'] = $this->db->get_where('table_user', ['id_user'=>$this->session->userdata('id_user')])->row_array();
		$x['data_program'] = $this->db->get_where('table_program', ['id_program'=>$id_program])->row_array();
		$id_faskes = $x['data_user']['id_faskes'];
		$x['data_faskes'] = $this->db->get_where('table_faskes', ['id_faskes'=>$id_faskes])->row_array();
		$x['data_sesi'] = $this->db->get_where('table_sesi_program', ['id_program'=>$id_program])->result_array();
		
		$this->load->view('klinis/templates/header', $x);
		$this->load->view('klinis/templates/style');
		$this->load->view('klinis/templates/sidebar');
		$this->load->view('klinis/pages/vaksinasi/pengaturan_jadwal');
		$this->load->view('klinis/templates/script');
		$this->load->view('klinis/templates/modal');
		$this->load->view('klinis/templates/footer');
	}
	public function tambahSesiProgram(){
		$tanggal_jadwal_program = $this->input->post('tanggal_jadwal_program');
		$waktu_jadwal_program_mulai = $this->input->post('waktu_jadwal_program_mulai');
		$waktu_jadwal_program_berakhir = $this->input->post('waktu_jadwal_program_berakhir');
		$kuota_jadwal_program = $this->input->post('kuota_jadwal_program');
		$id_program = $this->input->post('id_program_sesi');
		$link_url = $this->input->post('link_url');
		$id_sesi = $this->input->post('id_sesi');

		$edit_kuota = $this->input->post('edit_kuota');

		$data_sesi = [
			"id_program" => $id_program,
			// "tanggal_sesi" => date('Y-m-d', strtotime($tanggal_jadwal_program)),
			"tanggal_sesi" => $tanggal_jadwal_program,
			"waktu_mulai_sesi" => $waktu_jadwal_program_mulai,
			"waktu_berakhir_sesi" => $waktu_jadwal_program_berakhir,
			"kuota_sesi" => $kuota_jadwal_program,
			"status_sesi" => "active",
		];

		$get_kuota = $this->db->get_where('table_program', ['id_program'=>$id_program])->row_array();
		$kuota = $get_kuota['target_peserta_program'];

		$get_total_kuota = $this->db->get_where('table_sesi_program', ['id_program'=>$id_program, 'status_sesi'=>"active"])->result_array();
		$count_kuota=0;
		foreach($get_total_kuota as $gtk){
			$count_kuota += $gtk['kuota_sesi'];
		}

		if($id_sesi == ""){
			if(($count_kuota+$kuota_jadwal_program) <= $kuota){
				$this->db->INSERT('table_sesi_program', $data_sesi);
				$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Sesi berhasil ditambahkan !</div>');
			}else{
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Kuota sesi melebihi batas target !</div>');
			}
		}else{
			if($kuota_jadwal_program != $edit_kuota){
				$count_total_kuota_edit = $kuota_jadwal_program - $edit_kuota;
				if(($count_kuota+$count_total_kuota_edit) <= $kuota){
					$this->db->WHERE('id_sesi', $id_sesi);
					$this->db->UPDATE('table_sesi_program', $data_sesi);
					$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Sesi berhasil diubah !</div>');
				}else{
					$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Kuota sesi melebihi batas target !</div>');
				}
			}else{
				$this->db->WHERE('id_sesi', $id_sesi);
				$this->db->UPDATE('table_sesi_program', $data_sesi);
				$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Sesi berhasil diubah !</div>');
			}
		}
		redirect("Program/Jadwal/".$link_url);
	}
	public function hapusSesiProgram(){
		$id_sesi_hapus = $this->input->post('id_sesi_hapus');
		$link_url = $this->input->post('link_url_hapus_sesi');

		$this->db->SET('status_sesi', "Non-active");
		$this->db->WHERE('id_sesi', $id_sesi_hapus);
		$this->db->UPDATE('table_sesi_program');
		redirect("Program/Jadwal/".$link_url);
	}

	public function peserta($nama_program, $id_tanggal_mulai_program){
		$replace_url_nama = str_replace("-", " ", $nama_program);
		$panjang_angka = strlen($id_tanggal_mulai_program);
 		$id_program = substr($id_tanggal_mulai_program,0,($panjang_angka-8));
 		$tanggal_program = substr($id_tanggal_mulai_program,($panjang_angka-8),$panjang_angka);
        
		$x['title'] = $replace_url_nama." - CMS Klinis";
		$x['page_title'] = "Program";
		$x['data_user'] = $this->db->get_where('table_user', ['id_user'=>$this->session->userdata('id_user')])->row_array();
		$x['data_program'] = $this->db->get_where('table_program', ['id_program'=>$id_program])->row_array();
		$id_faskes = $x['data_user']['id_faskes'];
		$x['data_faskes'] = $this->db->get_where('table_faskes', ['id_faskes'=>$id_faskes])->row_array();
		$x['data_sesi'] = $this->db->get_where('table_sesi_program', ['id_program'=>$id_program])->result_array();
		
		$this->load->view('klinis/templates/header', $x);
		$this->load->view('klinis/templates/style');
		$this->load->view('klinis/templates/sidebar');
		$this->load->view('klinis/pages/vaksinasi/peserta');
		$this->load->view('klinis/templates/script');
		$this->load->view('klinis/templates/modal');
		$this->load->view('klinis/templates/footer');
	}

	public function reschedulePesertaProgram(){
		$id_koneksi = $this->input->POST("id_koneksi");
		$id_peserta = $this->input->POST("id_peserta");
		$link_url = $this->input->POST("link_url");
		$reschedule_sesi_program = $this->input->POST("reschedule_sesi_program_input");
		$new_reschedule_sesi_program = $this->input->POST("new_reschedule_sesi_program");
		$alasan_reschedule_peserta = $this->input->POST("alasan_reschedule_peserta");
		
		$koneksi = $this->db->get_where('table_koneksi_program', ['id_koneksi'=>$id_koneksi])->row_array();

		// INSERT DATA BARU
		$new_data_koneksi = [
			"id_peserta" => $id_peserta,
			"id_sesi" => $new_reschedule_sesi_program,
			"kategori_program" => $koneksi['kategori_program'],
			"status_koneksi" => "Waiting",
		];
		$this->db->INSERT('table_koneksi_program', $new_data_koneksi);

		// UPDATE DATA LAMA MENJADI STATUS RESCHEDULE
		$this->db->SET('alasan_status_koneksi', $alasan_reschedule_peserta);
		$this->db->SET('status_koneksi', "Reschedule");
		$this->db->WHERE('id_koneksi', $id_koneksi);
		$this->db->UPDATE('table_koneksi_program');

		// Update kuota sesi
		$sesi_lama = $this->db->get_where('table_sesi_program', ['id_sesi'=>$reschedule_sesi_program])->row_array();
		$kuota_lama = $sesi_lama['kuota_terdaftar'];
		
		// MENAMBAH LAGI SISA KUOTA SESI SEBELUM DIUBAH
		$this->db->SET('kuota_terdaftar', $kuota_lama-1);
		$this->db->WHERE('id_sesi', $reschedule_sesi_program);
		$this->db->UPDATE('table_sesi_program');

		$sesi_baru = $this->db->get_where('table_sesi_program', ['id_sesi'=>$new_reschedule_sesi_program])->row_array();
		$kuota_baru = $sesi_baru['kuota_terdaftar'];
		// MENGURANGI SISA KUOTA SESI SESUDAH DIUBAH
		$this->db->SET('kuota_terdaftar', $kuota_baru+1);
		$this->db->WHERE('id_sesi', $new_reschedule_sesi_program);
		$this->db->UPDATE('table_sesi_program');
		
		redirect('Program/Peserta/'.$link_url);
	}

	public function aktivasiPeserta($nama_program, $id_tanggal_mulai_program){
		$replace_url_nama = str_replace("-", " ", $nama_program);
		$panjang_angka = strlen($id_tanggal_mulai_program);
 		$id_program = substr($id_tanggal_mulai_program,0,($panjang_angka-8));
 		$tanggal_program = substr($id_tanggal_mulai_program,($panjang_angka-8),$panjang_angka);
        
		$x['title'] = $replace_url_nama." - CMS Klinis";
		$x['page_title'] = "Program";
		$x['data_user'] = $this->db->get_where('table_user', ['id_user'=>$this->session->userdata('id_user')])->row_array();
		$x['data_program'] = $this->db->get_where('table_program', ['id_program'=>$id_program])->row_array();
		$id_faskes = $x['data_user']['id_faskes'];
		$x['data_faskes'] = $this->db->get_where('table_faskes', ['id_faskes'=>$id_faskes])->row_array();
		
		$this->load->view('klinis/templates/header', $x);
		$this->load->view('klinis/templates/style');
		$this->load->view('klinis/templates/sidebar');
		$this->load->view('klinis/pages/vaksinasi/aktivasi_peserta');
		$this->load->view('klinis/templates/script');
		$this->load->view('klinis/templates/modal');
		$this->load->view('klinis/templates/footer');
	}

	public function confirmAktivasiProgram($nama_program, $id_tanggal_mulai_program, $id_koneksi){
		$this->db->SET('status_koneksi', "Confirmed");
		$this->db->WHERE('id_koneksi', $id_koneksi);
		$this->db->UPDATE('table_koneksi_program');
		
		redirect("Program/aktivasiPeserta/".$nama_program."/".$id_tanggal_mulai_program);
	}

	public function declinePesertaProgram(){
		$id_koneksi = $this->input->post('id_koneksi');
		$link_url = $this->input->post('link_url');
		$alasan_menolak_peserta = $this->input->post('alasan_menolak_peserta');

		$this->db->SET('alasan_status_koneksi', $alasan_menolak_peserta);
		$this->db->SET('status_koneksi', "Rejected");
		$this->db->WHERE('id_koneksi', $id_koneksi);
		$this->db->UPDATE('table_koneksi_program');
		redirect("Program/aktivasiPeserta/".$link_url);
	}

	public function registrasi($nama_program, $id_tanggal_mulai_program){
		$replace_url_nama = str_replace("-", " ", $nama_program);
		$panjang_angka = strlen($id_tanggal_mulai_program);
 		$id_program = substr($id_tanggal_mulai_program,0,($panjang_angka-8));
 		$tanggal_program = substr($id_tanggal_mulai_program,($panjang_angka-8),$panjang_angka);
        
		$x['title'] = $replace_url_nama." - CMS Klinis";
		$x['page_title'] = "Program";
		$x['data_user'] = $this->db->get_where('table_user', ['id_user'=>$this->session->userdata('id_user')])->row_array();
		$x['data_program'] = $this->db->get_where('table_program', ['id_program'=>$id_program])->row_array();
		$id_faskes = $x['data_user']['id_faskes'];
		$x['data_faskes'] = $this->db->get_where('table_faskes', ['id_faskes'=>$id_faskes])->row_array();
		$this->db->ORDER_BY('tanggal_sesi', "ASC");
		$this->db->ORDER_BY('waktu_mulai_sesi', "ASC");
		$x['data_sesi'] = $this->db->get_where('table_sesi_program', ['id_program'=>$id_program])->result_array();

		$this->load->view('klinis/pages/registrasi', $x);
	}

	public function registrasiPeserta(){
		$url_pendaftaran_peserta = $this->input->post('url_pendaftaran_peserta');

		$nik_peserta = $this->input->post('nik_peserta');
		$sesi_program = $this->input->post('sesi_program');
		$kategori_program = $this->input->post('kategori_program');
		$nama_depan_peserta = $this->input->post('nama_depan_peserta');
		$nama_belakang_peserta = $this->input->post('nama_belakang_peserta');
		$email_peserta = $this->input->post('email_peserta');
		$phone_peserta = $this->input->post('phone_peserta');
		$tanggal_lahir_peserta = $this->input->post('tanggal_lahir_peserta');
		$jenis_kelamin_peserta = $this->input->post('jenis_kelamin_peserta');
		$kota_peserta = $this->input->post('kota_peserta');
		$alamat_peserta = $this->input->post('alamat_peserta');
		$pekerjaan_peserta = $this->input->post('pekerjaan_peserta');
		$jenis_pekerjaan_peserta = $this->input->post('jenis_pekerjaan_peserta');
		

		$check_nik = $this->db->get_where('table_peserta', ['nik_peserta'=>$nik_peserta])->num_rows();
		
		if($check_nik == 0){
			$data_peserta = [
				"nik_peserta" => $nik_peserta,
				"nama_peserta" => $nama_depan_peserta."-".$nama_belakang_peserta,
				"email_peserta" => $email_peserta,
				"phone_peserta" => $phone_peserta,
				"tanggal_lahir_peserta" => $tanggal_lahir_peserta,
				"jenis_kelamin_peserta" => $jenis_kelamin_peserta,
				"kota_peserta" => $kota_peserta,
				"alamat_peserta" => $alamat_peserta,
				"pekerjaan_peserta" => $pekerjaan_peserta,
				"jenis_pekerjaan_peserta" => $jenis_pekerjaan_peserta,
				"status_peserta" => "active",
			];

			$insert_peserta = $this->db->INSERT('table_peserta', $data_peserta);
			if($insert_peserta){
				// Query get last id_peserta from table_peserta
				$query = "SELECT id_peserta FROM table_peserta ORDER BY id_peserta DESC LIMIT 1";
				$result_id_peserta = $this->db->query($query)->row_array();
				$id_peserta = $result_id_peserta['id_peserta'];
			}else{
				$id_peserta = "0000000000";
			}

			$data_koneksi = [
				"id_peserta" => $id_peserta,
				"id_sesi" => $sesi_program,
				"kategori_program" => $kategori_program,
				"status_koneksi" => "Waiting",
			];

			$this->db->INSERT('table_koneksi_program', $data_koneksi);

			// Update kuota sesi
			$sesi = $this->db->get_where('table_sesi_program', ['id_sesi'=>$sesi_program])->row_array();
			$kuota = $sesi['kuota_terdaftar'];
			$this->db->SET('kuota_terdaftar', $kuota+1);
			$this->db->WHERE('id_sesi', $sesi_program);
			$this->db->UPDATE('table_sesi_program');

			$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Registrasi berhasil !</div>');
			redirect('Program/Registrasi/'.$url_pendaftaran_peserta);
		}else{	
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">NIK telah terdaftar, silahkan gunakan NIK lain !</div>');
			redirect('Program/Registrasi/'.$url_pendaftaran_peserta);echo "data valid";
		}
	}

	public function CekPeserta($nama_program, $id_tanggal_mulai_program){
		$replace_url_nama = str_replace("-", " ", $nama_program);
		$panjang_angka = strlen($id_tanggal_mulai_program);
 		$id_program = substr($id_tanggal_mulai_program,0,($panjang_angka-8));
 		$tanggal_program = substr($id_tanggal_mulai_program,($panjang_angka-8),$panjang_angka);
        
		$x['title'] = $replace_url_nama." - CMS Klinis";
		$x['page_title'] = "Program";
		$x['data_user'] = $this->db->get_where('table_user', ['id_user'=>$this->session->userdata('id_user')])->row_array();
		$x['data_program'] = $this->db->get_where('table_program', ['id_program'=>$id_program])->row_array();
		$id_faskes = $x['data_user']['id_faskes'];
		$x['data_faskes'] = $this->db->get_where('table_faskes', ['id_faskes'=>$id_faskes])->row_array();
		$this->db->ORDER_BY('tanggal_sesi', "ASC");
		$this->db->ORDER_BY('waktu_mulai_sesi', "ASC");
		$x['data_sesi'] = $this->db->get_where('table_sesi_program', ['id_program'=>$id_program])->result_array();

		$this->load->view('klinis/pages/cekdataregistrasi', $x);
	}

}
