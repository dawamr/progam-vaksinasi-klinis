<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Get extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->Model('M_get');
    }
    public function getFaskesById(){
        $id_faskes = $this->input->post("id_faskes");
        $result = $this->M_get->getFaskesById($id_faskes);
        echo json_encode($result);
    }
    public function getUserById(){
        $id_user = $this->input->post("id_user");
        $result = $this->M_get->getUserById($id_user);
        echo json_encode($result);
    }
    public function getProgramById(){
        $id_program = $this->input->post("id_program");
        $result = $this->M_get->getProgramById($id_program);
        echo json_encode($result);
    }
    public function getSesiById(){
        $id_sesi = $this->input->post("id_sesi");
        $result = $this->M_get->getSesiById($id_sesi);
        echo json_encode($result);
    }
    public function getKotaById(){
        $id_kota = $this->input->post("id_kota");
        $result = $this->M_get->getKotaById($id_kota);
        echo json_encode($result);
    }
    public function getKategoriById(){
        $kode_kategori = $this->input->post("kode_kategori");
        $result = $this->M_get->getKategoriById($kode_kategori);
        echo json_encode($result);
    }

    public function getPesertaByNIKandPhone(){
        $nik = $this->input->post('nik');
        $phone_peserta = $this->input->post('phone_peserta');
        $result = $this->M_get->getPesertaByNIKandPhone($nik, $phone_peserta);
        echo json_encode($result);   
    }
    public function cekNIK(){
        $nik = $this->input->post('nik');
        $this->db->WHERE('nik_peserta', $nik);
        $result = $this->db->GET('table_peserta');
        if($result->num_rows() > 0){
            echo json_encode(array('response' => "invalid"));
        }else{
            echo json_encode(array('response' => "valid"));
        }

    }
    
}
