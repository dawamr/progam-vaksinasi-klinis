<?php 
    $replace_url_nama = str_replace(" ", "-", $data_program['nama_program']);
    $replace_url_tanggal = str_replace("-", "", $data_program['tanggal_mulai_program']);

    $tanggal_mulai_program = strtotime($data_program['tanggal_mulai_program']);
    $tanggal_berakhir_program = strtotime($data_program['tanggal_berakhir_program']); 

    $tanggal_sekarang = strtotime(date('Y-m-d'));

    $jarak = $tanggal_berakhir_program - $tanggal_mulai_program;
    $hari = $jarak / 60 / 60 / 24;

    $link_url_now = $replace_url_nama."/".$data_program['id_program'].$replace_url_tanggal;
    $link_url_register = base_url("Registrasi/Form/").$replace_url_nama."/".$data_program['id_program'].$replace_url_tanggal;

    $jarak_program_mulai = $tanggal_mulai_program - $tanggal_sekarang;
    $jarak_program_berakhir = $tanggal_berakhir_program - $tanggal_sekarang;
    $hari_akan_mulai = $jarak_program_mulai / 60 / 60 / 24;
    $hari_akan_berakhir = $jarak_program_berakhir / 60 / 60 / 24;

    $kuota_terdaftar_peserta = 0;
    foreach($data_sesi as $ds){
        $kuota_terdaftar_peserta += $ds['kuota_terdaftar'];
    }
?>
<div class="content">
    <div class="container-fluid">
        <div class="row mt-3">
            <div class="col-12 rounded">
                <div class="card-box pt-0 px-0 pb-5">
                    <span class="mb-4 font-weight-bold px-4 px-md-5 py-3 bg-custom text-light rounded">Program Yang Sedang Berlangsung</span>
                    <div class="px-md-5 mt-5">
                        <div class="card-box shadow-sm pt-0 px-0 pb-5">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card-box">
                                        <div class="row">
                                            <div class="col- mx-3">
                                                <h3 class=""><a href="<?=base_url()?>program/vaksinasi" class="text-custom"><i class="fa fa-chevron-left mr-3"></i></a><?= $data_program['nama_program']?></h3>
                                            </div>
                                            <div class="col- ml-auto mx-3 mt-2">
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn btn-outline-custom dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false" ><i class="fa fa-share-alt"></i> Share</button>
                                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 35px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                        <a class="dropdown-item py-3" href="void:javascript()" onclick="buttonShareCopyLink('<?= $link_url_register?>')"><i class="fa fa-copy"></i> Copy Link Pendaftaran</a>
                                                        <a class="dropdown-item py-3" href="void:javascript()" onclick="buttonShareWA('<?= $link_url_register?>')"><i class="fa fa-whatsapp"></i> Whatsapp</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="">
                                <nav class="navbar navbar-expand-lg navbar-dark bg-custom">
                                  <a class="navbar-brand d-block d-md-none" href="#">Menu</a>
                                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon"></span>
                                  </button>

                                  <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0 m-auto py-2 d-flex justify-content-center">
                                      <li class="nav-item mx-4 py-2">
                                        <a class="nav-link" href="<?= base_url('Program/List/').$link_url_now?>"><i class="fa fa-dashboard"></i> Dashboard</a>
                                      </li>
                                      <li class="nav-item mx-4 py-2 active">
                                        <a class="nav-link" href="<?= base_url('Program/Jadwal/').$link_url_now?>"><i class="fa fa-calendar"></i> Atur Jadwal Program</a>
                                      </li>
                                      <li class="nav-item mx-4 py-2">
                                        <a class="nav-link" href="<?= base_url('Program/Peserta/').$link_url_now?>"><i class="fa fa-users"></i> Peserta</a>
                                      </li>
                                      <li class="nav-item mx-4 py-2">
                                        <a class="nav-link" href="<?= base_url('Program/AktivasiPeserta/').$link_url_now?>"><i class="fa fa-check-square-o"></i> <b>Aktivasi Peserta</b></a>
                                      </li>
                                      <li class="nav-item mx-4 py-2 px-4 bg-white rounded">
                                        <?php
                                            if($hari_akan_berakhir > 0){
                                        ?>
                                            <a class="nav-link text-custom" 
                                            href="<?= $link_url_register?>" target="_blank">
                                            <b><i class="fa fa-paste"></i> Link Formulir Pendaftaran</b></a>
                                        <?php }else{?>
                                            <a class="btn nav-link text-custom" id="btn_link_formulir_berakhir">
                                                <b><i class="fa fa-paste"></i> Link Formulir Pendaftaran</b>
                                            </a>
                                        <?php }?>
                                      </li>
                                    </ul>
                                  </div>
                                </nav>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="card-box">
                                        <h4 class="header-title">Pengaturan Jadwal Program</h4>
                                        <form action="<?= base_url('Program/tambahSesiProgram')?>" method="POST" id="form_pengaturan_sesi_program">
                                            <input type="hidden" name="id_sesi" value="">
                                            <input type="hidden" name="id_program_sesi" value="<?= $data_program['id_program']?>">
                                            <input type="hidden" name="link_url" value="<?= $replace_url_nama.'/'.$data_program['id_program'].$replace_url_tanggal?>">
                                            <input type="hidden" name="edit_kuota" value="">
                                            
                                            <input type="hidden" name="edit_tanggal_sesi" value="">
                                            <input type="hidden" name="edit_waktu_mulai_sesi" value="">
                                            <input type="hidden" name="edit_waktu_berakhir_sesi" value="">
                                            
                                            <div class="text-center mt-4 mb-4">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <div class="card-box widget-flat border-info bg-info text-white">
                                                                    <h3 class="m-b-10"><?= $data_program['target_peserta_program']?></h3>
                                                                    <p class="text-uppercase m-b-5 font-13 font-600">Target Kuota</p>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="card-box bg-warning widget-flat border-warning text-white">
                                                                    <h3 class="m-b-10"><?= $kuota_terdaftar_peserta?></h3>
                                                                    <p class="text-uppercase m-b-5 font-13 font-600">Kuota Terdaftar</p>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-4">
                                                                <div class="card-box widget-flat border-danger bg-danger text-white">
                                                                    <h3 class="m-b-10"><?= $data_program['target_peserta_program'] - $kuota_terdaftar_peserta?></h3>
                                                                    <p class="text-uppercase m-b-5 font-13 font-600">Sisa Kuota</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row" id="div_ubah_sesi_program">


                                                            <div class="col-12">
                                                                <div class="flash-messages">
                                                                    <?= $this->session->flashdata('message')?>
                                                                </div>
                                                            </div>
                                                            <div class="col-12">
                                                                <h4 class="mb-4" id="ubah_form_pengaturan_sesi" class="lead">Tambahkan Sesi Program</h4>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <p class="mb-2"><b>Nama Sesi</b></p>
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <input class="form-control" type="text" placeholder="Nama Sesi (isi jika perlu)" name="nama_jadwal_sesi_program" id="nama_jadwal_sesi_program" required/>
                                                                    </div>
                                                                </div>
                                                            </div>

															<div class="col-md-2">
																<div class="form-group">
																	<label>Jenis Vaksin</label>
																	<select class="form-control" name="jenis_vaksin_program" required>
																		<option disabled selected value="">Pilih Jenis Vaksin</option>
																		<option value="AstraZeneca">AstraZeneca</option>
																		<option value="Sinovac-CoronaVac">Sinovac-CoronaVac</option>
																		<option value="Pfizer">Pfizer</option>
																	</select>
																</div>
															</div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <p class="mb-2"><b>Tanggal</b></p>
                                                                    <input type="date" class="form-control" name="tanggal_jadwal_program" id="tanggal_jadwal_program"
                                                                    min="<?= $data_program['tanggal_mulai_program']?>"
                                                                    max="<?= $data_program['tanggal_berakhir_program']?>"
                                                                    value="<?= $data_program['tanggal_mulai_program']?>"/>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <p class="mb-2"><b>Waktu</b></p>
                                                                <div class="row">
                                                                    <div class="col-6">
                                                                        <div class="form-group">
                                                                            <div class="input-group">
                                                                                <input class="form-control" name="waktu_jadwal_program_mulai" id="waktu_jadwal_program_mulai" required/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <div class="form-group">
                                                                            <div class="input-group">
                                                                                <input class="form-control" name="waktu_jadwal_program_berakhir" id="waktu_jadwal_program_berakhir" required/>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1 ml-auto">
                                                                <div class="form-group">
                                                                    <p class="mb-2"><b>Kuota</b></p>
                                                                    <input type="text" class="form-control" name="kuota_jadwal_program" id="kuota_jadwal_program" required />
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <br>
                                                                    <button type="button" class="btn btn-success rounded" id="btn_submit_form_pengaturan_sesi">Submit</button>
                                                                    <button type="button" class="btn btn-danger rounded" id="btn_reset_form_pengaturan_sesi" onclick="location.reload(true);">Reset</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="table-rep-plugin">
                                                            <div class="table-responsive">
                                                                <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.1/css/jquery.dataTables.min.css">
                                                                <table id="example" class="table table-striped" style="width:100%">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Sesi</th>
                                                                            <th>Nama Sesi</th>
																			<th>Jenis Vaksin</th>
                                                                            <th>Tanggal Sesi</th>
                                                                            <th>Waktu Sesi</th>
                                                                            <th>Kuota Sesi</th>
                                                                            <th>Status</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <?php
                                                                        $i = 1;
                                                                        $total_kuota_sesi_terdaftar = 0;
                                                                        foreach($data_sesi as $ds){
                                                                            if($ds['status_sesi'] == "active"){
                                                                    ?>
                                                                        <tr>
                                                                            <td><?= $i?></td>
                                                                            <td><?= $ds['nama_sesi']?></td>
																			<td><?= $ds['jenis_vaksin']?></td>
                                                                            <td><?= date('d F Y', strtotime($ds['tanggal_sesi']))?></td>
                                                                            <td><?= date('H:i', strtotime($ds['waktu_mulai_sesi'])). " - ".date('H:i', strtotime($ds['waktu_berakhir_sesi']))?></td>
                                                                            <td><?= $ds['kuota_sesi']?></td>
                                                                            <td>
                                                                                <button type="button" class="btn btn-warning rounded btn_edit_sesi" data-id_sesi="<?= $ds['id_sesi']?>">
                                                                                    <i class="fa fa-edit"></i>
                                                                                </button>
                                                                                <?php
                                                                                    if($hari_akan_mulai > 0){
                                                                                ?>
                                                                                <button type="button" class="btn btn-danger rounded btn_delete_sesi" data-id_sesi="<?= $ds['id_sesi']?>" data-link_url="<?= $replace_url_nama.'/'.$data_program['id_program'].$replace_url_tanggal?>">
                                                                                    <i class="fa fa-trash"></i>
                                                                                </button>
                                                                                <?php }?>
                                                                            </td>
                                                                        </tr>
                                                                    <?php
                                                                                $total_kuota_sesi_terdaftar+=$ds['kuota_sesi'];
                                                                                $i++;
                                                                            }
                                                                        }
                                                                    ?>
                                                                        
                                                                    </tbody>
                                                                    <tfoot>
                                                                        <tr class="table-info">
                                                                            <td colspan="5" class="font-weight-bold">Total Kuota Yang Terdaftar</td>
                                                                            <td class="font-weight-bold"><?= $total_kuota_sesi_terdaftar?></td>
                                                                            <td></td>
                                                                        </tr>
                                                                    </tfoot>
                                                                </table>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div><!-- end col -->

                                

                            </div>
                            <!-- end row -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
