<?php 
    $replace_url_nama = str_replace(" ", "-", $data_program['nama_program']);
    $replace_url_tanggal = str_replace("-", "", $data_program['tanggal_mulai_program']);

    $tanggal_mulai_program = strtotime($data_program['tanggal_mulai_program']);
    $tanggal_berakhir_program = strtotime($data_program['tanggal_berakhir_program']); 

    $tanggal_sekarang = strtotime(date('Y-m-d'));

    $jarak = $tanggal_berakhir_program - $tanggal_mulai_program;
    $hari = $jarak / 60 / 60 / 24;

    $link_url_now = $replace_url_nama."/".$data_program['id_program'].$replace_url_tanggal;
    $link_url_register = base_url("Registrasi/Form/").$replace_url_nama."/".$data_program['id_program'].$replace_url_tanggal;

    $jarak_program_mulai = $tanggal_mulai_program - $tanggal_sekarang;
    $jarak_program_berakhir = $tanggal_berakhir_program - $tanggal_sekarang;
    $hari_akan_mulai = $jarak_program_mulai / 60 / 60 / 24;
    $hari_akan_berakhir = $jarak_program_berakhir / 60 / 60 / 24;
?>
<div class="content">
    <div class="container-fluid">
        <div class="row mt-3">
            <div class="col-12 rounded">
                <div class="card-box pt-0 px-0 pb-5">
                    <span class="mb-4 font-weight-bold px-4 px-md-5 py-3 bg-custom text-light rounded">Program Yang Sedang Berlangsung</span>
                    <div class="px-md-5 mt-5">
                        <div class="card-box shadow-sm pt-0 px-0 pb-5">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card-box">
                                        <div class="row">
                                            <div class="col- mx-3">
                                                <h3 class=""><a href="<?=base_url()?>program/vaksinasi" class="text-custom"><i class="fa fa-chevron-left mr-3"></i></a><?= $data_program['nama_program']?></h3>
                                            </div>
                                            <div class="col- ml-auto mx-3 mt-2">
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn btn-outline-custom dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false" ><i class="fa fa-share-alt"></i> Share</button>
                                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="btnGroupDrop1" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 35px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                        <a class="dropdown-item py-3" href="void:javascript()" onclick="buttonShareCopyLink('<?= $link_url_register?>')"><i class="fa fa-copy"></i> Copy Link Pendaftaran</a>
                                                        <a class="dropdown-item py-3" href="void:javascript()" onclick="buttonShareWA('<?= $link_url_register?>')"><i class="fa fa-whatsapp"></i> Whatsapp</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="">
                                <nav class="navbar navbar-expand-lg navbar-dark bg-custom">
                                  <a class="navbar-brand d-block d-md-none" href="#">Menu</a>
                                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon"></span>
                                  </button>

                                  <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0 m-auto py-2 d-flex justify-content-center">
                                      <li class="nav-item mx-4 py-2">
                                        <a class="nav-link" href="<?= base_url('Program/List/').$link_url_now?>"><i class="fa fa-dashboard"></i> Dashboard</a>
                                      </li>
                                      <li class="nav-item mx-4 py-2">
                                        <a class="nav-link" href="<?= base_url('Program/Jadwal/').$link_url_now?>"><i class="fa fa-calendar"></i> Atur Jadwal Program</a>
                                      </li>
                                      <li class="nav-item mx-4 py-2 active">
                                        <a class="nav-link" href="<?= base_url('Program/Peserta/').$link_url_now?>"><i class="fa fa-users"></i> Peserta</a>
                                      </li>
                                      <li class="nav-item mx-4 py-2">
                                        <a class="nav-link" href="<?= base_url('Program/AktivasiPeserta/').$link_url_now?>"><i class="fa fa-check-square-o"></i> <b>Aktivasi Peserta</b></a>
                                      </li>
                                      <li class="nav-item mx-4 py-2 px-4 bg-white rounded">
                                        <?php
                                            if($hari_akan_berakhir > 0){
                                        ?>
                                            <a class="nav-link text-custom" 
                                            href="<?= $link_url_register?>" target="_blank">
                                            <b><i class="fa fa-paste"></i> Link Formulir Pendaftaran</b></a>
                                        <?php }else{?>
                                            <a class="btn nav-link text-custom" id="btn_link_formulir_berakhir">
                                                <b><i class="fa fa-paste"></i> Link Formulir Pendaftaran</b>
                                            </a>
                                        <?php }?>
                                      </li>
                                    </ul>
                                  </div>
                                </nav>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="card-box">
                                        <h4 class="header-title">Peserta Program</h4>
                                        <form method="POST" id="form_filter_peserta" target="_blank">
                                        <!-- <form action="<?= base_url('ExportPDF/dataPeserta')?>" method="POST" id="form_filter_peserta" target="_blank"> -->
                                            <input type="hidden" name="id_program_filter" value="<?= $data_program['id_program']?>">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label for="pilih_tanggal_export" class="col-form-label">
                                                            Tanggal Program
                                                        </label>
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="row">
                                                                        <div class="col-6">
                                                                            <input type="date" class="form-control" name="pilih_tanggal_export_start" id="pilih_tanggal_export_start"
                                                                            min="<?= $data_program['tanggal_mulai_program']?>"
                                                                            max="<?= $data_program['tanggal_berakhir_program']?>"
                                                                            value="<?= $data_program['tanggal_mulai_program']?>"/>
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <input type="date" class="form-control" name="pilih_tanggal_export_end" id="pilih_tanggal_export_end"
                                                                            min="<?= $data_program['tanggal_mulai_program']?>"
                                                                            max="<?= $data_program['tanggal_berakhir_program']?>"
                                                                            value="<?= $data_program['tanggal_mulai_program']?>"/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="pilih_status_export" class="col-form-label">
                                                            Status Peserta
                                                        </label>
                                                        <div class="">
                                                            <span id="span_kategori_program_peserta_error" class="text-danger"></span>
                                                            <select id="pilih_status_export" class="form-control" name="pilih_status_export" required>
                                                                <option value="Semua">Pilih Status Peserta (default : Semua Status)</option>
                                                                <option value="Confirmed">Confirmed</option>
                                                                <option value="Waiting">Waiting</option>
                                                                <option value="Rejected">Rejected</option>
                                                                <option value="Reschedule">Reschedule</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="" class="col-form-label">Export</label>
                                                        <div class="">
                                                            <button class="btn btn-warning" id="btn_export_pdf_peserta" type="button">
                                                                <i class="fa fa-save"></i> PDF
                                                            </button>
                                                            
                                                            <button class="btn btn-success" id="btn_export_excel_peserta" type="button">
                                                                <i class="fa fa-save"></i> Excel
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="table-rep-plugin">
                                            <div class="table-responsive">
                                            <?php
                                                $id_program = $data_program['id_program'];

                                                $sql_search_data_peserta = "
                                                    SELECT a.*, b.*, c.* FROM table_sesi_program a
                                                    LEFT JOIN table_koneksi_program b ON a.id_sesi = b.id_sesi
                                                    LEFT JOIN table_peserta c ON b.id_peserta = c.id_peserta
                                                    WHERE a.id_program = '$id_program'
                                                    -- ORDER BY a.tanggal_sesi ASC
                                                    -- AND a.tanggal_sesi = '2020-09-21'
                                                ";

                                                $data_peserta_program = $this->db->query($sql_search_data_peserta)->result_array();
                                            ?>
                                                <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.1/css/jquery.dataTables.min.css">
                                                <table id="peserta" class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>NIK</th>
                                                            <th>Nama</th>
															<th>No Antrian</th>
                                                            <th>L/P</th>
                                                            <th>Tanggal Lahir</th>
                                                            <th>Umur</th>
                                                            <th>Instansi Pekerjaan</th>
                                                            <th>Jenis Pekerjaan</th>
                                                            <th>Kode Kategori</th>
                                                            <th>Phone</th>
                                                            <th>Alamat</th>
                                                            <th>Tanggal Sesi</th>
                                                            <th>Status</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                            <?php
                                                foreach($data_peserta_program as $dpp){
                                                    if($dpp['id_peserta'] != ""){
                                                        $nama_peserta = str_replace("-", " ", $dpp['nama_peserta']);

														$bithdayDate = $dpp['tanggal_lahir_peserta'];
														$date = new DateTime($bithdayDate);
														$now = new DateTime();
														$interval = $now->diff($date);
														$umur =  $interval->y;													   
                                            ?>
                                                        <tr>
                                                            <td><?= $dpp['nik_peserta']?></td>
                                                            <td><?= $nama_peserta?></td>
															<td><?= $dpp['no_antrian'] ?></td>
                                                            <td>
                                                                <?php
                                                                    if($dpp['jenis_kelamin_peserta'] == "Laki-laki"){
                                                                        echo "L";
                                                                    }else{
                                                                        echo "P";
                                                                    }
                                                                ?>
                                                            </td>
                                                            <td><?= date('d-M-Y', strtotime($dpp['tanggal_lahir_peserta']))?></td>

                                                            <td><?= $umur ?></td>
                                                            <td><?= $dpp['pekerjaan_peserta']?></td>
                                                            <td><?= $dpp['jenis_pekerjaan_peserta']?></td>
                                                            <td><?= $dpp['kategori_program']?></td>
                                                            <td><?= $dpp['phone_peserta']?></td>
                                                            <td><?= $dpp['alamat_peserta']?></td>
                                                            <td><?= date('d-M-Y', strtotime($dpp['tanggal_sesi']))?></td>
                                                            <td><?= $dpp['status_koneksi']?></td>
                                                            <td>
                                                                <?php if($dpp['status_koneksi'] != "Reschedule"):?>
                                                                <button type="button" class="btn btn-info rounded btn_reschedule_peserta" 
                                                                data-id_koneksi="<?= $dpp['id_koneksi']?>"
                                                                data-id_peserta="<?= $dpp['id_peserta']?>"
                                                                data-link_url="<?= $replace_url_nama."/".$data_program['id_program'].$replace_url_tanggal?>"
                                                                data-tanggal_terdaftar="<?= $dpp['tanggal_sesi']?>"
                                                                data-sesi_terdaftar="<?= $dpp['id_sesi']?>"
                                                                data-tanggal_mulai_program="<?= $data_program['tanggal_mulai_program']?>"
                                                                >
                                                                        <i class="fa fa-edit"></i>
                                                                </button>
                                                                <?php else:?>
                                                                    <span>No Action !</span>
                                                                <?php endif;?>
                                                            </td>
                                                        </tr>
                                            <?php
                                                    }
                                                }
                                            ?>
                                                    </tbody>
                                                </table>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- end col -->
                            </div>
                            <!-- end row -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
