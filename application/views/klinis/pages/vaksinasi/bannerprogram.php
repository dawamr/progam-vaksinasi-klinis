<div class="col-12">
                                    <div class="card-box rounded">
                                        <div class="widget-chart-one-content p-4 p-md-5 shadow-sm text-left">
                                            <div class="row mt-4 mb-2">
                                                <div class="col-md-8">
                                                    <h3 class=""><?= $data_program['nama_program']?></h3>
                                                </div>
                                                <div class="col- ml-auto mx-3 mt-2">

                                                    <?php
                                                        if($hari_akan_mulai > 0 && $hari_akan_berakhir > 0){
                                                    ?>
                                                        <p class="px-4 py-2 text-light bg-info btn-rounded waves-light waves-effect">
                                                            Akan Berlangsung
                                                        </p>
                                                    <?php
                                                        }else if($hari_akan_mulai <= 0 && $hari_akan_berakhir > 0){
                                                    ?>
                                                        <p class="px-4 py-2 text-light bg-success btn-rounded waves-light waves-effect">
                                                            Sedang Berlangsung
                                                        </p>
                                                    <?php
                                                        }else{
                                                    ?>
                                                        <p class="px-4 py-2 text-light bg-danger btn-rounded waves-light waves-effect">
                                                            Telah Berakhir
                                                        </p>
                                                    <?php
                                                        }
                                                    ?>
                                                </div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <h5>Tanggal / Waktu</h5>
                                                    <p class="">
                                                        <?= date("l, d F Y", strtotime($data_program['tanggal_mulai_program']))?>
                                                        - 
                                                        <?= date("l, d F Y", strtotime($data_program['tanggal_berakhir_program']))?>
                                                        <span class="text-custom">(<?= $hari?> Hari)</span>
                                                    </p>
                                                </div>
                                                <div class="col-md-4">
                                                    <h5>Lokasi</h5>
                                                    <p><a href="https://www.google.com/maps/search/<?= $data_program['lokasi_program']?>" target="_blank" class="text-custom"><i class="fa fa-map-o"></i> <?= $data_program['lokasi_program']?></a></p>
                                                </div>
                                                <div class="col-md-4">
                                                    <h5>Penyelenggara</h5>
                                                    <img src="<?= base_url('assets/logo_mitra/').$data_faskes['logo_mitra']?>" alt="logo" style="object-fit:scale-down; max-width:250px; max-height:100px;" class="img-responsive">
                                                    <h4><?= $data_faskes['nama_faskes']?></h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>