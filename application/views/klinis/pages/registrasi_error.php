
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Registrasi Program Vaksinasi</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta content="Dashboard Klinis for Partners." name="description" />
        <meta content="PT Kreasi Layanan Medis (Klinis)" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- App favicon -->
        <link rel="shortcut icon" href="<?=base_url();?>assets/images/favicon.ico">

        <!--Form Wizard-->
        <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/plugins/jquery.steps/css/jquery.steps.css" />

        <!-- Sweet Alert css -->
        <link href="<?=base_url();?>assets/plugins/sweet-alert/sweetalert2.min.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="<?=base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>assets/css/metismenu.min.css" rel="stylesheet" type="text/css" />
        <link href="<?=base_url();?>assets/css/style.css" rel="stylesheet" type="text/css" />

        <script src="<?=base_url();?>assets/js/modernizr.min.js"></script>

    </head>


    <body>
        <!-- Begin page -->
        <div id="wrapper">


            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->

            <nav class="navbar navbar-light bg-white">
                <div class="page-title-box">
                    <a class="py-3 px-4" href="#"><img src="<?=base_url()?>assets/images/logo.png" alt="" height="40"></a>
                </div>
                </nav>

            <div class="container">
                <!-- Start Page content -->
                <div class="content mt-5">
                    <div class="container-fluid mt-5">

                        <!-- Basic Form Wizard -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-box text-center">
                                    <h4 class="m-t-0 header-title"><b>Program telah berakhir!</b></h4>
                                    <p class="text-muted m-b-30 font-13">
                                        Silahkan hubungi mitra program untuk konfirmasi link !
                                    </p>
                                </div>
                            </div>
                        </div>

                        <!-- End row -->

                    </div> <!-- container -->

                </div> <!-- content -->

                <footer class="footer text-right">
                    2018 © Highdmin. - Coderthemes.com
                </footer>

            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->
        </div>
        <!-- END wrapper -->


        <!-- jQuery  -->
        <script src="<?=base_url();?>assets/js/jquery.min.js"></script>
        <script src="<?=base_url();?>assets/js/popper.min.js"></script>
        <script src="<?=base_url();?>assets/js/bootstrap.min.js"></script>
        <script src="<?=base_url();?>assets/js/metisMenu.min.js"></script>
        <script src="<?=base_url();?>assets/js/waves.js"></script>
        <script src="<?=base_url();?>assets/js/jquery.slimscroll.js"></script>

        <!--Form Wizard-->
        <script src="<?=base_url();?>assets/plugins/jquery.steps/js/jquery.steps.min.js" type="text/javascript"></script>

        <!--wizard initialization-->
        <script src="<?=base_url();?>assets/pages/jquery.wizard-init.js" type="text/javascript"></script>

        <!-- Sweet Alert Js  -->
        <script src="<?=base_url();?>assets/plugins/sweet-alert/sweetalert2.min.js"></script>
            <script src="<?=base_url();?>assets/pages/jquery.sweet-alert.init.js"></script>

        <!-- App js -->
        <script src="<?=base_url();?>assets/js/jquery.core.js"></script>
        <script src="<?=base_url();?>assets/js/jquery.app.js"></script>
        
        <!-- Script alert message -->
        <script type="text/javascript">
            $(function(){
                $(".flash-messages").delay(2500).fadeOut();
                setTimeout(function(){ 
                    <?php $this->session->set_flashdata('message',"");?>
                }, 3000);
            });
        </script>

        <script type="text/javascript">
            function buttonShareWA($url){
                window.open("whatsapp://send?text=Hai, ayo ikut program vaksin dengan klik link berikut ini ya!!! "+$url+" Buruan daftar sebelum kuota habis !");
            }

            function buttonShareCopyLink($url){
                navigator.clipboard.writeText($url);
            }
        </script>
    </body>
</html>