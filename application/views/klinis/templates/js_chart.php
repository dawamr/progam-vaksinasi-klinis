<script src="<?=base_url()?>assets/plugins/chartjs/chart.min.js"></script>

<!-- MODAL PIE CHART PESERTA TERDAFTAR-->
<div class="modal fade chart-peserta-terdaftar" tabindex="-1" role="dialog" aria-labelledby="pieChartNama" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="pieChartNama">Informasi Kuota</h4>
            </div>
            <div class="modal-body d-flex justify-content-center">
            	<div class="row justify-content-center">
	                <div class="col-md-6">
	                    <div class="chart-container">
	                        <div class="doughnut-chart-container">
	                            <canvas id="canvas-chart-peserta-terdaftar-by-kota-kabupaten" height="500" width="500"></canvas>
	                        </div>
	                    </div>
	                </div>
	                <div class="col-md-6">
	                    <div class="chart-container">
	                        <div class="doughnut-chart-container">
	                            <canvas id="canvas-chart-peserta-terdaftar-by-kategori" height="500" width="500"></canvas>
	                        </div>
	                    </div>
	                </div>
	                <div class="col-md-6">
	                    <div class="chart-container">
	                        <div class="doughnut-chart-container">
	                            <canvas id="canvas-chart-peserta-terdaftar-by-sesi" height="500" width="500"></canvas>
	                        </div>
	                    </div>
	                </div>
            	</div>
            </div>
            <div class="modal-footer">
                <div class="form-group text-right m-b-0">
                    <button type="button" class="btn btn-light waves-effect m-l-5" data-dismiss="modal">Cancel</button>
                </div>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- MODAL PIE CHART KUOTA TERSEDIA-->
<div class="modal fade chart-kuota-tersedia" tabindex="-1" role="dialog" aria-labelledby="pieChartNama" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="pieChartNama">Informasi Kuota</h4>
            </div>
            <div class="modal-body d-flex justify-content-center">
            	<div class="row">
	                <div class="col-md-6">
	                    <div class="chart-container">
	                        <div class="doughnut-chart-container">
	                            <canvas id="canvas-chart-kuota-tersedia-by-sesi" height="500" width="500"></canvas>
	                        </div>
	                    </div>
	                </div>
	            </div>
            </div>
            <div class="modal-footer">
                <div class="form-group text-right m-b-0">
                    <button type="button" class="btn btn-light waves-effect m-l-5" data-dismiss="modal">Cancel</button>
                </div>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- MODAL PIE CHART AKTIVASI PESERTA-->
<div class="modal fade chart-aktivasi-peserta" tabindex="-1" role="dialog" aria-labelledby="pieChartNama" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="pieChartNama">Aktivasi Peserta</h4>
            </div>
            <div class="modal-body d-flex justify-content-center">
            	<div class="row">
	                <div class="col-md-8">
	                    <div class="chart-container">
	                        <div class="doughnut-chart-container">
	                            <canvas id="canvas-chart-aktivasi-peserta" height="500" width="500"></canvas>
	                        </div>
	                    </div>
	                </div>
	            </div>
            </div>
            <div class="modal-footer">
                <div class="form-group text-right m-b-0">
                    <button type="button" class="btn btn-light waves-effect m-l-5" data-dismiss="modal">Cancel</button>
                </div>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<?php
$id_program = $data_program['id_program'];

// Query untuk mendapatkan daftar peserta berdasarkan kota kabupaten
$sql_peserta_by_kab_kota = "
	SELECT COUNT(b.id_peserta) as jumlah_peserta_by_kuota, c.nama_kota_kabupaten, c.kode_kota_kabupaten FROM table_koneksi_program a
	LEFT JOIN table_peserta b ON a.id_peserta = b.id_peserta
	LEFT JOIN table_kota_kabupaten c ON b.kota_peserta = c.kode_kota_kabupaten
	LEFT JOIN table_sesi_program d ON a.id_sesi = d.id_sesi
	WHERE d.id_program = '$id_program'
	GROUP BY b.kota_peserta
";
$count_peserta_terdaftar = $this->db->query($sql_peserta_by_kab_kota)->result_array();
$arr_nama_kota_kab = [];
foreach($count_peserta_terdaftar as $cpt){
	array_push($arr_nama_kota_kab, $cpt['nama_kota_kabupaten']);
}

// Query untuk mendapatkan daftar peserta berdasarkan kategori program
$sql_peserta_by_kategori_program = "
	SELECT COUNT(b.id_peserta) as jumlah_peserta_by_kategori, c.nama_kota_kabupaten, c.kode_kota_kabupaten, a.kategori_program FROM table_koneksi_program a
	LEFT JOIN table_peserta b ON a.id_peserta = b.id_peserta
	LEFT JOIN table_kota_kabupaten c ON b.kota_peserta = c.kode_kota_kabupaten
	LEFT JOIN table_sesi_program d ON a.id_sesi = d.id_sesi
	WHERE d.id_program = '$id_program'
	GROUP BY a.kategori_program
";
$count_peserta_terdaftar_by_kategori = $this->db->query($sql_peserta_by_kategori_program)->result_array();
$arr_nama_kategori = [];
foreach($count_peserta_terdaftar_by_kategori as $cptbk){
	array_push($arr_nama_kategori, $cptbk['kategori_program']);
}

// Query untuk mendapatkan daftar peserta berdasarkan sesi
$sql_peserta_by_sesi_program = "
	SELECT COUNT(b.id_peserta) as jumlah_peserta_by_sesi, c.nama_kota_kabupaten, c.kode_kota_kabupaten, a.kategori_program, a.id_sesi, d.*
	FROM table_koneksi_program a
	LEFT JOIN table_peserta b ON a.id_peserta = b.id_peserta
	LEFT JOIN table_kota_kabupaten c ON b.kota_peserta = c.kode_kota_kabupaten
	LEFT JOIN table_sesi_program d ON a.id_sesi = d.id_sesi
	WHERE d.id_program = '$id_program'
	GROUP BY a.id_sesi
";
$count_peserta_terdaftar_by_sesi = $this->db->query($sql_peserta_by_sesi_program)->result_array();
$arr_nama_sesi = [];
foreach($count_peserta_terdaftar_by_sesi as $cptbs){
	if($cptbs['nama_sesi'] == ""){
		array_push($arr_nama_sesi, $cptbs['id_sesi']." | ".date('d-F-Y', strtotime($cptbs['tanggal_sesi']))." | ".date('H:i', strtotime($cptbs['waktu_mulai_sesi']))."-".date('H:i', strtotime($cptbs['waktu_berakhir_sesi'])) );
	}else{
		array_push($arr_nama_sesi, $cptbs['nama_sesi']." | ".date('d-F-Y', strtotime($cptbs['tanggal_sesi']))." | ".date('H:i', strtotime($cptbs['waktu_mulai_sesi']))."-".date('H:i', strtotime($cptbs['waktu_berakhir_sesi'])) );
	}
}

// Query untuk cek ketersediaan kuota berdasarkan sesi program
$sql_kuota_tersedia_berdasarkan_sesi = "
	SELECT * FROM table_sesi_program
	WHERE id_program = '$id_program'
	ORDER BY tanggal_sesi ASC, waktu_mulai_sesi
";
$count_kuota_tersedia_berdasarkan_sesi = $this->db->query($sql_kuota_tersedia_berdasarkan_sesi)->result_array();
$arr_sisa_kuota_by_sesi = [];
foreach($count_kuota_tersedia_berdasarkan_sesi as $cktbs){
	if($cktbs['status_sesi'] == "active"){
		array_push($arr_sisa_kuota_by_sesi, $cktbs['id_sesi']." | ".date('d-F-Y', strtotime($cktbs['tanggal_sesi']))." | ".date('H:i', strtotime($cktbs['waktu_mulai_sesi']))."-".date('H:i', strtotime($cktbs['waktu_berakhir_sesi'])) );
	}
}



// Query untuk aktivasi peserta (mendapatkan result peserta yang sudah teraktivasi atau belum)
$sql_search_data_peserta = "
        SELECT a.*, b.*, c.* FROM table_sesi_program a
        LEFT JOIN table_koneksi_program b ON a.id_sesi = b.id_sesi
        LEFT JOIN table_peserta c ON b.id_peserta = c.id_peserta
        WHERE a.id_program = '$id_program';
    ";
$data_peserta_program = $this->db->query($sql_search_data_peserta)->result_array();

$count_reschedule = 0;
$count_waiting = 0;
$count_rejected = 0;
$count_confirm = 0;
foreach ($data_peserta_program as $dpp) {
	if($dpp['status_koneksi'] == "Reschedule"){
		$count_reschedule++;
	}elseif($dpp['status_koneksi'] == "Rejected"){
		$count_rejected++;
	}elseif($dpp['status_koneksi'] == "Confirmed"){
		$count_confirm++;
	}elseif($dpp['status_koneksi'] == "Waiting"){
		$count_waiting++;
	}
}

?>


<!-- Chart Peserta Terdaftar -->
<script>

	$(function(){
		//get the doughnut chart canvas
		var canvas_kota_kabupaten = $("#canvas-chart-peserta-terdaftar-by-kota-kabupaten");

		//doughnut chart data
		var data_canvas_kota_kabupaten = {
			labels: [
				<?php 
					for($i = 0; $i < count($arr_nama_kota_kab); $i++){
						echo "'".$arr_nama_kota_kab[$i]."'".",";
					}
				?>
			],
			datasets: [{
				label: "Peserta Berdasarkan Kabupaten / Kota",
				data: [
				<?php
					foreach($count_peserta_terdaftar as $cpt){
						echo $cpt['jumlah_peserta_by_kuota'].',';
					}
				?>],
				backgroundColor: [
					'#02c0ce','#2d7bf4','#e3eaef','#f1556c',"#f9bc0b", "#49220b"
				],
				borderColor: [
					'#02c0ce','#2d7bf4','#e3eaef','#f1556c',"#f9bc0b", "#49220b"
				],
				borderWidth: [1,1,1,1,1,1]
			}]
		};

		//options
		var options_canvas_kota_kabupaten = {
			responsive: true,
			title: {
				display: true,
				position: "top",
				text: "Berdasarkan Kabupaten / Kota",
				fontSize: 18,
				fontColor: "#111"
			},
			legend: {
				display: true,
				position: "bottom",
				labels: {
					fontColor: "#333",
					fontSize: 12
				}
			}
		};

		//create Chart class object
		var chart_kota_kabupaten = new Chart(canvas_kota_kabupaten, {
			type: "doughnut",
			data: data_canvas_kota_kabupaten,
			options: options_canvas_kota_kabupaten
		});
	});


	$(function(){
		//get the doughnut chart canvas
		var canvas_kategori = $("#canvas-chart-peserta-terdaftar-by-kategori");

		//doughnut chart data
		var data_canvas_kategori = {
			labels: [
				<?php 
					for($i = 0; $i < count($arr_nama_kategori); $i++){
						echo "'".$arr_nama_kategori[$i]."'".",";
					}
				?>
			],
			datasets: [{
				label: "Peserta Berdasarkan Kategori Program",
				data: [
				<?php
					foreach($count_peserta_terdaftar_by_kategori as $cptbk){
						echo $cptbk['jumlah_peserta_by_kategori'].",";
					}
				?>],
				backgroundColor: [
					'#02c0ce','#2d7bf4','#e3eaef','#f1556c',"#f9bc0b", "#49220b"
				],
				borderColor: [
					'#02c0ce','#2d7bf4','#e3eaef','#f1556c',"#f9bc0b", "#49220b"
				],
				borderWidth: [1,1,1,1,1,1]
			}]
		};

		//options
		var options_canvas_kategori = {
			responsive: true,
			title: {
				display: true,
				position: "top",
				text: "Berdasarkan Kategori Program",
				fontSize: 18,
				fontColor: "#111"
			},
			legend: {
				display: true,
				position: "bottom",
				labels: {
					fontColor: "#333",
					fontSize: 12
				}
			}
		};

		//create Chart class object
		var chart_kategori = new Chart(canvas_kategori, {
			type: "doughnut",
			data: data_canvas_kategori,
			options: options_canvas_kategori
		});
	});


	$(function(){
		//get the doughnut chart canvas
		var canvas_sesi = $("#canvas-chart-peserta-terdaftar-by-sesi");

		//doughnut chart data
		var data_canvas_sesi = {
			labels: [
				<?php 
					for($i = 0; $i < count($arr_nama_sesi); $i++){
						echo "'".$arr_nama_sesi[$i]."'".",";
					}
				?>
			],
			datasets: [{
				label: "Peserta Berdasarkan Sesi Program",
				data: [
				<?php
					foreach($count_peserta_terdaftar_by_sesi as $cptbs){
						echo $cptbs['jumlah_peserta_by_sesi'].",";
					}

				?>],
				backgroundColor: [
					'#02c0ce','#2d7bf4','#e3eaef','#f1556c',"#f9bc0b", "#49220b"
				],
				borderColor: [
					'#02c0ce','#2d7bf4','#e3eaef','#f1556c',"#f9bc0b", "#49220b"
				],
				borderWidth: [1,1,1,1,1,1]
			}]
		};

		//options
		var options_canvas_sesi = {
			responsive: true,
			title: {
				display: true,
				position: "top",
				text: "Berdasarkan Sesi Program",
				fontSize: 18,
				fontColor: "#111"
			},
			legend: {
				display: true,
				position: "bottom",
				labels: {
					fontColor: "#333",
					fontSize: 12
				}
			}
		};

		//create Chart class object
		var chart_sesi = new Chart(canvas_sesi, {
			type: "doughnut",
			data: data_canvas_sesi,
			options: options_canvas_sesi
		});
	});
</script>

<!-- Chart Kuota Tersedia -->
<script>
	$(function(){
  		//get the doughnut chart canvas
  		var canvas_tersedia_by_sesi = $("#canvas-chart-kuota-tersedia-by-sesi");

		//doughnut chart data
		var data_canvas_tersedia_by_sesi = {
			labels: [
				<?php 
					for($i = 0; $i < count($arr_sisa_kuota_by_sesi); $i++){
						echo "'".$arr_sisa_kuota_by_sesi[$i]."'".",";
					}
				?>
			],
			datasets: [
				{
					label: "TeamB Score",
					data: [
						<?php
							foreach($count_kuota_tersedia_berdasarkan_sesi as $cktbs){
								if($cktbs['status_sesi'] == "active"){
									echo ($cktbs['kuota_sesi']-$cktbs['kuota_terdaftar']).',';
								}
							}
						?>,
					],
					backgroundColor: [
						'#02c0ce','#2d7bf4','#e3eaef','#f1556c',"#f9bc0b", "#49220b"
					],
					borderColor: [
						'#02c0ce','#2d7bf4','#e3eaef','#f1556c',"#f9bc0b", "#49220b"
					],
					borderWidth: [1, 1, 1, 1, 1, 1]
				}
			]
		};

		//options
		var options_canvas_tersedia_by_sesi = {
			responsive: true,
			title: {
				display: true,
				position: "top",
				text: "Tuota Tersedia Berdasarkan Sesi",
				fontSize: 18,
				fontColor: "#111"
			},
			legend: {
				display: true,
				position: "bottom",
				labels: {
					fontColor: "#333",
					fontSize: 12
				}
			}
		};

		//create Chart class object
		var chart_tersedia_by_sesi = new Chart(canvas_tersedia_by_sesi, {
			type: "doughnut",
			data: data_canvas_tersedia_by_sesi,
			options: options_canvas_tersedia_by_sesi
		});
	});
</script>

<!-- Chart Aktivasi Peserta -->
<script>
	$(function(){
  		//get the doughnut chart canvas
  		var ctx2 = $("#canvas-chart-aktivasi-peserta");

		//doughnut chart data
		var data2 = {
			labels: ["Peserta Teraktivasi", "Peserta Belum Teraktivasi", "Peserta Di Tolak", "Peserta Reschedule"],
			datasets: [
				{
					label: "TeamB Score",
					data: [<?= $count_confirm.','.$count_waiting.','.$count_rejected.','.$count_reschedule?>],
					backgroundColor: [
						'#02c0ce','#2d7bf4','#e3eaef','#f1556c'
					],
					borderColor: [
						'#02c0ce','#2d7bf4','#e3eaef','#f1556c'
					],
					borderWidth: [5, 1, 1, 1, 1, 1]
				}
			]
		};

		//options
		var options = {

			responsive: true,
			// title: {
			// 	display: true,
			// 	position: "top",
			// 	text: "",
			// 	fontSize: 18,
			// 	fontColor: "#111"
			// },
			legend: {
				display: true,
				position: "top",
				labels: {
					fontColor: "#333",
					fontSize: 12
				}
			}
		};

		//create Chart class object
		var chart2 = new Chart(ctx2, {
			type: "doughnut",
			data: data2,
			options: options
		});
	});
</script>