<script type="text/javascript">
	$(".btn_edit_program").click(function(){
		var id_program = $(this).data("id_program");
		$.ajax({
            type : "POST",
            url: "<?= base_url('Get/getProgramById')?>",
            dataType : "JSON",
            data : {id_program:id_program},
            success: function(data){
    			$('.edit-program').modal('show');

                $('[name="edit_id_program"]').val(data.id_program);
                $('[name="edit_id_user"]').val(data.id_user);
                $('[name="edit_tipe_program"]').val(data.tipe_program);
                $('[name="edit_nama_program"]').val(data.nama_program);
                $('[name="edit_deskripsi_program"]').val(data.deskripsi_program);
                $('#edit_program_start_date').datepicker('setDate', new Date(data.tanggal_mulai_program));
                $('#edit_program_end_date').datepicker('setDate', new Date(data.tanggal_berakhir_program));
                $('[name="edit_lokasi_program"]').val(data.lokasi_program);
                $('[name="lokasi_kecamatan_program"]').val(data.kecamatan_program);
                $('[name="edit_jenis_vaksin_program"]').val(data.jenis_vaksin_program);
                $('[name="edit_batch_vaksin_program"]').val(data.no_batch_program);
                $('[name="edit_dosis_vaksin_program"]').val(data.dosis_vaksin_program);
                $('[name="edit_target_peserta_program"]').val(data.target_peserta_program);
                $('input[name="edit_target_klien_program"]').tagsinput( "add", data.target_klien_program );
                $('[name="edit_goals_program"]').val(data.goals_program);
                $('[name="edit_anggaran_program"]').val(data.anggaran_program);
            }
        });
	});
    $("#btn_tambah_jenis_vaksin").click(function(){
        var id_program = $(this).data("id_program");
        var link_url_jenis_vaksin = $(this).data("link_url_jenis_vaksin");

        $('[name="id_program_jenis_vaksin"]').val(id_program);
        $('[name="link_url_jenis_vaksin"]').val(link_url_jenis_vaksin);
        $('.tambah-jenis-vaksin-program').modal('show');
    });

    $("#btn_edit_jenis_vaksin").click(function(){
        var id_program = $(this).data("id_program");
        var link_url_jenis_vaksin = $(this).data("link_url_jenis_vaksin");
        var jenis_vaksin = $(this).data("jenis_vaksin");
        var batch_vaksin = $(this).data("batch_vaksin");
        var dosis_vaksin = $(this).data("dosis_vaksin");

        $('[name="id_program_jenis_vaksin"]').val(id_program);
        $('[name="link_url_jenis_vaksin"]').val(link_url_jenis_vaksin);
        $('[name="edit_jenis_vaksin_program"]').val(jenis_vaksin);
        $('[name="edit_batch_vaksin_program"]').val(batch_vaksin);
        $('[name="edit_dosis_vaksin_program"]').val(dosis_vaksin);
        $('.edit-jenis-vaksin-program').modal('show');
    });


    $("#btn_export_pdf_peserta").click(function(){
       checkExport("pdf"); 
    });
    
    $("#btn_export_excel_peserta").click(function(){
       checkExport("excel"); 
    });

    function checkExport(exportto){
        var id_program = $(this).data("id_program");
        var pilih_status_export = $("#pilih_status_export").val();
        
        var pilih_tanggal_export_start = $("#pilih_tanggal_export_start").val();
        var pilih_tanggal_export_end = $("#pilih_tanggal_export_end").val();

        var tanggal_start = new Date(pilih_tanggal_export_start);
        var dd = String(tanggal_start.getDate()).padStart(2, '0');
        var mm = String(tanggal_start.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = tanggal_start.getFullYear();
        tanggal_start = yyyy + '-' + mm + '-' + dd;

        var tanggal_end = new Date(pilih_tanggal_export_end);
        var dd = String(tanggal_end.getDate()).padStart(2, '0');
        var mm = String(tanggal_end.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy = tanggal_end.getFullYear();
        tanggal_end = yyyy + '-' + mm + '-' + dd;

        if(tanggal_end < tanggal_start){
            alert('Tanggal Program tidak valid');
        }else{
            if(exportto == "pdf"){
                // document.getElementById('form_filter_peserta').action = "<?= base_url('ExportPDF/dataPeserta')?>"; //Will set it
                document.getElementById('form_filter_peserta').action = "<?= base_url('ExportOffice/PDF')?>"; //Will set it
                $("#form_filter_peserta").submit();
            }else if(exportto == "excel"){
                document.getElementById('form_filter_peserta').action = "<?= base_url('ExportOffice/Excel')?>"; //Will set it
                $("#form_filter_peserta").submit();
            }
        }
    }

	$(".btn_hapus_program").click(function(){
		var id_program = $(this).data("id_program");
        $('[name="id_program"]').val(id_program);
        $('.hapus-program').modal('show');
    });
    
    $(".btn_decline_aktivasi_peserta").click(function(){
        var id_koneksi = $(this).data("id_koneksi");
        var link_url = $(this).data("link_url");
        $('[name="id_koneksi"]').val(id_koneksi);
        $('[name="link_url"]').val(link_url);
        $('.decline-peserta-program').modal('show');
    });

    $("#btn_link_formulir_berakhir").click(function(){
        $('.link-formulir-berakhir').modal('show');
    });

    $("#btn_submit_form_pengaturan_sesi").click(function(){
        var tanggal_jadwal_program = $("#tanggal_jadwal_program").val();
        var waktu_jadwal_program_mulai = $("#waktu_jadwal_program_mulai").val();
        var waktu_jadwal_program_berakhir = $("#waktu_jadwal_program_berakhir").val();
        var kuota_jadwal_program = $("#kuota_jadwal_program").val();
        
        var ConvertWaktuMulai = Date.parse(tanggal_jadwal_program+' '+waktu_jadwal_program_mulai);
        var ConvertWaktuBerakhir =  Date.parse(tanggal_jadwal_program+' '+waktu_jadwal_program_berakhir);

        if(ConvertWaktuMulai >= ConvertWaktuBerakhir){
            alert("Format waktu tidak sesuai! Periksa kembali !");
        }else{
            if(kuota_jadwal_program == "" || kuota_jadwal_program == 0){
                alert("Masukkan Kuota");
            }else{
                $("#form_pengaturan_sesi_program").submit();
            }
        }
    });

    $(".btn_edit_sesi").click(function(){
        var id_sesi = $(this).data("id_sesi");
        $.ajax({
            type : "POST",
            url: "<?= base_url('Get/getSesiById')?>",
            dataType : "JSON",
            data : {id_sesi:id_sesi},
            success: function(data){
                $('[name="id_sesi"]').val(data.id_sesi);
                $('[name="nama_jadwal_sesi_program"]').val(data.nama_sesi);

                $('[name="edit_tanggal_sesi"]').val(data.tanggal_sesi);
                $('[name="edit_waktu_mulai_sesi"]').val(data.waktu_mulai_sesi);
                $('[name="edit_waktu_berakhir_sesi"]').val(data.waktu_berakhir_sesi);

                $('[name="tanggal_jadwal_program"]').val(data.tanggal_sesi);
                $('[name="edit_kuota"]').val(data.kuota_sesi);
                $('#waktu_jadwal_program_mulai').timepicker('setTime', data.waktu_mulai_sesi);
                $('#waktu_jadwal_program_berakhir').timepicker('setTime', data.waktu_berakhir_sesi);
                $('[name="kuota_jadwal_program"]').val(data.kuota_sesi);
				$('#btn_submit_form_pengaturan_sesi').text('Ubah');
				$('#btn_reset_form_pengaturan_sesi').text('Batal');
				$('#ubah_form_pengaturan_sesi').text('Ubah Jadwal Program');
				$('#ubah_form_pengaturan_sesi').addClass('text');
				$('#div_ubah_sesi_program').addClass('bg-light py-3 rounded border border-danger');
                // $('[name="id_sesi"]').val(data.id_sesi);
                // // $('#tanggal_jadwal_program').datepicker('setDate', new Date(data.tanggal_sesi));
                // $('[name="nama_jadwal_sesi_program"]').val(data.nama_sesi);
                // $('[name="tanggal_jadwal_program"]').val(data.tanggal_sesi);
                // $('[name="edit_kuota"]').val(data.kuota_sesi);
                // $('#waktu_jadwal_program_mulai').timepicker('setTime', data.waktu_mulai_sesi);
                // $('#waktu_jadwal_program_berakhir').timepicker('setTime', data.waktu_berakhir_sesi);
                // $('[name="kuota_jadwal_program"]').val(data.kuota_sesi);
            }
        });
    });


    $(".btn_reset_input_sesi").click(function(){
        $('[name="id_sesi"]').val("");
        $('#tanggal_jadwal_program').datepicker('setDate', new Date());
        $('[name="waktu_jadwal_program_mulai"]').val("");
        $('[name="waktu_jadwal_program_berakhir"]').val("");
        $('[name="kuota_jadwal_program"]').val("");
        $('[name="edit_kuota"]').val("");
    });

    $(".btn_delete_sesi").click(function(){
        var id_sesi = $(this).data("id_sesi");
        var link_url = $(this).data("link_url");
        $('[name="id_sesi_hapus"]').val(id_sesi);
        $('[name="link_url_hapus_sesi"]').val(link_url);
        $('.hapus-sesi-program').modal('show');
    });

    $(".btn_reschedule_peserta").click(function(){
        var id_koneksi = $(this).data("id_koneksi");
        var id_peserta = $(this).data("id_peserta");
        var tanggal_terdaftar = $(this).data("tanggal_terdaftar");
        var link_url = $(this).data("link_url");
        var tanggal_mulai_program = $(this).data("tanggal_mulai_program");
        var sesi_terdaftar = $(this).data("sesi_terdaftar");

        $('[name="id_koneksi"]').val(id_koneksi);
        $('[name="id_peserta"]').val(id_peserta);
        $('[name="tanggal_terdaftar"]').val(tanggal_terdaftar);
        $('[name="link_url"]').val(link_url);
        $('[name="reschedule_sesi_program"]').val(sesi_terdaftar);
        $('[name="reschedule_sesi_program_input"]').val(sesi_terdaftar);

        var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
        var firstDate = new Date();
        var secondDate = new Date(tanggal_mulai_program);
        var diffDays = Math.round(Math.round((secondDate.getTime() - firstDate.getTime()) / (oneDay)));
        if(diffDays < 3){
            $('.reschedule-berakhir').modal('show');
        }else{
            $('.reschedule-peserta-program').modal('show');
        }
    });
    function cekReschedule(){
        var reschedule_sesi_program = $("#reschedule_sesi_program").val();
        var new_reschedule_sesi_program = $("#new_reschedule_sesi_program").val();
        if(reschedule_sesi_program == new_reschedule_sesi_program){
            alert("Sesi reschedule tidak boleh sama dengan yang sebelumnya");
            $("#new_reschedule_sesi_program").val("");
        }
    };
</script>

<script type="text/javascript">
    function buttonShareWA($url){
        window.open("whatsapp://send?text=Hai, ayo ikut program vaksin dengan klik link berikut ini ya!!! "+$url+" Buruan daftar sebelum kuota habis !");
    }

    function buttonShareCopyLink($url){
        navigator.clipboard.writeText($url);
    }
</script>

<script type="text/javascript">
    $('#waktu_jadwal_program_mulai').timepicker({
        uiLibrary: 'bootstrap4'
    });
  
    $('#waktu_jadwal_program_berakhir').timepicker({
        uiLibrary: 'bootstrap4'
    });
</script>
