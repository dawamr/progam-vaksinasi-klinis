<script src="<?=base_url()?>assets/plugins/chartjs/chart.min.js"></script>

<!-- MODAL PIE CHART PESERTA TERDAFTAR-->
<div class="modal fade chart-peserta-terdaftar" tabindex="-1" role="dialog" aria-labelledby="pieChartNama" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="pieChartNama">Informasi Kuota</h4>
            </div>
            <div class="modal-body d-flex justify-content-center">
                <div class="col-md-6">
                    <div class="chart-container">
                        <div class="doughnut-chart-container">
                            <canvas id="canvas-chart-peserta-terdaftar" height="500" width="500"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-group text-right m-b-0">
                    <button type="button" class="btn btn-light waves-effect m-l-5" data-dismiss="modal">Cancel</button>
                </div>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- MODAL PIE CHART KUOTA TERSEDIA-->
<div class="modal fade chart-kuota-tersedia" tabindex="-1" role="dialog" aria-labelledby="pieChartNama" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="pieChartNama">Informasi Kuota</h4>
            </div>
            <div class="modal-body d-flex justify-content-center">
                <div class="col-md-6">
                    <div class="chart-container">
                        <div class="doughnut-chart-container">
                            <canvas id="canvas-chart-kuota-tersedia" height="500" width="500"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-group text-right m-b-0">
                    <button type="button" class="btn btn-light waves-effect m-l-5" data-dismiss="modal">Cancel</button>
                </div>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- MODAL PIE CHART AKTIVASI PESERTA-->
<div class="modal fade chart-aktivasi-peserta" tabindex="-1" role="dialog" aria-labelledby="pieChartNama" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="pieChartNama">Aktivasi Peserta</h4>
            </div>
            <div class="modal-body d-flex justify-content-center">
                <div class="col-md-6">
                    <div class="chart-container">
                        <div class="doughnut-chart-container">
                            <canvas id="canvas-chart-aktivasi-peserta" height="500" width="500"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-group text-right m-b-0">
                    <button type="button" class="btn btn-light waves-effect m-l-5" data-dismiss="modal">Cancel</button>
                </div>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<?php
$id_program = $data_program['id_program'];

$sql_search_data_peserta = "
        SELECT a.*, b.*, c.* FROM table_sesi_program a
        LEFT JOIN table_koneksi_program b ON a.id_sesi = b.id_sesi
        LEFT JOIN table_peserta c ON b.id_peserta = c.id_peserta
        WHERE a.id_program = '$id_program';
    ";
$data_peserta_program = $this->db->query($sql_search_data_peserta)->result_array();

$count_reschedule = 0;
$count_waiting = 0;
$count_rejected = 0;
$count_confirm = 0;
foreach ($data_peserta_program as $dpp) {
	if($dpp['status_koneksi'] == "Reschedule"){
		$count_reschedule++;
	}elseif($dpp['status_koneksi'] == "Rejected"){
		$count_rejected++;
	}elseif($dpp['status_koneksi'] == "Confirmed"){
		$count_confirm++;
	}elseif($dpp['status_koneksi'] == "Waiting"){
		$count_waiting++;
	}
}
?>


<!-- Chart Peserta Terdaftar -->
<script>
	$(function(){
		//get the doughnut chart canvas
		var canvas_1 = $("#canvas-chart-peserta-terdaftar");

		//doughnut chart data
		var data_canvas_1 = {
			labels: ["match1", "match2", "match3", "match4", "match5", "match6"],
			datasets: [{
				label: "TeamB Score",
				data: [20, 35, 40, 60, 50, 5],
				backgroundColor: [
					'#02c0ce','#2d7bf4','#e3eaef','#f1556c',"#f9bc0b", "#49220b"
				],
				borderColor: [
					'#02c0ce','#2d7bf4','#e3eaef','#f1556c',"#f9bc0b", "#49220b"
				],
				borderWidth: [1, 1, 1, 1, 1, 1]
			}]
		};

		//options
		var options_canvas_1 = {
			responsive: true,
			title: {
				display: true,
				position: "top",
				text: "",
				fontSize: 18,
				fontColor: "#111"
			},
			legend: {
				display: true,
				position: "bottom",
				labels: {
					fontColor: "#333",
					fontSize: 12
				}
			}
		};

		//create Chart class object
		var chart2 = new Chart(canvas_1, {
			type: "doughnut",
			data: data_canvas_1,
			options: options_canvas_1
		});
	});
</script>

<!-- Chart Kuota Tersedia -->
<script>
	$(function(){
  		//get the doughnut chart canvas
  		var canvas_2 = $("#canvas-chart-kuota-tersedia");

		//doughnut chart data
		var data_canvas_2 = {
			labels: ["", "match2", "match3", "match4", "match5", "match6"],
			datasets: [
				{
					label: "TeamB Score",
					data: [20, 35, 40, 60, 50, 5],
					backgroundColor: [
						'#02c0ce','#2d7bf4','#e3eaef','#f1556c',"#f9bc0b", "#49220b"
					],
					borderColor: [
						'#02c0ce','#2d7bf4','#e3eaef','#f1556c',"#f9bc0b", "#49220b"
					],
					borderWidth: [1, 1, 1, 1, 1, 1]
				}
			]
		};

		//options
		var options_canvas_2 = {
			responsive: true,
			title: {
				display: true,
				position: "top",
				text: "",
				fontSize: 18,
				fontColor: "#111"
			},
			legend: {
				display: true,
				position: "bottom",
				labels: {
					fontColor: "#333",
					fontSize: 12
				}
			}
		};

		//create Chart class object
		var chart2 = new Chart(canvas_2, {
			type: "doughnut",
			data: data_canvas_2,
			options: options_canvas_2
		});
	});
</script>

<!-- Chart Aktivasi Peserta -->
<script>
	$(function(){
  		//get the doughnut chart canvas
  		var ctx2 = $("#canvas-chart-aktivasi-peserta");

		//doughnut chart data
		var data2 = {
			labels: ["Peserta Teraktivasi", "Peserta Belum Teraktivasi", "Peserta Di Tolak", "Peserta Reschedule"],
			datasets: [
				{
					label: "TeamB Score",
					data: [<?= $count_confirm.','.$count_waiting.','.$count_rejected.','.$count_reschedule?>],
					backgroundColor: [
						'#02c0ce','#2d7bf4','#e3eaef','#f1556c'
					],
					borderColor: [
						'#02c0ce','#2d7bf4','#e3eaef','#f1556c'
					],
					borderWidth: [5, 1, 1, 1, 1, 1]
				}
			]
		};

		//options
		var options = {

			responsive: true,
			title: {
				display: true,
				position: "top",
				text: "",
				fontSize: 18,
				fontColor: "#111"
			},
			legend: {
				display: true,
				position: "bottom",
				labels: {
					fontColor: "#333",
					fontSize: 12
				}
			}
		};

		//create Chart class object
		var chart2 = new Chart(ctx2, {
			type: "doughnut",
			data: data2,
			options: options
		});
	});
</script>