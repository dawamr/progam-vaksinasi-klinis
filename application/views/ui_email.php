<?php 
function penyebut($nilai) {
    $nilai = abs($nilai);
    $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
    $temp = "";
    if ($nilai < 12) {
        $temp = " ". $huruf[$nilai];
    } else if ($nilai <20) {
        $temp = penyebut($nilai - 10). " Belas";
    } else if ($nilai < 100) {
        $temp = penyebut($nilai/10)." Puluh". penyebut($nilai % 10);
    } else if ($nilai < 200) {
        $temp = " Seratus" . penyebut($nilai - 100);
    } else if ($nilai < 1000) {
        $temp = penyebut($nilai/100) . " Ratus" . penyebut($nilai % 100);
    } else if ($nilai < 2000) {
        $temp = " Seribu" . penyebut($nilai - 1000);
    } else if ($nilai < 1000000) {
        $temp = penyebut($nilai/1000) . " Ribu" . penyebut($nilai % 1000);
    } else if ($nilai < 1000000000) {
        $temp = penyebut($nilai/1000000) . " Juta" . penyebut($nilai % 1000000);
    } else if ($nilai < 1000000000000) {
        $temp = penyebut($nilai/1000000000) . " Milyar" . penyebut(fmod($nilai,1000000000));
    } else if ($nilai < 1000000000000000) {
        $temp = penyebut($nilai/1000000000000) . " Trilyun" . penyebut(fmod($nilai,1000000000000));
    }     
    return $temp;
}

function terbilang($nilai) {
    if($nilai<0) {
        $hasil = "minus ". trim(penyebut($nilai))." Rupiah";
    } else {
        $hasil = trim(penyebut($nilai))." Rupiah";
    }
    return $hasil;
}
?>
<div style="width:100%;max-width:600px;min-width:352px;font-family:Lato,arial;font-size:14px;line-height:24px;color:#595959;box-sizing:border-box;margin:0 auto;border:solid 1px #ddd">
            <div style="text-align:right;padding:30px 15px;border-bottom:solid 2px #0bafab">
                <img style="width:200px;height:48px" src="https://dash.klinis.id/assets/images/logo.png" alt="Logo Klinis" title="Logo Klinis">
            </div>
            <div style="margin:32px 8px 40px 16px">
                <h3 style="margin-bottom:24px;line-height:32px;font-size:22px;font-weight:800;text-align:center;color:#47d3cd">Pembayaran Berhasil</h3>
                <p style="font-style:normal;font-weight:normal;font-size:14px;line-height:24px;color:#595959">Yth. Bapak/Ibu *NAMA CUSTOMER*,
                <br><br>
                Melalui email ini kami informasikan bahwa pembayaran Anda telah <strong style="font-weight:700;color:#47d3cd">berhasil</strong>.
                <br>Rincian pembayaran dapat dilihat di bawah ini:</p>

                <div style="margin:24px 0px;padding:37px 10px;border:1px solid #d9d9d9;box-sizing:border-box;max-width:568px">
                    <div style="display:table;width:100%;margin-bottom:16px">
                    <div style="width:90%">
                    
                    <img style="width:12.5em;height:auto;display:table-cell" src="https://dash.klinis.id/assets/images/logo.png" alt="Logo Klinis" title="Logo Klinis" class="CToWUd">
                    </div>
                    <div style="font-size:100%;font-weight:bold;font-stretch:normal;font-style:normal;line-height:normal;letter-spacing:normal;color:#9b9b9b;display:table-cell;text-align:right">INVOICE</div>
                    </div>
                    <div style="display:table;width:100%;margin-top:10px">
                        <div style="margin:0px 99px 20px 0px;float:left;display:block;min-height:60px">
                            <div style="font-size:14px;line-height:17px;color:#424242;font-weight:600">PT. Kreasi Layanan Medis</div>
                            <div style="font-size:12px;line-height:135%;color:#7f7f7f;font-weight:normal;width:250px;height:44px;margin-top:5px">
                                <div>
                                    Gedung CNI Creative Center (C3), Lt.2.<br>
                                    Jalan Puri Indah Blok O2 No.1-3<br>
                                    Kembangan, Jakarta Barat 11610.<br>
                                </div>
                                <div>NPWP: *NOMOR NPWP PERUSAHAAN*</div>
                            </div>
                            <div style="margin-bottom:5px">&nbsp;</div>
                        </div>
                        <div style="float:left;display:block;margin-top:0px;min-height:60px">
                        <div style="display:table-cell;vertical-align:top;font-size:12px;line-height:17px">
                            <table style="font-family:Lato,arial;border-collapse:collapse;border-spacing:0;color:#7f7f7f;margin-left:auto;color:#7f7f7f">
                                <tbody>
                                <tr>
                                    <td style="font-family:Lato,arial">No. Invoice</td>
                                    <td>:</td>
                                    <td style="font-family:Lato,arial;color:#47d3cd;font-weight:600;text-align:left;text-align:-webkit-left">
                                        <a href="https://dash.klinis.id/InvoicePDF/eInvoice" target="_blank" style="color:#47d3cd">*NOMOR INVOICE*</a>
                                        <!-- <a href="http://localhost/dash/InvoicePDF/eInvoice" target="_blank" style="color:#47d3cd">*NOMOR INVOICE*</a> -->
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-family:Lato,arial">Tanggal</td>
                                    <td>:</td>
                                    <td style="font-family:Lato,arial;color:#424242;font-weight:600;text-align:left;text-align:-webkit-left">*TANGGAL INVOICE*</td>
                                </tr>
                                <tr>
                                    <td style="font-family:Lato,arial">Status</td>
                                    <td>:</td>
                                    <td style="font-family:Lato,arial;color:#424242;font-weight:600;text-align:left;text-align:-webkit-left">*STATUS PEMBAYARAN*</td>
                                </tr>
                                <tr>
                                    <td style="font-family:Lato,arial">Pembayaran</td>
                                    <td>:</td>
                                    <td style="font-family:Lato,arial;color:#424242;font-weight:600;text-align:left;text-align:-webkit-left">*METODE PEMBAYARAN*</td>
                                </tr>
                                </tbody>
                            </table>
                            </div>
                        </div>
                        <div style="clear:both">
                    </div>
                    <div style="display:table;width:100%;margin:20px 0px 20px 2px;font-size:0.87em;line-height:1.1em;font-family:Lato,arial">
                        <div style="margin-bottom:5px;font-weight:normal;color:#7f7f7f;font-family:Lato,arial">Kepada :</div>
                        <div style="margin-botom:5px;font-weight:600;color:#424242;font-family:Lato,arial">*NAMA CUSTOMER*</div>
                        <div style="margin-bottom:5px;font-weight:600;color:#424242;font-family:Lato,arial">*PHONE CUSTUMER*</div>
                        <div style="font-weight:600;color:#424242;text-decoration:none;font-family:Lato,arial"><a style="color:#424242;text-decoration:none">*EMAIL CUSTOMER*</a></div>
                    </div>
                    <div style="box-sizing:border-box;font-size:inherit;display:table;width:100%">
                        <table style="border-radius:6px;border-spacing:0px;border:solid 1px #ececec;width:100%;font-size:0.87em;line-height:1.3em;font-family:lato,arial">
                            <thead>
                                <tr style="font-family:lato,arial;color:#595959;font-weight:600;font-style:normal;background-color:#ececec;font-weight:600;font-size:0.87em;line-height:1em;height:40px">
                                    <td style="font-family:lato,arial;color:#595959;font-weight:600;font-style:normal;background-color:#ececec;font-weight:600;height:40px;padding-left:20px">Nama Produk</td>
                                    <td style="font-family:lato,arial;color:#595959;font-weight:600;font-style:normal;background-color:#ececec;font-weight:600;height:40px;padding:0px 20px 0px 0px;text-align:right">Qty</td>
                                    <td style="font-family:lato,arial;color:#595959;font-weight:600;font-style:normal;background-color:#ececec;font-weight:600;height:40px;padding:0px 10px 0px 10px">Harga<br>sebelum Pajak</td>
                                    <td style="font-family:lato,arial;color:#595959;font-weight:600;font-style:normal;background-color:#ececec;font-weight:600;height:40px;padding:0px 20px 0px 0px;text-align:right">Pajak 10%</td>
                                    <td style="font-family:lato,arial;color:#595959;font-weight:600;font-style:normal;background-color:#ececec;font-weight:600;height:40px;text-align:right;padding-right:20px">Harga Nett</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $total_bayar =23213240;
                                $harga_nett = 25000;
                            
                                for($i = 0; $i < 3; $i++){ ?>
                                    <tr style="color:#424242;font-family:lato,arial">
                                        <td style="font-family:lato,arial;border-bottom:solid 1px #ececec;padding:20px 0px 20px 20px">*NAMA PRODUK*</td>
                                        <td style="font-family:lato,arial;border-bottom:solid 1px #ececec;padding:20px 20px 20px 0px;text-align:right">*QTY*</td>
                                        <td style="font-family:lato,arial;border-bottom:solid 1px #ececec;padding:20px 20px 20px 0px;text-align:right">*HARGA SEBELUM PAJAK*</td>
                                        <td style="font-family:lato,arial;border-bottom:solid 1px #ececec;padding:20px 20px 20px 0px;text-align:right">*PAJAK*</td>
                                        <td style="font-family:lato,arial;border-bottom:solid 1px #ececec;padding:20px 20px 20px 0px;text-align:right"><?= $harga_nett?></td>
                                    </tr>
                                <?php 
                                $total_bayar += $harga_nett;
                                }?>
                                <tr>
                                    <td colspan="3" style="font-family:lato,arial;padding:20px 0px 20px 20px;color:#7f7f7f;font-style:italic">*<?= terbilang($total_bayar)?>*</td>
                                    <td colspan="2" style="font-family:lato,arial;padding:20px 20px 20px 0px;color:#47d3cd;text-align:right;font-weight:600">Total Bayar : <?= $total_bayar?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr style="box-sizing:border-box;margin-top:20px;margin-bottom:20px;border:1px dashed #ececec">
                    <div style="font-family:lato,arial;padding:20px 13.87px;border-radius:24px;border:solid 1px #ececec">
                        <div style="font-size:12px;line-height:157%;color:#595959">
                            •   Bila memiliki pertanyaan silakan hubungi kami via email di <strong style="color:#47d3cd">
                            <a href="mailto:support@klinis.id" target="_blank" style="color:#47d3cd">support@klinis.id</a></strong> atau di nomor telepon <strong style="color:#47d3cd">(+62) 85211223301</strong></div>
                    </div>
                    <p style="font-family:lato,arial;text-align:center;font-size:10px;line-height:135%;color:#7f7f7f">Dokumen ini dicetak secara digital pada <strong>*TANGGAL CETAK* *WAKTU CETAK*</strong></p>
                </div>
                <p style="font-family:lato,arial">Email ini dapat disimpan sebagai bukti pembayaran Anda. 
                <br>
                Terima kasih telah menggunakan KLINIS untuk menyempurnakan perlindungan kesehatan Anda dan keluarga.</p>

                <p style="font-family:lato,arial;margin-top:24px">Salam Hangat,<br>
                <a style="font-family:lato,arial;color:#47d3cd;text-decoration:none" href="*URL LINK PROTEKSI*" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.alodokter.com/proteksi-alodokter&amp;source=gmail&amp;ust=1633682583651000&amp;usg=AFQjCNGSdMItkDm4Og3GomlyaxbZ7vdF6Q">Medicare KLINIS</a></p>
            </div>
            <div>
                <div style="background:#47d3cd;
                            color:#fff;
                            overflow:hidden;
                            padding:20px 0 0 0;
                            background:#3670d3;
                            box-sizing:border-box;
                            background:-moz-linear-gradient(top,#3670d3 0%,#2164d1 56%,#283d9b 100%);
                            background:-webkit-linear-gradient(top,#3670d3 0%,#2164d1 56%,#283d9b 100%);
                            background:linear-gradient(to bottom,#93fffa 0%,#47d3cd 56%,#1a9993 100%);
                            ">
                    <div style="display:flex;width:290px;margin:0 auto">
                        <div style="margin-top:auto;height:160px">
                            <img style="width:100px;margin-top:auto" src="https://ci5.googleusercontent.com/proxy/gGtVzTIkK2DzfAs3KJWdT60ZHGJxgGjCB-wkoBsqVh-2PiNyqjF6pZYGfrvOlfQEdbUm1Nl35KFxJyq_ZMtZde9GHALAj5DPndfffvIuhfSz7fltgT6G_FoFkXMd8HEtK1BlIDCJw1qe9P60z9kptQ=s0-d-e1-ft#https://res.cloudinary.com/dk0z4ums3/image/upload/w_100,f_auto/v1545967189/unnamed_z0hsw2.png" class="CToWUd">
                        </div>
                        <div style="padding:0 0 0 10px;text-align:center;display:flex">
                            <div style="margin-bottom:10px">
                                <h3 style="margin:0 0 5px 0;letter-spacing:2px">KLINIS</h3>
                                <h4 style="font-weight:900;font-size:24px;margin:0 0 10px 0;line-height:26px">Chat Dokter<br>Gratis!</h4>
                                <span style="display:block;margin:0 0 10px 0;font-size:14px">Download Sekarang</span>
                                <div>
                                    <a href="*URL PLAY STORE KLINIS*">
                                    <img style="width:80px" src="https://ci3.googleusercontent.com/proxy/Ry-ehWAcTB3JAoGG6YOrbhd4OA2hw-wok9iJqQAzjv9vBRb-CpeE9m24Dsqcoo7lBCfwREJEjMXBTMnhc6_CGJ47KDVoWzF0t18ZEUWjU6XTXQLZal8zL6cTjrTNJKu8R2HbceJnZ4sLuyn4eCabcyeSxccF-ZK3=s0-d-e1-ft#https://res.cloudinary.com/dk0z4ums3/image/upload/w_80,f_auto/v1545112837/email_template/Appstore.svg" class="CToWUd">
                                    </a>
                                    <a href="*URL APPS STORE KLINIS*">
                                    <img style="width:80px" src="https://ci6.googleusercontent.com/proxy/RlBRJQLpToutct-5Tj_AqHEJHOyaGngeRgM0clDCFhkatUEws_PTiW1TjYQXM6VFudcdvSs0djWYRIVL30A_KHixZ0RW1aPI22jylE4hI9t71dCP8_SeVjLjFUTHQK-SVINZnxOL5wUgMKXC2AodLy3iLJqEUyCQeQ=s0-d-e1-ft#https://res.cloudinary.com/dk0z4ums3/image/upload/w_80,f_auto/v1545112839/email_template/Playstore.svg" class="CToWUd">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="background:#47d3cd;color:#fff;overflow:hidden;
                
                background:-moz-linear-gradient(top,#3670d3 0%,#2164d1 56%,#283d9b 100%);
                            background:-webkit-linear-gradient(top,#3670d3 0%,#2164d1 56%,#283d9b 100%);
                            background:linear-gradient(to bottom,#37b1ab 0%,#47d3cd 56%,#6be8e2 100%);
                ">
                    <div style="text-align:center;margin:16px 0px">
                            <h3 style="margin-bottom:4px;font-size:14px;line-height:22px;font-weight:bold">PT. KREASI LAYANAN MEDIS</h3>
                            <p style="font-size:12px;line-height:20px;margin:0px">
                            Gedung CNI Creative Center (C3), Lt.2.
                            <br>
                                <a href="https://g.page/klinis?share" style="color:white;text-decoration:none;font-size:13px" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://maps.google.com/?q%3DJL.%2BProf.%2BDr.%2BSatrio%2BNo.%2B5%2BBlok%2BC4%2BKuningan%2B%25E2%2580%2593%2BSetiabudi,%2BJakarta%2B12950,%2BIndonesia%26entry%3Dgmail%26source%3Dg&amp;source=gmail&amp;ust=1633682583652000&amp;usg=AFQjCNHPdL55k42dJLtgDfzfWIt_PheDCQ">
                                    Jalan Puri Indah Blok O2 No.1-3
                                    <br>
                                    Kembangan, Jakarta Barat 11610.
                                </a>
                            </p>
                    </div>
                    <div style="text-align:center;margin-bottom:16px">
                        <h3 style="margin-bottom:4px;font-size:14px;line-height:22px;font-weight:bold">Layanan Costumer Care</h3>
                        <p style="margin:0px;font-size:12px;line-height:20px"><a style="color:#fff;text-decoration:none"> +(021)-58358309 </a><br><a style="color:#fff;text-decoration:none">support@klinis.id</a></p>
                    </div>
                </div>
            </div>
            <div style="padding:36px 0px 12px 0px">
                <div style="text-align:center;padding:0;margin-bottom:16px">
                    <a href="*URL FACEBOOK PERUSAHAAN*" style="display:inline-block;width:10%;max-width:40px;margin:0 11px" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.facebook.com/alodokter&amp;source=gmail&amp;ust=1633682583652000&amp;usg=AFQjCNFHUdaziqZz1SjIskREKW1uiB_S3Q">
                    <img style="width:100%" src="https://ci5.googleusercontent.com/proxy/xjU3lBT25RJsv4AJlbNWsbdh8l4MXB_H8wduPFEDxWEoPX-o_7TwVefULzplEC36CeE4dizmX-kY45iyz74ChLb2NUmoFtF-yq9WU2M04PbxyXSCNrp0eDlrUtis3qBnaacgqY0Ibe0u9RGxDKOq5ORBx7SS8ZLA=s0-d-e1-ft#https://res.cloudinary.com/dk0z4ums3/image/upload/f_auto/v1545112837/email_template/icon_facebook.svg" class="CToWUd">
                    </a>
                    <a href="*URL INSTAGRAM PERUSAHAAN*" style="display:inline-block;width:10%;max-width:40px;margin:0 11px" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.instagram.com/alodokter_id/&amp;source=gmail&amp;ust=1633682583652000&amp;usg=AFQjCNH5KyjYwyG5gyJsK--aA0wZQ-1fTg">
                    <img style="width:100%" src="https://ci6.googleusercontent.com/proxy/NfCC6qAllxgWIZIL-3j15fGJzFsIVJ0pgTwCRdOX4ET_ZAmyr9E2oUgQnx8k9cFj3jG3ppBMFeyha35U-JAnPar0Rn2vQyix0nFz4jDPKkAhwFNOUemm0beDYIUoOSXpDiWVphkucYm2UmDSt6G05smb7pzyl-wvlw=s0-d-e1-ft#https://res.cloudinary.com/dk0z4ums3/image/upload/f_auto/v1545112837/email_template/icon_instagram.svg" class="CToWUd">
                    </a>
                    <a href="*URL TWITTER PERUSAHAAN*" style="display:inline-block;width:10%;max-width:40px;margin:0 11px" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://twitter.com/alodokter&amp;source=gmail&amp;ust=1633682583652000&amp;usg=AFQjCNGVmad12WbCjzth124f5SM_NuWuJg">
                    <img style="width:100%" src="https://ci4.googleusercontent.com/proxy/SatyMj3ZTbNWW7INYphXh7JnhgogQ1u7kPjhqWhUNaSrE2st1aOQA3fJhN6PKQTCWuNaJBBBmE1ZmZ8Tdl8cu3EozeIsjuCz-6judUqeyJpUuhVlcVtoXE9KUz6W0_38bjkSaO_uy68eWC9CsL2LsRex7aVrono=s0-d-e1-ft#https://res.cloudinary.com/dk0z4ums3/image/upload/f_auto/v1545112837/email_template/icon_twitter.svg" class="CToWUd">
                    </a>
                    <a href="*URL YOUTUBE PERUSAHAAN*" style="display:inline-block;width:10%;max-width:40px;margin:0 11px" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.youtube.com/channel/UCVlQXBPDCbz5YES_gBpC1rg&amp;source=gmail&amp;ust=1633682583652000&amp;usg=AFQjCNGC2F_4LYE4AZoE-Va5YlCOjO_xPw">
                    <img style="width:100%" src="https://ci4.googleusercontent.com/proxy/wqkL-YBfl2_P1pdVD9yuTM9wriMsmyujiqqMMbOR58DuW3iipEE3dqMbjeRPzwMEVmRfeKWbeAmYEwvOuXcmIKQtj9m90CRJVNHYmV2cdooxPIhTkqxak7VhdLiu-sMkbzKUHS65rMNH_XDSSzH65AOol1iCiiM=s0-d-e1-ft#https://res.cloudinary.com/dk0z4ums3/image/upload/f_auto/v1545112838/email_template/icon_youtube.svg" class="CToWUd">
                    </a>
                    <a href="*URL WEBSITE PERUSAHAAN*" style="display:inline-block;width:10%;max-width:40px;margin:0 11px" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.alodokter.com&amp;source=gmail&amp;ust=1633682583652000&amp;usg=AFQjCNErb05-KsNqvAx64fSrizc90_yPag">
                    <img style="width:100%" src="https://ci6.googleusercontent.com/proxy/aLAklNGqaT5uQUA4VXfDmBmqkTkSiBa7owBnVfJ8qAyIwKwoL2tk5QXf3Q8iuPwedZ8PZ7CRGuPIzHd2Qn_r_daRpqz3nXhTumnitLHNc86FS5nYMb7u5HKfiBoX8UGON3JNnDC3epybRkKwKXXkk3rffw=s0-d-e1-ft#https://res.cloudinary.com/dk0z4ums3/image/upload/f_auto/v1545112837/email_template/icon_web.svg" class="CToWUd">
                    </a>
                </div>
                <span style="text-align:center;line-height:16px;display:block;font-size:12px">© 2020 - 2021 PT. Kreasi Layanan Medis. All Rights Reserved</span>
            </div>
        </div>
        <img src="https://ci6.googleusercontent.com/proxy/q_KmFqBVqTbqWv4IfeaoT2pcca5o87V--LOlr4u-iDBXP5P33rqT8n6S_MSJuijuTvolBOZ1eXA2xTRGzKOIWgDLqyElh7cctY3t7QgZm3xIXn3mT-J2RuZGS7zusj9FKXnQa9V4DyUQi_I18y06Q3T8l_vTF9NfwHxuV4mEzH0nL18hKPeQUOwcn5C2cU0HcwH_2sVo4Wa8_OGykdHs-ZstqV5LlOp5e25RjA2XbeJ6FsnrkYJxXnTaPrKw0VXbQqRs3j2QP0tdPuNNdAqsRxnaOSpzxXBs23K4MfN8L6Fw0kRcoe-Pzj6Wi9zHJ7Iop2vVxT7bro1KumYPNH-g_CSV0TpgT9V0HmKRWA1SLXOfKCiE0Yk8u4vw24q4stSdozUWEYOiT2rqP_YzZgY5wzV5ArFI3wGGM16U9Uy-Lg=s0-d-e1-ft#http://url3540.alodokter.com/wf/open?upn=QKz4gpAkhni7yXsc0a0AvoV-2Ber-2B8K-2BPkvCIfmU0-2By6radLqvyal44T4xT-2BqmEjAD3nb-2FVsjpLahTp4cqplEwUEadkCPpqykaXF6vsIMKXAKv4AZOW7WYvPyCoP2XdRQ9CY8oIul3iHaEdqC8YgZ02u-2BxSGd3j4IXydzC8hsokiBfN-2BCkTmDymoKTJRmzpWoNtUxuTNCX-2FLzxsU3bQjYMGK5sAep-2B36CiZFUkQKv0dgY-3D" alt="" width="1" height="1" border="0" 
        style="height:1px!important;width:1px!important;border-width:0!important;margin-top:0!important;margin-bottom:0!important;margin-right:0!important;margin-left:0!important;padding-top:0!important;padding-bottom:0!important;padding-right:0!important;padding-left:0!important" class="CToWUd">
        </div>
























<div style="width:100%;max-width:600px;min-width:352px;font-family:'Lato',arial;font-size:14px;line-height:24px;color:#595959;box-sizing:border-box;margin:0 auto;border:solid 1px #ddd">
    <div style="text-align:right;padding:30px 15px;border-bottom:solid 2px #0bafab">
        <!-- <img style="width:200px;height:48px" src="https://ci6.googleusercontent.com/proxy/ezE1KLRfl1PUAfRUxiVXgfomhXInpnbR2yVXrKTV4kycQS4hDqs3ba9OZEYIP9Zx5Ncuex3vuKKH3IbgNrDhbt3jOaoyWqCAIn8t-NmSIJEde7gTTTi0TIRB5-PGYu0DQmVzbIXm=s0-d-e1-ft#http://res.cloudinary.com/dk0z4ums3/image/upload/v1562809069/setting/1562809068.png" alt="Alodokter" title="Alodokter" class="CToWUd"> -->
        <img style="width:200px;height:48px" src="https://dash.klinis.id/assets/images/logo.png" alt="Logo Klinis" title="Logo Klinis" class="CToWUd">
    </div>
    <div style="margin:32px 8px 40px 16px">
        <h3 style="margin-bottom:24px;line-height:32px;font-size:22px;font-weight:800;text-align:center;color:#47d3cd">Pembayaran Berhasil</h3>
        <p style="font-style:normal;font-weight:normal;font-size:14px;line-height:24px;color:#595959">Yth. Bapak/Ibu *NAMA CUSTOMER*,
        <br><br>
        Melalui email ini kami informasikan bahwa pembayaran Anda telah <strong style="font-weight:700;color:#47d3cd">berhasil</strong>.
        <br>Rincian pembayaran dapat dilihat di bawah ini:</p>

        <div style="margin:24px 0px;padding:37px 10px;border:1px solid #d9d9d9;box-sizing:border-box;max-width:568px">
            <div style="display:table;width:100%;margin-bottom:16px">
              <div style="width:90%">
              
              <img style="width:12.5em;height:auto;display:table-cell" src="https://dash.klinis.id/assets/images/logo.png" alt="Logo Klinis" title="Logo Klinis" class="CToWUd">
            </div>
              <div style="font-size:100%;font-weight:bold;font-stretch:normal;font-style:normal;line-height:normal;letter-spacing:normal;color:#9b9b9b;display:table-cell;text-align:right">INVOICE</div>
            </div>
            <div style="display:table;width:100%;margin-top:10px">
                <div style="margin:0px 99px 20px 0px;float:left;display:block;min-height:60px">
                    <div style="font-size:14px;line-height:17px;color:#424242;font-weight:600">PT. Kreasi Layanan Medis</div>
                    <div style="font-size:12px;line-height:135%;color:#7f7f7f;font-weight:normal;width:250px;height:44px;margin-top:5px">
                        <div>
                            Gedung CNI Creative Center (C3), Lt.2.<br>
                            Jalan Puri Indah Blok O2 No.1-3<br>
                            Kembangan, Jakarta Barat 11610.<br>
                        </div>
                        <div>NPWP: *NOMOR NPWP PERUSAHAAN*</div>
                    </div>
                    <div style="margin-bottom:5px">&nbsp;</div>
                </div>
                <div style="float:left;display:block;margin-top:0px;min-height:60px">
                  <div style="display:table-cell;vertical-align:top;font-size:12px;line-height:17px">
                      <table style="font-family:'Lato',arial;border-collapse:collapse;border-spacing:0;color:#7f7f7f;margin-left:auto;color:#7f7f7f">
                        <tbody>
                          <tr>
                            <td style="font-family:'Lato',arial">No. Invoice</td>
                            <td>:</td>
                            <td style="font-family:'Lato',arial;color:#47d3cd;font-weight:600;text-align:left;text-align:-webkit-left">
                                <a href="https://dash.klinis.id/InvoicePDF/eInvoice" target="_blank" style="color:#47d3cd">*NOMOR INVOICE*</a>
                                <!-- <a href="http://localhost/dash/InvoicePDF/eInvoice" target="_blank" style="color:#47d3cd">*NOMOR INVOICE*</a> -->
                            </td>
                          </tr>
                          <tr>
                            <td style="font-family:'Lato',arial">Tanggal</td>
                            <td>:</td>
                            <td style="font-family:'Lato',arial;color:#424242;font-weight:600;text-align:left;text-align:-webkit-left">*TANGGAL INVOICE*</td>
                          </tr>
                          <tr>
                            <td style="font-family:'Lato',arial">Status</td>
                            <td>:</td>
                            <td style="font-family:'Lato',arial;color:#424242;font-weight:600;text-align:left;text-align:-webkit-left">*STATUS PEMBAYARAN*</td>
                          </tr>
                          <tr>
                            <td style="font-family:'Lato',arial">Pembayaran</td>
                            <td>:</td>
                            <td style="font-family:'Lato',arial;color:#424242;font-weight:600;text-align:left;text-align:-webkit-left">*METODE PEMBAYARAN*</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                </div>
                <div style="clear:both">
            </div>
            <div style="display:table;width:100%;margin:20px 0px 20px 2px;font-size:0.87em;line-height:1.1em;font-family:'Lato',arial">
                <div style="margin-bottom:5px;font-weight:normal;color:#7f7f7f;font-family:'Lato',arial">Kepada :</div>
                <div style="margin-botom:5px;font-weight:600;color:#424242;font-family:'Lato',arial">*NAMA CUSTOMER*</div>
                <div style="margin-bottom:5px;font-weight:600;color:#424242;font-family:'Lato',arial">*PHONE CUSTUMER*</div>
                <div style="font-weight:600;color:#424242;text-decoration:none;font-family:'Lato',arial"><a style="color:#424242;text-decoration:none">*EMAIL CUSTOMER*</a></div>
            </div>
            <div style="box-sizing:border-box;font-size:inherit;display:table;width:100%">
                <table style="border-radius:6px;border-spacing:0px;border:solid 1px #ececec;width:100%;font-size:0.87em;line-height:1.3em;font-family:'Lato',arial">
                    <thead>
                        <tr style="font-family:'Lato',arial;color:#595959;font-weight:600;font-style:normal;background-color:#ececec;font-weight:600;font-size:0.87em;line-height:1em;height:40px">
                            <td style="font-family:'Lato',arial;color:#595959;font-weight:600;font-style:normal;background-color:#ececec;font-weight:600;height:40px;padding-left:20px">Nama Produk</td>
                            <td style="font-family:'Lato',arial;color:#595959;font-weight:600;font-style:normal;background-color:#ececec;font-weight:600;height:40px;padding:0px 20px 0px 0px;text-align:right">Qty</td>
                            <td style="font-family:'Lato',arial;color:#595959;font-weight:600;font-style:normal;background-color:#ececec;font-weight:600;height:40px;padding:0px 10px 0px 10px">Harga<br>sebelum Pajak</td>
                            <td style="font-family:'Lato',arial;color:#595959;font-weight:600;font-style:normal;background-color:#ececec;font-weight:600;height:40px;padding:0px 20px 0px 0px;text-align:right">Pajak 10%</td>
                            <td style="font-family:'Lato',arial;color:#595959;font-weight:600;font-style:normal;background-color:#ececec;font-weight:600;height:40px;text-align:right;padding-right:20px">Harga Nett</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $total_bayar =23213240;
                        $harga_nett = 25000;
                       
                        for($i = 0; $i < 3; $i++){ ?>
                            <tr style="color:#424242;font-family:'Lato',arial">
                                <td style="font-family:'Lato',arial;border-bottom:solid 1px #ececec;padding:20px 0px 20px 20px">*NAMA PRODUK*</td>
                                <td style="font-family:'Lato',arial;border-bottom:solid 1px #ececec;padding:20px 20px 20px 0px;text-align:right">*QTY*</td>
                                <td style="font-family:'Lato',arial;border-bottom:solid 1px #ececec;padding:20px 20px 20px 0px;text-align:right">*HARGA SEBELUM PAJAK*</td>
                                <td style="font-family:'Lato',arial;border-bottom:solid 1px #ececec;padding:20px 20px 20px 0px;text-align:right">*PAJAK*</td>
                                <td style="font-family:'Lato',arial;border-bottom:solid 1px #ececec;padding:20px 20px 20px 0px;text-align:right"><?= $harga_nett?></td>
                            </tr>
                        <?php 
                        $total_bayar += $harga_nett;
                        }?>
                        <tr>
                            <td colspan="3" style="font-family:'Lato',arial;padding:20px 0px 20px 20px;color:#7f7f7f;font-style:italic">*<?= terbilang("23213240")?>*</td>
                            <td colspan="2" style="font-family:'Lato',arial;padding:20px 20px 20px 0px;color:#47d3cd;text-align:right;font-weight:600">Total Bayar : <?= $total_bayar?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <hr style="box-sizing:border-box;margin-top:20px;margin-bottom:20px;border:1px dashed #ececec">
            <div style="font-family:'Lato',arial;padding:20px 13.87px;border-radius:24px;border:solid 1px #ececec">
                <div style="font-size:12px;line-height:157%;color:#595959">
                    •   Bila memiliki pertanyaan silakan hubungi kami via email di <strong style="color:#47d3cd">
                    <a href="mailto:support@klinis.id" target="_blank" style="color:#47d3cd">support@klinis.id</a></strong> atau di nomor telepon <strong style="color:#47d3cd">(+62) 85211223301</strong></div>
            </div>
            <p style="font-family:'Lato',arial;text-align:center;font-size:10px;line-height:135%;color:#7f7f7f">Dokumen ini dicetak secara digital pada <strong>*TANGGAL CETAK* *WAKTU CETAK*</strong></p>
        </div>
        <p style="font-family:'Lato',arial">Email ini dapat disimpan sebagai bukti pembayaran Anda. 
        <br>
        Terima kasih telah menggunakan KLINIS untuk menyempurnakan perlindungan kesehatan Anda dan keluarga.</p>

        <p style="font-family:'Lato',arial;margin-top:24px">Salam Hangat,<br>
        <a style="font-family:'Lato',arial;color:#47d3cd;text-decoration:none" href="*URL LINK PROTEKSI*" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.alodokter.com/proteksi-alodokter&amp;source=gmail&amp;ust=1633682583651000&amp;usg=AFQjCNGSdMItkDm4Og3GomlyaxbZ7vdF6Q">Medicare KLINIS</a></p>
    </div>
    <div>
        <div style="background:#47d3cd;
                    color:#fff;
                    overflow:hidden;
                    padding:20px 0 0 0;
                    background:#3670d3;
                    box-sizing:border-box;
                    background:-moz-linear-gradient(top,#3670d3 0%,#2164d1 56%,#283d9b 100%);
                    background:-webkit-linear-gradient(top,#3670d3 0%,#2164d1 56%,#283d9b 100%);
                    background:linear-gradient(to bottom,#93fffa 0%,#47d3cd 56%,#1a9993 100%);
                    ">
            <div style="display:flex;width:290px;margin:0 auto">
                <div style="margin-top:auto;height:160px">
                    <img style="width:100px;margin-top:auto" src="https://ci5.googleusercontent.com/proxy/gGtVzTIkK2DzfAs3KJWdT60ZHGJxgGjCB-wkoBsqVh-2PiNyqjF6pZYGfrvOlfQEdbUm1Nl35KFxJyq_ZMtZde9GHALAj5DPndfffvIuhfSz7fltgT6G_FoFkXMd8HEtK1BlIDCJw1qe9P60z9kptQ=s0-d-e1-ft#https://res.cloudinary.com/dk0z4ums3/image/upload/w_100,f_auto/v1545967189/unnamed_z0hsw2.png" class="CToWUd">
                </div>
                <div style="padding:0 0 0 10px;text-align:center;display:flex">
                    <div style="margin-bottom:10px">
                        <h3 style="margin:0 0 5px 0;letter-spacing:2px">KLINIS</h3>
                        <h4 style="font-weight:900;font-size:24px;margin:0 0 10px 0;line-height:26px">Chat Dokter<br>Gratis!</h4>
                        <span style="display:block;margin:0 0 10px 0;font-size:14px">Download Sekarang</span>
                        <div>
                            <a href="*URL PLAY STORE KLINIS*">
                            <img style="width:80px" src="https://ci3.googleusercontent.com/proxy/Ry-ehWAcTB3JAoGG6YOrbhd4OA2hw-wok9iJqQAzjv9vBRb-CpeE9m24Dsqcoo7lBCfwREJEjMXBTMnhc6_CGJ47KDVoWzF0t18ZEUWjU6XTXQLZal8zL6cTjrTNJKu8R2HbceJnZ4sLuyn4eCabcyeSxccF-ZK3=s0-d-e1-ft#https://res.cloudinary.com/dk0z4ums3/image/upload/w_80,f_auto/v1545112837/email_template/Appstore.svg" class="CToWUd">
                            </a>
                            <a href="*URL APPS STORE KLINIS*">
                            <img style="width:80px" src="https://ci6.googleusercontent.com/proxy/RlBRJQLpToutct-5Tj_AqHEJHOyaGngeRgM0clDCFhkatUEws_PTiW1TjYQXM6VFudcdvSs0djWYRIVL30A_KHixZ0RW1aPI22jylE4hI9t71dCP8_SeVjLjFUTHQK-SVINZnxOL5wUgMKXC2AodLy3iLJqEUyCQeQ=s0-d-e1-ft#https://res.cloudinary.com/dk0z4ums3/image/upload/w_80,f_auto/v1545112839/email_template/Playstore.svg" class="CToWUd">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="background:#47d3cd;color:#fff;overflow:hidden;
        
        background:-moz-linear-gradient(top,#3670d3 0%,#2164d1 56%,#283d9b 100%);
                    background:-webkit-linear-gradient(top,#3670d3 0%,#2164d1 56%,#283d9b 100%);
                    background:linear-gradient(to bottom,#37b1ab 0%,#47d3cd 56%,#6be8e2 100%);
        ">
            <div style="text-align:center;margin:16px 0px">
                    <h3 style="margin-bottom:4px;font-size:14px;line-height:22px;font-weight:bold">PT. KREASI LAYANAN MEDIS</h3>
                    <p style="font-size:12px;line-height:20px;margin:0px">
                    Gedung CNI Creative Center (C3), Lt.2.
                    <br>
                        <a href="https://g.page/klinis?share" style="color:white;text-decoration:none;font-size:13px" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://maps.google.com/?q%3DJL.%2BProf.%2BDr.%2BSatrio%2BNo.%2B5%2BBlok%2BC4%2BKuningan%2B%25E2%2580%2593%2BSetiabudi,%2BJakarta%2B12950,%2BIndonesia%26entry%3Dgmail%26source%3Dg&amp;source=gmail&amp;ust=1633682583652000&amp;usg=AFQjCNHPdL55k42dJLtgDfzfWIt_PheDCQ">
                            Jalan Puri Indah Blok O2 No.1-3
                            <br>
                            Kembangan, Jakarta Barat 11610.
                        </a>
                    </p>
            </div>
            <div style="text-align:center;margin-bottom:16px">
                <h3 style="margin-bottom:4px;font-size:14px;line-height:22px;font-weight:bold">Layanan Costumer Care</h3>
                <p style="margin:0px;font-size:12px;line-height:20px"><a style="color:#fff;text-decoration:none"> +(021)-58358309 </a><br><a style="color:#fff;text-decoration:none">support@klinis.id</a></p>
            </div>
        </div>
    </div>
    <div style="padding:36px 0px 12px 0px">
        <div style="text-align:center;padding:0;margin-bottom:16px">
            <a href="*URL FACEBOOK PERUSAHAAN*" style="display:inline-block;width:10%;max-width:40px;margin:0 11px" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.facebook.com/alodokter&amp;source=gmail&amp;ust=1633682583652000&amp;usg=AFQjCNFHUdaziqZz1SjIskREKW1uiB_S3Q">
            <img style="width:100%" src="https://ci5.googleusercontent.com/proxy/xjU3lBT25RJsv4AJlbNWsbdh8l4MXB_H8wduPFEDxWEoPX-o_7TwVefULzplEC36CeE4dizmX-kY45iyz74ChLb2NUmoFtF-yq9WU2M04PbxyXSCNrp0eDlrUtis3qBnaacgqY0Ibe0u9RGxDKOq5ORBx7SS8ZLA=s0-d-e1-ft#https://res.cloudinary.com/dk0z4ums3/image/upload/f_auto/v1545112837/email_template/icon_facebook.svg" class="CToWUd">
            </a>
            <a href="*URL INSTAGRAM PERUSAHAAN*" style="display:inline-block;width:10%;max-width:40px;margin:0 11px" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.instagram.com/alodokter_id/&amp;source=gmail&amp;ust=1633682583652000&amp;usg=AFQjCNH5KyjYwyG5gyJsK--aA0wZQ-1fTg">
            <img style="width:100%" src="https://ci6.googleusercontent.com/proxy/NfCC6qAllxgWIZIL-3j15fGJzFsIVJ0pgTwCRdOX4ET_ZAmyr9E2oUgQnx8k9cFj3jG3ppBMFeyha35U-JAnPar0Rn2vQyix0nFz4jDPKkAhwFNOUemm0beDYIUoOSXpDiWVphkucYm2UmDSt6G05smb7pzyl-wvlw=s0-d-e1-ft#https://res.cloudinary.com/dk0z4ums3/image/upload/f_auto/v1545112837/email_template/icon_instagram.svg" class="CToWUd">
            </a>
            <a href="*URL TWITTER PERUSAHAAN*" style="display:inline-block;width:10%;max-width:40px;margin:0 11px" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://twitter.com/alodokter&amp;source=gmail&amp;ust=1633682583652000&amp;usg=AFQjCNGVmad12WbCjzth124f5SM_NuWuJg">
            <img style="width:100%" src="https://ci4.googleusercontent.com/proxy/SatyMj3ZTbNWW7INYphXh7JnhgogQ1u7kPjhqWhUNaSrE2st1aOQA3fJhN6PKQTCWuNaJBBBmE1ZmZ8Tdl8cu3EozeIsjuCz-6judUqeyJpUuhVlcVtoXE9KUz6W0_38bjkSaO_uy68eWC9CsL2LsRex7aVrono=s0-d-e1-ft#https://res.cloudinary.com/dk0z4ums3/image/upload/f_auto/v1545112837/email_template/icon_twitter.svg" class="CToWUd">
            </a>
            <a href="*URL YOUTUBE PERUSAHAAN*" style="display:inline-block;width:10%;max-width:40px;margin:0 11px" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.youtube.com/channel/UCVlQXBPDCbz5YES_gBpC1rg&amp;source=gmail&amp;ust=1633682583652000&amp;usg=AFQjCNGC2F_4LYE4AZoE-Va5YlCOjO_xPw">
            <img style="width:100%" src="https://ci4.googleusercontent.com/proxy/wqkL-YBfl2_P1pdVD9yuTM9wriMsmyujiqqMMbOR58DuW3iipEE3dqMbjeRPzwMEVmRfeKWbeAmYEwvOuXcmIKQtj9m90CRJVNHYmV2cdooxPIhTkqxak7VhdLiu-sMkbzKUHS65rMNH_XDSSzH65AOol1iCiiM=s0-d-e1-ft#https://res.cloudinary.com/dk0z4ums3/image/upload/f_auto/v1545112838/email_template/icon_youtube.svg" class="CToWUd">
            </a>
            <a href="*URL WEBSITE PERUSAHAAN*" style="display:inline-block;width:10%;max-width:40px;margin:0 11px" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.alodokter.com&amp;source=gmail&amp;ust=1633682583652000&amp;usg=AFQjCNErb05-KsNqvAx64fSrizc90_yPag">
            <img style="width:100%" src="https://ci6.googleusercontent.com/proxy/aLAklNGqaT5uQUA4VXfDmBmqkTkSiBa7owBnVfJ8qAyIwKwoL2tk5QXf3Q8iuPwedZ8PZ7CRGuPIzHd2Qn_r_daRpqz3nXhTumnitLHNc86FS5nYMb7u5HKfiBoX8UGON3JNnDC3epybRkKwKXXkk3rffw=s0-d-e1-ft#https://res.cloudinary.com/dk0z4ums3/image/upload/f_auto/v1545112837/email_template/icon_web.svg" class="CToWUd">
            </a>
        </div>
        <span style="text-align:center;line-height:16px;display:block;font-size:12px">© 2020 - 2021 PT. Kreasi Layanan Medis. All Rights Reserved</span>
    </div>
</div>
  <img src="https://ci6.googleusercontent.com/proxy/q_KmFqBVqTbqWv4IfeaoT2pcca5o87V--LOlr4u-iDBXP5P33rqT8n6S_MSJuijuTvolBOZ1eXA2xTRGzKOIWgDLqyElh7cctY3t7QgZm3xIXn3mT-J2RuZGS7zusj9FKXnQa9V4DyUQi_I18y06Q3T8l_vTF9NfwHxuV4mEzH0nL18hKPeQUOwcn5C2cU0HcwH_2sVo4Wa8_OGykdHs-ZstqV5LlOp5e25RjA2XbeJ6FsnrkYJxXnTaPrKw0VXbQqRs3j2QP0tdPuNNdAqsRxnaOSpzxXBs23K4MfN8L6Fw0kRcoe-Pzj6Wi9zHJ7Iop2vVxT7bro1KumYPNH-g_CSV0TpgT9V0HmKRWA1SLXOfKCiE0Yk8u4vw24q4stSdozUWEYOiT2rqP_YzZgY5wzV5ArFI3wGGM16U9Uy-Lg=s0-d-e1-ft#http://url3540.alodokter.com/wf/open?upn=QKz4gpAkhni7yXsc0a0AvoV-2Ber-2B8K-2BPkvCIfmU0-2By6radLqvyal44T4xT-2BqmEjAD3nb-2FVsjpLahTp4cqplEwUEadkCPpqykaXF6vsIMKXAKv4AZOW7WYvPyCoP2XdRQ9CY8oIul3iHaEdqC8YgZ02u-2BxSGd3j4IXydzC8hsokiBfN-2BCkTmDymoKTJRmzpWoNtUxuTNCX-2FLzxsU3bQjYMGK5sAep-2B36CiZFUkQKv0dgY-3D" alt="" width="1" height="1" border="0" 
  style="height:1px!important;width:1px!important;border-width:0!important;margin-top:0!important;margin-bottom:0!important;margin-right:0!important;margin-left:0!important;padding-top:0!important;padding-bottom:0!important;padding-right:0!important;padding-left:0!important" class="CToWUd">
</div>